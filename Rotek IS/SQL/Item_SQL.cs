﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace RotekIS.SQL
{
    class Item_SQL
    {
        public int id;
        public Decimal count, wrecked;

        public Item_SQL(int stockID, string[] values)
        {
            int[] parametersID = Stock_SQL.GetParameters(stockID).Keys.ToArray<int>();

            SQL_main.AddCommand("DECLARE @INo_ID int");
            SQL_main.AddCommand("SET @INo_ID =(SELECT top 1 II.ItemNo_ID FROM Items I JOIN Items_ItemNo II ON II.Items_ID = I.ID WHERE ");

            for (int i = 0; i < parametersID.Length; i++)
            {
                SQL_main.AddCommand("~(I.Name= '" + values[i] + "' AND I.Parameters_ID= " + parametersID[i] + " )");
                if (i != parametersID.Length - 1) SQL_main.AddCommand("~OR");
            }
            SQL_main.AddCommand("~GROUP BY II.ItemNo_ID  having COUNT(II.ItemNo_ID)=" + parametersID.Length + ")");

            SQL_main.AddCommand("select TOP 1 C.ItemNo_Id, C.Count, C.Wrecked from Count C where C.Employer_ID is null AND C.ItemNo_ID = @INo_ID ORDER BY Date DESC");

            DataTable dt = SQL_main.Commit_List().Tables[0];
            if (dt.Rows.Count == 1)
            {

                id = int.Parse(dt.Rows[0][0].ToString());
                count = Decimal.Parse(dt.Rows[0][1].ToString());
                wrecked = Decimal.Parse(dt.Rows[0][2].ToString());
            }

        }

    }
}
