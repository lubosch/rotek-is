﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using RotekIS.Properties;
using RotekIS.SQL;

namespace RotekIS.Stocks
{
    public class Employer
    {

        public Employer()
        {

        }

        public static DataSet List_zamestnanci()
        {
            return SQL_main.List("SELECT ID, Name , Surname FROM Employer WHERE Typ IS NULL AND Until IS NULL ORDER BY Surname, Name");
        }

        public static DataSet List_nezamestnanci()
        {
            return SQL_main.List("SELECT ID, Name , Surname FROM Employer WHERE Typ = 1 AND Until IS NULL ORDER BY Surname, Name");
        }

        public static DataSet Name_Surname(string zaciatok)
        {
            return SQL_main.List("SELECT CONCAT(Surname , ' ', Name)  as NameC FROM Employer WHERE Until IS NULL AND CONCAT(Surname , ' ', Name) LIKE '%" + zaciatok + "%' ORDER BY Surname ");
        }


        public static void Add(String name, string surname, int typ)
        {
            if (typ == 0)
            {
                SQL_main.Odpalovac("INSERT INTO Employer (Name, Surname, Since) VALUES ('" + name + "','" + surname + "', GETDATE())");

            }
            else if (typ == 1)
            {
                SQL_main.Odpalovac("INSERT INTO Employer (Name, Surname, Since, Typ) VALUES ('" + name + "','" + surname + "', GETDATE(), " + typ + ")");
            }
        }

        public static void Remove(String name, string surname)
        {
            SQL_main.Odpalovac("UPDATE Employer SET UNTIL = GETDATE() WHERE CONCAT(Surname , ' ', Name) = '" + surname + " " + name + "'");
        }

        public static string[] Names(string menpr)
        {
            DataSet ds = SQL_main.List("SELECT Name, Surname FROM Employer WHERE (Name + ' ' + Surname) = '" + menpr + "'");
            return new string[] { ds.Tables[0].Rows[0][0].ToString(), ds.Tables[0].Rows[0][1].ToString() };
        }


    }
}
