﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RotekIS.SQL
{


    class Save_disk
    {
        public static String root = "\\\\192.168.1.150\\Sklad\\Sklad\\";

        public static string Stock_Image(string strpath, int stockID)
        {
          
            string stock = Stock_SQL.stock_name(stockID);

            if (File.Exists(strpath))
            {
                string save_to = root + "Fotky\\" + stock + "\\";
                if (!Directory.Exists(save_to)) Directory.CreateDirectory(save_to);
                save_to += strpath.Substring(strpath.LastIndexOf('\\') + 1);
                File.Copy(strpath, save_to, true);
                return save_to;
            }
            return null;
        }



    }
}
