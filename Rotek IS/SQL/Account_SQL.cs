﻿using RotekIS.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using RotekIS.Main;

namespace RotekIS.SQL
{
    public class Account_SQL
    {
        private Account_SQL()
        {

        }
        public static void Add_Type(string name)
        {
            SQL_main.Odpalovac("INSERT INTO Rights (Name) Values ('" + name + "')");
        
        }

        public static void Add(string nick, string real, string password, string oldpass, string mail, string mail_pass, int rights)
        {
            if (oldpass == null)
            {
                SQL_main.AddCommand("INSERT INTO Account (Nick, Real_Name, Passwd, Mail, Mail_passwd, Rights_ID) VALUES (@Nick, @Real ,@Passwd, @Mail, @Mail_pass, (SELECT TOP 1 ID FROM Rights WHERE Name=@Rights))");
                SQL_main.AddParameter("@Nick", nick);
                SQL_main.AddParameter("@Passwd", password);
                SQL_main.AddParameter("@Mail", mail);
                SQL_main.AddParameter("@Mail_pass", mail_pass);
                SQL_main.AddParameter("@Real", real);
                string rghts = null;
                switch (rights)
                {
                    case 1:
                        rghts = "admin";
                        break;
                    case 2:
                        rghts = "zakaz";
                        break;
                    case 3:
                        rghts = "sklad";
                        break;
                }
                SQL_main.AddParameter("@Rights", rghts);
            }
            else
            {
                string sql = "";
                sql = ("UPDATE Account SET");
                if (nick != "") sql += (" , Nick = @Nick");
                if (real != "") sql += (" , Real_Name = @Real");
                if (password != "") sql += (" , Passwd = @Passwd");
                if (mail != "") sql += (" , Mail = @Mail");
                if (mail_pass != "") sql += (" , Mail_passwd = @Mail_pass");
                if (sql.IndexOf(',') < 0) return;
                sql = sql.Replace("SET ,", "SET ");
                sql += " WHERE Passwd ='" + oldpass + "'";
                SQL_main.AddCommand(sql);
                SQL_main.AddParameter("@Nick", nick);
                SQL_main.AddParameter("@Passwd", password);
                SQL_main.AddParameter("@Mail", mail);
                SQL_main.AddParameter("@Mail_pass", mail_pass);
                SQL_main.AddParameter("@Real", real);


            }
            SQL_main.Commit_Transaction();

        }
        public static Uzivatel Type_User(string Password)
        {
            DataSet ds = new DataSet();
            Uzivatel uz = new Uzivatel();
            SQL_main.AddCommand("SELECT R.Name, A.Nick, A.Mail, A.Mail_passwd, A.Real_Name FROM RIGHTS R JOIN Account A ON A.Rights_ID=R.ID WHERE A.Passwd=@Passwd");
            SQL_main.AddParameter("@Passwd", Password);
            ds = SQL_main.Commit_List();
            if (ds.Tables[0].Rows.Count == 0)
            {
                return uz;
            }
            uz.typ = ds.Tables[0].Rows[0][0].ToString();
            uz.nick = ds.Tables[0].Rows[0][1].ToString();
            uz.mail = ds.Tables[0].Rows[0][2].ToString();
            uz.mail_heslo = ds.Tables[0].Rows[0][3].ToString();
            uz.real_name = ds.Tables[0].Rows[0][4].ToString();
            return uz;
        }


    }
}
