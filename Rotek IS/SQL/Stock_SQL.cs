﻿using RotekIS.General;
using RotekIS.Stocks;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RotekIS.SQL
{
    public class Stock_SQL
    {
        public static void Add(string name, ListViewItem[] parameters)
        {

            DataTable dd = new DataTable();
            dd = SQL_main.List("SELECT ID FROM Stock WHERE Name = '" + name + "'").Tables[0];
            if (dd.Rows.Count > 0)
            {
                int stock_id = (int)dd.Rows[0]["ID"];
                dd = SQL_main.List("SELECT Name, Search FROM Parameters WHERE Stock_ID = " + stock_id).Tables[0];

                SQL_main.AddCommand("    ");
                foreach (ListViewItem param in parameters)
                {
                    if (!dd.AsEnumerable().Any(row => row.Field<String>("Name").ToLower() == param.SubItems[0].Text.ToLower()))
                    {
                        SQL_main.AddCommand("INSERT INTO Parameters (Name, Search, Stock_ID) VALUES('" + param.SubItems[0].Text + "' , '" + param.SubItems[1].Text + "' , " + stock_id + " )");
                    }
                    else if (!dd.AsEnumerable().Any(row => row.Field<String>("Name").ToLower() == param.SubItems[0].Text.ToLower() && row.Field<String>("Search").ToLower() == param.SubItems[1].Text.ToLower()))
                    {
                        SQL_main.AddCommand("UPDATE Parameters SET Search = '" + param.SubItems[1].Text + "' WHERE Name = '" + param.SubItems[0].Text + "' AND Stock_ID = " + stock_id + " ");
                    }
                }
                SQL_main.Commit_Transaction();

            }
            else
            {
                SQL_main.AddCommand("INSERT INTO Stock (Name) VALUES ('" + name + "')");

                foreach (ListViewItem param in parameters)
                {
                    SQL_main.AddCommand("INSERT INTO Parameters (Name, Search, Stock_ID) SELECT '" + param.SubItems[0].Text + "', '" + param.SubItems[1].Text + "', ID FROM Stock WHERE Name='" + name + "'");
                    //SQL_main.Odpalovac("INSERT INTO Parameters (Name, Stock_ID) SELECT '" + param + "', ID FROM Stock WHERE Name='" + name + "'");
                }
                SQL_main.Commit_Transaction();
            }
        }


        public static int parameter_ID(string name, int stock_id)
        {
            SQL_main.AddCommand("SELECT TOP 1 ID FROM Parameters WHERE Name = '" + name + "' AND Stock_ID = " + stock_id);
            Object param_ID = SQL_main.Commit_Transaction();
            if (param_ID != null)
            {
                return (int)param_ID;
            }
            return -1;
        }


        public static int getItemID(String name, String columnName, int stock_ID)
        {
            SQL_main.AddCommand("SELECT TOP 1 i.ID FROM Items i JOIN Parameters p ON p.ID = i.Parameters_ID WHERE i.Name = '" + name + "' AND p.Name = '" + columnName + "' AND p.Stock_ID = " + stock_ID);
            Object existItem = SQL_main.Commit_Transaction();
            int existing_ItemID;
            if (existItem != null)
            {
                return (int)existItem;
            }
            return -1;
        }

        public static int getItemID(int itemNo_ID, String columnName, int stock_ID)
        {
            SQL_main.AddCommand("SELECT TOP 1 i.ID FROM Items i JOIN Items_ItemNo ii ON ii.Items_ID = i.ID JOIN Parameters p ON p.ID = i.Parameters_ID WHERE p.Name = '" + columnName + "' AND p.Stock_ID = " + stock_ID + " AND ii.ItemNo_ID=" + itemNo_ID);
            Object existItem = SQL_main.Commit_Transaction();
            int existing_ItemID;
            if (existItem != null)
            {
                return (int)existItem;
            }
            return -1;
        }

        public static void RenameItem(int itemNo_ID, String newName, String columnName, int stock_ID)
        {
            int param_ID = parameter_ID(columnName, stock_ID);
            int existing_ItemID = getItemID(newName, columnName, stock_ID);
            if (existing_ItemID == -1)
            {
                SQL_main.AddCommand("INSERT INTO Items(Name, Parameters_ID) output INSERTED.ID  VALUES('" + newName + "', " + param_ID + ")");
                existing_ItemID = (int)SQL_main.Commit_Transaction();
            }

            int actual_itemID = getItemID(itemNo_ID, columnName, stock_ID);

            SQL_main.Odpalovac("UPDATE Items_ItemNo SET Items_ID = " + existing_ItemID + " WHERE ItemNo_ID = " + itemNo_ID + " AND Items_ID = " + actual_itemID);

            SQL_main.AddCommand("SELECT COUNT(*) FROM Items_ItemNo WHERE Items_ID = " + actual_itemID);
            if ((int)SQL_main.Commit_Transaction() == 0)
            {
                SQL_main.Odpalovac("DELETE FROM Items WHERE ID = " + actual_itemID);
            }
            Error.Show("Premenované");
        }
        public static void AddSold(int stock_id)
        {
            DataTable dd = new DataTable();
            dd = SQL_main.List("SELECT p.Name parameter_name, s.name stock_name FROM Stock s JOIN Parameters p ON p.Stock_ID = s.ID WHERE s.ID = " + stock_id + "").Tables[0];
            if (dd.Rows.Count > 0)
            {
                ListViewItem[] lvi = new ListViewItem[dd.Rows.Count];
                for (int i = 0; i < dd.Rows.Count; i++)
                {
                    lvi[i] = new ListViewItem(new string[] { dd.Rows[i]["parameter_name"].ToString(), "Všade" });
                }
                Add((String)dd.Rows[0]["stock_name"] + " - predaj", lvi);
            }
        }

        public static DataSet List()
        {
            return SQL_main.List("SELECT S.ID, Name FROM Stock S LEFT JOIN (SELECT Stock_ID, Count(Stock_ID) as occuracy FROM ItemNo GROUP BY Stock_ID) INo ON INo.Stock_ID=S.ID ORDER BY INo.occuracy DESC ");
        }

        public static int stock_ID(string name)
        {
            SQL_main.AddCommand("SELECT TOP 1 ID FROM Stock WHERE Name='" + name + "'");
            object obj = SQL_main.Commit_Transaction();
            if (obj == null)
            {
                return -1;
            }
            return (int)obj;

        }
        public static void rename_stock(int stockID, string newName)
        {
            SQL_main.Odpalovac("UPDATE Stock SET Name = '" + newName + "' WHERE ID = " + stockID);
        }

        public static string stock_name(int stockID)
        {
            SQL_main.AddCommand("SELECT TOP 1 Name FROM Stock WHERE ID=" + stockID + "");
            return (string)SQL_main.Commit_Transaction();
        }

        public static int stock_sold_id(int stockID)
        {
            SQL_main.AddCommand("SELECT TOP 1 ID Name FROM Stock WHERE Name= (SELECT TOP 1 Name FROM Stock WHERE ID = " + stockID + ") + ' - predaj' ");
            object o = SQL_main.Commit_Transaction();
            if (o != null)
            {
                return int.Parse(o.ToString());
            }
            else
            {
                return -1;
            }
        }


        public static DataSet ItemHistory(int[] parameters_ID, string[] values)
        {

            SQL_main.AddCommand("SELECT e.Name Meno, e.Surname Priezvisko, IIF(previous.ID IS NULL,c.count, c.Count - previous.Count) 'Zmena poctu',IIF(previous.ID IS NULL,c.Wrecked, c.Wrecked - previous.Wrecked) 'Zmena zlomených', c.Count 'Vtedajší počet', c.Wrecked 'Spolu zlomil', c.Date Dátum FROM Count c");
            SQL_main.AddCommand("~LEFT JOIN Employer e ON e.ID = c.Employer_ID ");
            SQL_main.AddCommand("~OUTER APPLY(SELECT top 1 * FROM Count c_prev ");
            SQL_main.AddCommand("~Where (c.Employer_ID = c_prev.Employer_ID OR c.Employer_ID IS NULL AND c_prev.Employer_ID IS NULL) ");
            SQL_main.AddCommand("~AND c_prev.Date < c.Date AND c.ItemNo_ID = c_prev.ItemNo_ID ");
            SQL_main.AddCommand("~ORDER BY c_prev.Date DESC) previous");

            SQL_main.AddCommand("~WHERE c.ItemNo_ID IN(SELECT ItemNo_ID FROM (SELECT * FROM Items WHERE");
            int i = 0;
            for (i = 0; i < parameters_ID.Length; i++)
            {
                SQL_main.AddCommand("~(Name = '" + values[i] + "' AND Parameters_ID = " + parameters_ID[i] + ")");
                if (i != parameters_ID.Length - 1) SQL_main.AddCommand("~OR");
            }
            SQL_main.AddCommand("~) I JOIN Items_ItemNo II ON II.Items_ID = I.ID  GROUP BY ItemNo_ID HAVING Count(ItemNo_ID)=" + parameters_ID.Length + ") ORDER BY c.Date DESC");
            return SQL_main.Commit_List();



        }

        public static DataSet ItemInfo(int[] parameters_ID, string[] values)
        {

            SQL_main.AddCommand("SELECT Name Meno, Surname Priezvisko, Count Počet, Wrecked Zlomené, Date Dátum FROM Count LEFT JOIN Employer E ON E.ID = Employer_ID WHERE DATE IN(SELECT MAX(Date) FROM COUNT C WHERE C.ItemNo_ID = (select ItemNo_ID FROM (SELECT * FROM Items WHERE");
            int i = 0;
            for (i = 0; i < parameters_ID.Length; i++)
            {
                SQL_main.AddCommand("~(Name = '" + values[i] + "' AND Parameters_ID = " + parameters_ID[i] + ")");
                if (i != parameters_ID.Length - 1) SQL_main.AddCommand("~OR");
            }
            SQL_main.AddCommand("~) I JOIN Items_ItemNo II ON II.Items_ID = I.ID  GROUP BY ItemNo_ID HAVING Count(ItemNo_ID)=" + parameters_ID.Length + ") GROUP BY Employer_ID)");
            return SQL_main.Commit_List();
        }

        public static DataSet ListParameterItems(int[] parameters_ID, string[] values)
        {
            SQL_main.AddCommand("SELECT Name, Parameters_ID FROM Items WHERE ID IN (SELECT Items_ID FROM Items_ItemNo WHERE ItemNo_ID IN (select ItemNo_ID FROM (SELECT * FROM Items WHERE  ");
            int i = 0, j = 0;
            for (i = 0; i < parameters_ID.Length; i++)
            {
                SQL_main.AddCommand("~(");
                string[] vls = values[i].Split(' ');
                for (j = 0; j < vls.Length; j++)
                {
                    SQL_main.AddCommand("~Name LIKE '%" + vls[j] + "%' ");
                    if (j != vls.Length - 1) SQL_main.AddCommand("~AND");
                }
                SQL_main.AddCommand("~ AND Parameters_ID = " + parameters_ID[i] + ")");
                if (i != parameters_ID.Length - 1) SQL_main.AddCommand("~OR");
            }

            SQL_main.AddCommand("~) I JOIN Items_ItemNo II ON II.Items_ID = I.ID  GROUP BY ItemNo_ID HAVING Count(ItemNo_ID)=" + parameters_ID.Length + "))");
            return SQL_main.Commit_List();
        }

        public static DataSet ListParameterItems(int[] parameters_ID, string[] values, string name, string surname)
        {
            SQL_main.AddCommand("DECLARE @EID int");
            SQL_main.AddCommand("SET @EID=(SELECT TOP 1 ID FROM Employer WHERE Name='" + name + "' AND Surname ='" + surname + "')");

            //SQL_main.AddCommand("SELECT I.Name, I.Parameters_ID From Items I WHERE ID IN (SELECT Items_ID FROM Items_ItemNo II WHERE II.ItemNo_ID IN (select ItemNo_ID FROM Count C where C.Employer_ID = @EID GROUP BY (ItemNo_ID)) AND (");


            //int i = 0, j=0;

            //for (i = 0; i < parameters_ID.Length; i++)
            //{
            //    SQL_main.AddCommand("~(");
            //    string[] vls = values[i].Split(' ');
            //    for (j = 0; j < vls.Length; j++)
            //    {
            //        SQL_main.AddCommand("~Name LIKE '%" + vls[j] + "%' ");
            //        if (j != vls.Length - 1) SQL_main.AddCommand("~AND");
            //    }

            //    SQL_main.AddCommand("~ AND Parameters_ID = " + parameters_ID[i] + ")");
            //    if (i != parameters_ID.Length - 1) SQL_main.AddCommand("~OR");
            //}

            //SQL_main.AddCommand("~) GROUP BY Items_ID)");

            SQL_main.AddCommand("SELECT Name, Parameters_ID FROM Items WHERE ID IN (SELECT Items_ID FROM Items_ItemNo II JOIN Count C ON C.ItemNo_ID = II.ItemNo_ID WHERE II.ItemNo_ID IN (select ItemNo_ID FROM (SELECT * FROM Items WHERE  ");
            int i = 0, j = 0;
            for (i = 0; i < parameters_ID.Length; i++)
            {
                SQL_main.AddCommand("~(");
                string[] vls = values[i].Split(' ');
                for (j = 0; j < vls.Length; j++)
                {
                    SQL_main.AddCommand("~Name LIKE '%" + vls[j] + "%' ");
                    if (j != vls.Length - 1) SQL_main.AddCommand("~AND");
                }
                SQL_main.AddCommand("~ AND Parameters_ID = " + parameters_ID[i] + ")");
                if (i != parameters_ID.Length - 1) SQL_main.AddCommand("~OR");
            }

            SQL_main.AddCommand("~) I JOIN Items_ItemNo II ON II.Items_ID = I.ID  GROUP BY ItemNo_ID HAVING Count(ItemNo_ID)=" + parameters_ID.Length + ") AND C.Employer_ID = @EID)");


            return SQL_main.Commit_List();
        }



        public static Dictionary<int, string> GetParameters(int stockID)
        {
            DataSet ds = SQL_main.List("SELECT P.Name, P.ID FROM Parameters as P WHERE P.Stock_ID = " + stockID);
            Dictionary<int, string> vystup = new Dictionary<int, string>();
            int i = 0;
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                vystup.Add(int.Parse(dr[1].ToString()), dr[0].ToString());
                i++;
            }

            return vystup;
        }

        internal static DataSet GetParameters(string name)
        {
            return SQL_main.List("SELECT P.Name, P.Search FROM Parameters P INNER JOIN Stock S ON S.ID=P.Stock_ID WHERE S.Name='" + name + "'");

        }

        public static void Delete(int itemno)
        {
            SQL_main.AddCommand("DELETE FROM Count WHERE ItemNo_ID = " + itemno);
            SQL_main.AddCommand("DELETE FROM Cost WHERE ItemNo_ID = " + itemno);
            SQL_main.AddCommand("DELETE FROM Notes WHERE ItemNo_ID = " + itemno);
            SQL_main.AddCommand("DELETE FROM Items_ItemNo WHERE ItemNo_ID = " + itemno);
            SQL_main.AddCommand("DELETE FROM ItemNo WHERE ID = " + itemno);
            SQL_main.Commit_Transaction();

            //SQL_main.Odpalovac("DELETE FROM Items WHERE ID = " + itemno);

        }

        public static void RemoveStock(int stock_ID)
        {

            SQL_main.AddCommand("DELETE FROM Notes WHERE ItemNo_ID IN (SELECT ID FROM ItemNo WHERE Stock_ID = " + stock_ID + ")");
            SQL_main.AddCommand("DELETE FROM Cost WHERE ItemNo_ID IN (SELECT ID FROM ItemNo WHERE Stock_ID = " + stock_ID + ")");
            SQL_main.AddCommand("DELETE FROM Count WHERE ItemNo_ID IN (SELECT ID FROM ItemNo WHERE Stock_ID = " + stock_ID + ")");
            SQL_main.AddCommand("DELETE FROM Items_ItemNo WHERE ItemNo_ID IN (SELECT ID FROM ItemNo WHERE Stock_ID = " + stock_ID + ")");
            SQL_main.AddCommand("DELETE FROM ItemNo WHERE Stock_ID = " + stock_ID);
            SQL_main.AddCommand("DELETE FROM Items WHERE Parameters_ID IN (SELECT ID FROM Parameters WHERE Stock_ID = " + stock_ID + ")");
            SQL_main.AddCommand("DELETE FROM Parameters WHERE Stock_ID = " + stock_ID);
            SQL_main.AddCommand("DELETE FROM Stock WHERE ID = " + stock_ID);
            SQL_main.Commit_Transaction();
        }


        public static void Remove(string parameter, string stock_name)
        {
            if (SQL_main.List("SELECT * FROM Parameters WHERE Stock_ID = (SELECT TOP 1 ID FROM Stock WHERE Name ='" + stock_name + "') AND Name='" + parameter + "'").Tables[0].Rows.Count == SQL_main.List("SELECT * FROM Parameters WHERE Stock_ID = (SELECT TOP 1 ID FROM Stock WHERE Name ='" + stock_name + "') ").Tables[0].Rows.Count)
            {
                SQL_main.AddCommand("DELETE FROM Notes WHERE ItemNo_ID IN (SELECT ID FROM ItemNo WHERE ID IN (SELECT ItemNo_ID FROM Items_ItemNo WHERE Items_ID IN (SELECT ID FROM Items WHERE Parameters_ID IN (SELECT ID FROM Parameters WHERE Stock_ID = (SELECT TOP 1 ID FROM Stock WHERE Name ='" + stock_name + "') AND Name='" + parameter + "'))))");
                SQL_main.AddCommand("DELETE FROM Cost WHERE ItemNo_ID IN (SELECT ID FROM ItemNo WHERE ID IN (SELECT ItemNo_ID FROM Items_ItemNo WHERE Items_ID IN (SELECT ID FROM Items WHERE Parameters_ID IN (SELECT ID FROM Parameters WHERE Stock_ID = (SELECT TOP 1 ID FROM Stock WHERE Name ='" + stock_name + "') AND Name='" + parameter + "'))))");
                SQL_main.AddCommand("DELETE FROM Count WHERE ItemNo_ID IN (SELECT ID FROM ItemNo WHERE ID IN (SELECT ItemNo_ID FROM Items_ItemNo WHERE Items_ID IN (SELECT ID FROM Items WHERE Parameters_ID IN (SELECT ID FROM Parameters WHERE Stock_ID = (SELECT TOP 1 ID FROM Stock WHERE Name ='" + stock_name + "') AND Name='" + parameter + "'))))");
                SQL_main.AddCommand("DELETE FROM ItemNo WHERE ID IN (SELECT ItemNo_ID FROM Items_ItemNo WHERE Items_ID IN (SELECT ID FROM Items WHERE Parameters_ID IN (SELECT ID FROM Parameters WHERE Stock_ID = (SELECT TOP 1 ID FROM Stock WHERE Name ='" + stock_name + "') AND Name='" + parameter + "')))");

            }
            SQL_main.AddCommand("DELETE FROM Items_ItemNo WHERE Items_ID IN (SELECT ID FROM Items WHERE Parameters_ID IN (SELECT ID FROM Parameters WHERE Stock_ID = (SELECT TOP 1 ID FROM Stock WHERE Name ='" + stock_name + "') AND Name='" + parameter + "'))");
            SQL_main.AddCommand("DELETE FROM Items WHERE Parameters_ID IN (SELECT ID FROM Parameters WHERE Stock_ID = (SELECT TOP 1 ID FROM Stock WHERE Name ='" + stock_name + "') AND Name='" + parameter + "')");
            SQL_main.AddCommand("DELETE FROM Parameters WHERE Stock_ID = (SELECT TOP 1 ID FROM Stock WHERE Name ='" + stock_name + "') AND Name='" + parameter + "'");
            SQL_main.Commit_Transaction();
        }
        public static bool isSold(int stock_ID)
        {
            return isSold(stock_name(stock_ID));
        }

        public static bool isSold(string stock_name)
        {
            if (stock_name.LastIndexOf(" - predaj") > -1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public static bool isParameter(String parameter, string stock_name)
        {
            //Error.Show(parameter + " "  + stock_name);
            if (parameter == "" || stock_name == "")
            {
                return false;
            }
            DataTable dd;
            //Error.Show(parameter + " " + stock_name);
            dd = SQL_main.List("SELECT * FROM Parameters WHERE Stock_ID = (SELECT TOP 1 ID FROM Stock WHERE Name ='" + stock_name + "') AND Name='" + parameter + "'").Tables[0];
            if (dd.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void Rename(string premenovat, string parameter, string stock_name)
        {
            SQL_main.Odpalovac("UPDATE Parameters SET Name = '" + premenovat + "' WHERE Stock_ID = (SELECT TOP 1 ID FROM Stock WHERE Name ='" + stock_name + "') AND Name='" + parameter + "'");
        }

        public static Dictionary<String, String> getSearch(String stock_name)
        {
            DataTable dd;
            dd = SQL_main.List("SELECT p.Name, p.Search FROM Parameters p JOIN Stock s ON p.Stock_ID  = s.ID WHERE s.Name = '" + stock_name + "'").Tables[0];
            Dictionary<String, String> dictionary = new Dictionary<String, String>();
            for (int i = 0; i < dd.Rows.Count; i++)
            {
                dictionary.Add(dd.Rows[i]["Name"].ToString(), dd.Rows[i]["Search"].ToString());
            }
            return dictionary;
        }

        public static void reset_wrecked(ProgressBar pgbar)
        {
            export_wrecked(pgbar);
            if (Error.Question("Prebehol export v poriadku a chcete resetovat zlomene?"))
            {
                SQL_main.Odpalovac("INSERT INTO Count(Count, Wrecked, Date, Employer_ID, ItemNo_ID) SELECT Count, 0, GETDATE(), c.Employer_ID, c.ItemNo_ID FROM Count c JOIN (select Employer_ID,ItemNo_ID, MAX(Date) Date from count where Wrecked >0  AND Employer_ID IS NOT NULL group by ItemNo_ID, Employer_id) cc ON cc.ItemNo_ID = c.ItemNo_ID AND cc.Date = c.Date  AND c.Employer_ID = cc.Employer_ID");
                SQL_main.Odpalovac("UPDATE Dates SET date = GETDATE() WHERE name = 'reset wrecked'");
            }
        }

        public static void export_wrecked(ProgressBar pgbar)
        {
            Stock_SQL.export_stock_change(DateTime.Now, DateTime.Now, pgbar, Stock_SQL.export_single_wrecked_stock);


            //DataTable dt = SQL_main.List("SELECT Count, 0, GETDATE(), c.Employer_ID, c.ItemNo_ID FROM Count c JOIN (select Employer_ID,ItemNo_ID, MAX(Date) Date from count where Wrecked >0  AND Employer_ID IS NOT NULL group by ItemNo_ID, Employer_id) cc ON cc.ItemNo_ID = c.ItemNo_ID AND cc.Date = c.Date  AND c.Employer_ID = cc.Employer_ID").Tables[0];
            //Export.export(dt);
        }

        public static void export_stock_change(DateTime since, DateTime until, ProgressBar pgbar, Func<string, string, int, DateTime, DateTime, DataTable> single_dt_function)
        {
            List<DataSet> data_zamestnancov = new List<DataSet>();

            DataSet all_stocks = export_someone_stock(null, null, since, until, single_dt_function);
            all_stocks.DataSetName = "Sklad";
            data_zamestnancov.Add(all_stocks);

            DataTable list_zamestnancov = Employer.List_zamestnanci().Tables[0];
            pgbar.Maximum = list_zamestnancov.Rows.Count;
            pgbar.Value = 0;

            foreach (DataRow zamestnanec_row in list_zamestnancov.Rows)
            {
                string name = zamestnanec_row["name"].ToString();
                string surname = zamestnanec_row["surname"].ToString();
                all_stocks = export_someone_stock(name, surname, since, until, single_dt_function);
                data_zamestnancov.Add(all_stocks);
                pgbar.Value++;
            }
            Export.export(data_zamestnancov);

        }

        public static void export_monthly(int employer_id, DateTime since, DateTime until, ProgressBar pgbar)
        {
            List<DataSet> data_skladov = new List<DataSet>();
            DataTable all_stocks = List().Tables[0];
            DataSet prijem = new DataSet("Prijem");
            DataSet vydaj = new DataSet("Vydaj");

            data_skladov.Add(prijem);
            data_skladov.Add(vydaj);

            pgbar.Maximum = all_stocks.Rows.Count;
            pgbar.Value = 0;

            foreach (DataRow sklad_row in all_stocks.Rows)
            {
                int stock_id = int.Parse(sklad_row["ID"].ToString());
                string name = sklad_row["name"].ToString();
                DataSet ds_positive = export_monthly_stock(stock_id, employer_id, since, until, true);
                DataTable stock_positive = ds_positive.Tables[0];
                stock_positive.TableName = name;
                ds_positive.Tables.Remove(stock_positive);
                prijem.Tables.Add(stock_positive);

                DataSet ds_negative = export_monthly_stock(stock_id, employer_id, since, until, false);
                DataTable stock_negative = ds_negative.Tables[0];
                stock_negative.TableName = name;
                ds_negative.Tables.Remove(stock_negative);
                vydaj.Tables.Add(stock_negative);

                pgbar.Value++;
            }

            Export.export(data_skladov);
        }

        public static DataSet export_monthly_stock(int stock_ID, int employer_ID, DateTime since, DateTime until, Boolean positive)
        {
            if (SQL_main.List("SELECT * FROM Parameters p WHERE p.Stock_ID = " + stock_ID).Tables[0].Rows.Count > 0)
            {
                SQL_main.AddCommand("DECLARE @cols AS NVARCHAR(1000), @colsname AS NVARCHAR(1000), @query  AS NVARCHAR(MAX), @StockID AS NVARCHAR(20)");

                SQL_main.AddCommand("SET @StockID=" + stock_ID);

                SQL_main.AddCommand("SET @cols = ''");
                SQL_main.AddCommand("SET @colsname = ''");
                SQL_main.AddCommand("SELECT @cols= COALESCE( @cols + '[', '') + CAST(ID as nvarchar) + '] ,' FROM Parameters p WHERE p.Stock_ID = @StockID");
                SQL_main.AddCommand("SELECT @colsname = COALESCE( @colsname + '[', '') + CAST(ID as nvarchar) + '] [' + name + '], ' FROM Parameters p WHERE p.Stock_ID = @StockID");

                SQL_main.AddCommand("SET @cols=LEFT(@cols, LEN(@cols)-1)");
                SQL_main.AddCommand("SET @colsname=LEFT(@colsname,LEN(@colsname)-1)");

                //SQL_main.AddCommand("SET @query = 'SELECT ISNULL((E.name + E.surname), ''Sklad'') Meno, ' + @colsname + ', ");
                SQL_main.AddCommand("SET @query = 'SELECT ' + @colsname + ', ");

                SQL_main.AddCommand("~CNT.Count Pocet, CNT.Wrecked Znicene");
                SQL_main.AddCommand("~, CAST(Cst.Cost AS NVARCHAR(15)) + '' €'' AS Cena, S.Name Regal, (SELECT nnn.Name + ''; '' AS [text()] FROM Notes nnn WHERE nnn.ItemNo_ID = Itms.ItemNo_ID AND nnn.Date > ''" + Helper.DateSQL(since) + "'' AND nnn.Date < ''" + Helper.DateSQL(until) + "'' FOR XML PATH('''') ) Poznámky FROM  (");

                SQL_main.AddCommand("~SELECT II.ItemNo_ID, I.Name, I.Parameters_ID, INo.Photo, INo.Shelf_ID, INo.Stock_ID FROM Items_ItemNo II INNER JOIN Items I ON I.ID=II.Items_ID");
                SQL_main.AddCommand("~INNER JOIN ItemNo INo ON INo.ID=II.ItemNo_ID WHERE ");
                SQL_main.AddCommand("~ INo.Stock_ID=' + @StockID + '");
                SQL_main.AddCommand("~)x pivot(");
                SQL_main.AddCommand("~max(Name)");
                SQL_main.AddCommand("~for Parameters_ID in (' + @cols + ')");
                SQL_main.AddCommand("~) Itms LEFT JOIN Shelf S ON S.ID=Itms.Shelf_ID");
                SQL_main.AddCommand("~JOIN (SELECT c.ItemNo_ID, SUM(c.Count - coalesce(previous.Count,0) + c.Wrecked - coalesce(previous.Wrecked,0)) Count, SUM(c.Wrecked - coalesce(previous.Wrecked,0)) Wrecked FROM Count c");
                SQL_main.AddCommand("~OUTER APPLY(SELECT top 1 * FROM Count c_prev WHERE ");
                //SQL_main.AddCommand("~c.Employer_ID = c_prev.Employer_ID OR");
                if (employer_ID == -1)
                {
                    if (positive)
                    {
                        SQL_main.AddCommand("~c_prev.Employer_ID IS NULL AND");
                    }
                    else
                    {
                        SQL_main.AddCommand("~c_prev.Employer_ID = c.Employer_ID AND");
                    }

                }
                else if (employer_ID != -1)
                {
                    SQL_main.AddCommand("~c.Employer_ID = " + employer_ID.ToString() + " AND c_prev.Employer_ID = " + employer_ID.ToString() + " AND ");
                }
                SQL_main.AddCommand("~ c_prev.Date < c.Date AND c.ItemNo_ID = c_prev.ItemNo_ID");
                SQL_main.AddCommand("~ORDER BY c_prev.Date DESC) previous");

                if (positive)
                {
                    SQL_main.AddCommand("~WHERE (c.Count >= previous.Count OR c.Wrecked > previous.Wrecked OR previous.ItemNo_ID IS NULL) AND c.Employer_ID IS NULL AND previous.Employer_ID IS NULL");
                    SQL_main.AddCommand("~AND c.Date > ''" + Helper.DateSQL(since) + "'' AND c.Date < ''" + Helper.DateSQL(until) + "''");
                    SQL_main.AddCommand("~GROUP BY c.ItemNo_ID, c.Employer_ID) CNT ON Cnt.ItemNo_ID=Itms.ItemNo_ID");

                }
                else
                {
                    SQL_main.AddCommand("~WHERE (c.Count >= previous.Count OR c.Wrecked > previous.Wrecked OR previous.ItemNo_ID IS NULL) AND c.Employer_ID IS NOT NULL");
                    SQL_main.AddCommand("~AND c.Date > ''" + Helper.DateSQL(since) + "'' AND c.Date < ''" + Helper.DateSQL(until) + "''");
                    SQL_main.AddCommand("~GROUP BY c.ItemNo_ID) CNT ON Cnt.ItemNo_ID=Itms.ItemNo_ID");
                }

                SQL_main.AddCommand("~LEFT JOIN Cost Cst ON Cst.ItemNo_ID=Itms.ItemNo_ID AND Cst.Until IS NULL ");
                //SQL_main.AddCommand("~LEFT JOIN Employer E ON E.ID = CNT.Employer_ID");
                //SQL_main.AddCommand("~ORDER BY E.ID");
                SQL_main.AddCommand("~;'");

                SQL_main.AddCommand("EXECUTE(@query)");


                try
                {
                    return SQL_main.Commit_List();
                }
                catch (Exception ex)
                {
                    Error.Show(ex);
                }
            }

            return null;
        }

        public static DataSet export_someone_stock(string name, string surname, DateTime since, DateTime until, Func<string, string, int, DateTime, DateTime, DataTable> single_dt_function)
        {
            DataTable dt = List().Tables[0];
            DataSet all_stocks = new DataSet("All_stocks");
            foreach (DataRow dr in dt.Rows)
            {
                int stock_id = int.Parse(dr[0].ToString());
                string stock_name = dr[1].ToString();
                DataTable single_stock = single_dt_function(name, surname, stock_id, since, until);
                single_stock.TableName = stock_name;

                all_stocks.Tables.Add(single_stock);
            }
            if (name != null) all_stocks.DataSetName = name + " " + surname;

            return all_stocks;
        }


        public static DataTable export_single_stock(string name, string surname, int stock_id, DateTime since, DateTime until)
        {
            DataTable dt_since = Employer_Stock.List(name, surname, stock_id, since, 0, 0).Tables[0];
            DataTable dt_until = Employer_Stock.List(name, surname, stock_id, until, 0, 0).Tables[0];
            DataTable res = merge_tables(dt_since, dt_until);
            return res;
        }

        public static DataTable merge_tables(DataTable dt_since, DataTable dt_until)
        {
            DataTable dt_res = dt_until.Clone();
            dt_res.Rows.Clear();

            foreach (DataRow dr in dt_until.Rows)
            {
                int item_no = int.Parse(dr["ItemNo_ID"].ToString());
                //MessageBox.(dr["Pocet"].ToString());
                //Na predaj


                Decimal count = get_count_from_row(dr);

                Decimal count_new = 0;
                DataRow[] d_f = dt_since.Select("ItemNo_ID = " + item_no);
                if (d_f.Count() > 0)
                {
                    count_new = get_count_from_row(d_f[0]);
                }

                if (count_new != count)
                {
                    change_count_from_row(dr, (count - count_new));
                    dt_res.ImportRow(dr);
                }

            }


            return dt_res;
        }

        public static DataTable export_single_wrecked_stock(string name, string surname, int stock_id, DateTime since, DateTime until)
        {
            DataTable dt_until = Employer_Stock.List(name, surname, stock_id, until, 0, 0).Tables[0];
            DataTable dt_res = dt_until.Clone();
            dt_res.Rows.Clear();

            foreach (DataRow dr in dt_until.Rows)
            {
                Decimal count = get_wrecked_from_row(dr);
                if (count != 0)
                {
                    dt_res.ImportRow(dr);
                }

            }

            return dt_res;
        }

        public static Decimal get_wrecked_from_row(DataRow dr)
        {
            if (dr.Table.Columns.Contains("Znicene"))
            {
                return Decimal.Parse(dr["Znicene"].ToString());
            }

            if (dr.Table.Columns.Contains("Predane"))
            {
                return Decimal.Parse(dr["Predane"].ToString());
            }

            throw new Exception("nepodareny export");

        }

        public static Decimal get_count_from_row(DataRow dr)
        {
            if (dr.Table.Columns.Contains("Pocet"))
            {
                return Decimal.Parse(dr["Pocet"].ToString());
            }

            if (dr.Table.Columns.Contains("Na predaj"))
            {
                return Decimal.Parse(dr["Na predaj"].ToString());
            }

            throw new Exception("nepodareny export");

        }

        public static DataRow change_count_from_row(DataRow dr, Decimal value)
        {
            if (dr.Table.Columns.Contains("Pocet"))
            {
                dr["Pocet"] = value;
            }

            if (dr.Table.Columns.Contains("Na predaj"))
            {
                dr["Na predaj"] = value;
            }
            return dr;

        }

        public static Boolean should_reset()
        {
            DataTable dt = SQL_main.List("SELECT Date FROM Dates WHERE name = 'reset wrecked'").Tables[0];
            DateTime last = DateTime.Parse(dt.Rows[0]["Date"].ToString());

            if (last.AddMonths(3).CompareTo(DateTime.Now) < 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

    }
}
