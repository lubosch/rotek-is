﻿using RotekIS.General;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;

namespace RotekIS.SQL
{
    public class Employer_Stock
    {
        public static DataSet List(string query, int limit, int offset)
        {
            return List(null, null, -1, query, DateTime.Now, limit, offset);

        }

        public static DataSet List(string name, string surname, int stock_ID, DateTime dt, int limit, int offset)
        {
            return List(name, surname, stock_ID, "", dt, limit, offset);
        }

        public static Boolean IdentifySold(int stock_ID)
        {
            if (stock_ID == -1) { return false; }

            String name = Stock_SQL.stock_name(stock_ID);
            if (name.LastIndexOf(" - predaj") > -1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public static DataSet List(string name, string surname, int stock_ID, string query, DateTime until, int limit, int offset)
        {

            if (stock_ID == -1 || SQL_main.List("SELECT * FROM Parameters p WHERE p.Stock_ID = " + stock_ID).Tables[0].Rows.Count > 0)
            {
                Boolean sold = IdentifySold(stock_ID);
                SQL_main.AddCommand("DECLARE @cols AS NVARCHAR(4000), @colsname AS NVARCHAR(4000), @query  AS NVARCHAR(MAX), @StockID AS NVARCHAR(20)");
                SQL_main.AddCommand("DECLARE @EID as NVARCHAR(20)");
                SQL_main.AddCommand("SET @EID = (SELECT TOP 1 ID FROM Employer WHERE Surname= '" + surname + "' AND Name= '" + name + "')");


                SQL_main.AddCommand("SET @StockID=" + stock_ID);

                SQL_main.AddCommand("SET @cols = ''");
                SQL_main.AddCommand("SET @colsname = ''");
                if (stock_ID == -1)
                {
                    SQL_main.AddCommand("SELECT @cols= COALESCE( @cols + '[', '') + CAST(ID as nvarchar) + '] ,' FROM Parameters p ");
                    SQL_main.AddCommand("SELECT @colsname = COALESCE( @colsname + '[', '') + CAST(ID as nvarchar) + '] [' + name + '], ' FROM Parameters p ");
                }
                else
                {
                    SQL_main.AddCommand("SELECT @cols= COALESCE( @cols + '[', '') + CAST(ID as nvarchar) + '] ,' FROM Parameters p WHERE p.Stock_ID = @StockID");
                    SQL_main.AddCommand("SELECT @colsname = COALESCE( @colsname + '[', '') + CAST(ID as nvarchar) + '] [' + name + '], ' FROM Parameters p WHERE p.Stock_ID = @StockID");
                }
                SQL_main.AddCommand("SET @cols=LEFT(@cols, LEN(@cols)-1)");
                SQL_main.AddCommand("SET @colsname=LEFT(@colsname,LEN(@colsname)-1)");

                SQL_main.AddCommand("SET @query = 'SELECT ' + @colsname + ', ");

                if (sold)
                {
                    SQL_main.AddCommand("~Count.Count ''Na predaj'', Count.Wrecked Predane");
                }
                else
                {
                    SQL_main.AddCommand("~Count.Count Pocet, Count.Wrecked Znicene");
                }
                SQL_main.AddCommand("~, CAST(Cst.Cost AS NVARCHAR(15)) + '' €'' AS Cena, Photo, S.Name Regal, Itms.ItemNo_ID, Stock_ID, (SELECT nnn.Name + ''; '' AS [text()] FROM Notes nnn WHERE nnn.ItemNo_ID = Itms.ItemNo_ID FOR XML PATH('''') ) Poznámky FROM  (");

                SQL_main.AddCommand("~SELECT II.ItemNo_ID, I.Name, I.Parameters_ID, INo.Photo, INo.Shelf_ID, INo.Stock_ID FROM Items_ItemNo II INNER JOIN Items I ON I.ID=II.Items_ID");
                SQL_main.AddCommand("~INNER JOIN ItemNo INo ON INo.ID=II.ItemNo_ID WHERE ");
                if (stock_ID == -1)
                {
                    SQL_main.AddCommand("~ II.ItemNo_ID IN(SELECT II.ItemNo_ID FROM Items I JOIN Items_ItemNo II ON II.Items_ID = I.ID WHERE I.Name LIKE ''%" + query + "%'')");
                }
                else
                {
                    SQL_main.AddCommand("~ INo.Stock_ID=' + @StockID + '");

                }
                SQL_main.AddCommand("~)x pivot(");
                SQL_main.AddCommand("~max(Name)");
                SQL_main.AddCommand("~for Parameters_ID in (' + @cols + ')");
                SQL_main.AddCommand("~) Itms LEFT JOIN Shelf S ON S.ID=Itms.Shelf_ID LEFT JOIN (SELECT ItemNo_ID, MAX(Date) as Date FROM COUNT WHERE");
                if (name == null)
                {
                    SQL_main.AddCommand("~Employer_ID IS NULL");
                }
                else
                {
                    SQL_main.AddCommand("~Employer_ID =' + @EID + '");
                }
                SQL_main.AddCommand("~AND Date < ''" + Helper.DateSQL(until) + "'' GROUP BY ItemNo_ID ) CNT ON Cnt.ItemNo_ID=Itms.ItemNo_ID");

                SQL_main.AddCommand("~INNER JOIN Count ON Count.ItemNo_ID=Cnt.ItemNo_ID AND Count.Date=Cnt.Date");
                SQL_main.AddCommand("~LEFT JOIN Cost Cst ON Cst.ItemNo_ID=Itms.ItemNo_ID AND Cst.Until IS NULL ");
                if (name == null)
                {
                    SQL_main.AddCommand("~WHERE Count.Employer_ID IS NULL");
                }
                else
                {
                    SQL_main.AddCommand("~WHERE (Count!=0 OR Wrecked!=0) AND Count.Employer_ID= ' + @EID + ' ");
                }

                //SQL_main.AddCommand("~ORDER BY Itms.ItemNo_ID OFFSET " + (limit * offset) + " ROWS FETCH NEXT " + limit + " ROWS ONLY");
                SQL_main.AddCommand("~;'");


                SQL_main.AddCommand("EXECUTE(@query)");


                try
                {
                    return SQL_main.Commit_List();
                }
                catch (Exception ex)
                {
                    Error.Show(ex);
                }
            }
            return null;

        }

        public static void AddItem(int stockID, string[] values, string note, string photo, string shelf, decimal count, decimal wrecked, decimal cost, int mode)
        {
            int i = 0;
            int[] parametersID = Stock_SQL.GetParameters(stockID).Keys.ToArray<int>();

            SQL_main.AddCommand("DECLARE @ItemNoId int, @Shelf_ID int");
            SQL_main.AddCommand("SET @Shelf_ID=NULL");
            if (shelf != null)
            {
                SQL_main.AddCommand("SET @Shelf_ID = ( SELECT ID FROM Stock WHERE Name='" + shelf + "')");
                SQL_main.AddCommand("IF ( @Shelf_ID IS NULL ) BEGIN");
                SQL_main.AddCommand("INSERT INTO Shelf (Name) VALUES ('" + shelf + "')");
                SQL_main.AddCommand("SET @Shelf_ID=scope_identity()");
                SQL_main.AddCommand("END");
            }

            SQL_main.AddCommand("SET @ItemNoId =(SELECT MIN(ItemNo_ID) FROM Items_ItemNo II INNER JOIN Items I ON I.ID = II.Items_ID WHERE( ");

            string podmienka1 = "", podmienka2 = "";


            for (i = 0; i < parametersID.Length; i++)
            {
                podmienka1 += ("I.Name = '" + values[i] + "' ");
                podmienka2 += ("I.Parameters_ID = " + parametersID[i]);
                if (i + 1 != parametersID.Length)
                {
                    podmienka1 += (" OR ");
                    podmienka2 += (" OR ");
                }
            }

            SQL_main.AddCommand("~" + podmienka1 + " ) AND ( " + podmienka2 + ") GROUP BY ItemNo_ID HAVING COUNT(I.Name)=" + parametersID.Length + ")");


            //DECLARE @ID int;
            //set @Id=(SELECT  ItemNo_ID FROM Items_ItemNo II INNER JOIN Items I ON I.ID=II.Items_ID WHERE (Name='aa' or Name='vv' or Name='mm') AND (Parameters_ID = 1 OR Parameters_ID=2 or Parameters_ID=3) GROUP BY ItemNo_ID HAVING COUNT(I.Name)=@Ex)
            //print @ID


            SQL_main.AddCommand("IF (@ItemNoId IS NULL) BEGIN");

            SQL_main.AddCommand("INSERT INTO ItemNo (Stock_ID, Shelf_ID, Photo) VALUES (" + stockID + ", @Shelf_ID, @Photo)");
            SQL_main.AddParameter("@Photo", photo);

            SQL_main.AddCommand("SELECT @ItemNoId = scope_identity()");

            SQL_main.AddCommand("DECLARE @ItemsId int");


            i = 0;

            for (i = 0; i < parametersID.Length; i++)
            {

                SQL_main.AddCommand("SET @ItemsId = (SELECT TOP 1 (ID) FROM Items WHERE Name = '" + values[i] + "' AND Parameters_ID = " + parametersID[i] + ")");
                SQL_main.AddCommand("IF (@ItemsId IS NULL) BEGIN");
                SQL_main.AddCommand("INSERT INTO Items (Name, Parameters_ID) VALUES ('" + values[i] + "'," + parametersID[i] + ")");
                SQL_main.AddCommand("SELECT @ItemsId = scope_identity()");
                SQL_main.AddCommand("END");
                SQL_main.AddCommand("INSERT INTO Items_ItemNo (ItemNo_Id, Items_Id) VALUES (@ItemNoId, @ItemsId)");
            }

            SQL_main.AddCommand("INSERT INTO Count (Count, Wrecked, Date, ItemNo_ID) VALUES (" + count.ToString().Replace(',', '.') + ", " + wrecked.ToString().Replace(',', '.') + ", GETDATE(), @ItemNoId" + ")");
            if (cost != 0)
            {
                SQL_main.AddCommand("INSERT INTO Cost (Cost, Since, ItemNo_ID) VALUES (" + cost.ToString().Replace(',', '.') + ", GETDATE(), @ItemNoId" + ")");
            }
            if (note != null && note.Trim().Length > 0)
            {
                SQL_main.AddCommand("INSERT INTO Notes (Name, Date, ItemNo_ID) VALUES ('" + note + "', GETDATE(), @ItemNoId" + ")");
            }

            SQL_main.AddCommand("END");

            SQL_main.AddCommand("ELSE BEGIN");
            SQL_main.AddCommand("Print @ItemNoId");

            if (count < 0)
            {
                //SQL_main.AddCommand("INSERT INTO Count (COUNT, WRECKED, Date, ItemNo_ID) SELECT TOP 1 (Count + " + count.ToString().Replace(',', '.') + ") , (Wrecked + " + wrecked.ToString().Replace(',', '.') + "), GETDATE(), @ItemNoID  FROM COUNT WHERE ItemNo_ID=@ItemNoId AND Employer_ID IS NULL ORDER BY Date DESC");
                SQL_main.AddCommand("UPDATE Count SET Count = (Count + " + count.ToString().Replace(',', '.') + ") WHERE ID = (SELECT TOP 1 ID FROM Count WHERE ItemNo_id = @ItemNoId AND Employer_ID IS NULL ORDER BY Date DESC)");
            }
            else
            {
                SQL_main.AddCommand("INSERT INTO Count (COUNT, WRECKED, Date, ItemNo_ID) SELECT TOP 1 (Count + " + count.ToString().Replace(',', '.') + ") , (Wrecked + " + wrecked.ToString().Replace(',', '.') + "), GETDATE(), @ItemNoID  FROM COUNT WHERE ItemNo_ID=@ItemNoId AND Employer_ID IS NULL ORDER BY Date DESC");
            }
            if (photo != null)
            {
                SQL_main.AddCommand("UPDATE ItemNo SET Photo='" + photo + "' WHERE ID=@ItemNoID");
            }
            if (shelf != null)
            {
                SQL_main.AddCommand("UPDATE ItemNo SET Shelf_ID = @Shelf_ID WHERE ID=@ItemNoID");
            }

            if (cost != 0)
            {
                SQL_main.AddCommand("UPDATE Cost SET Until=GETDATE() WHERE Until IS NULL AND ItemNo_ID=@ItemNoID");
                SQL_main.AddCommand("INSERT INTO Cost (Cost, Since, ItemNo_ID) VALUES (" + cost.ToString().Replace(',', '.') + ", GETDATE(), @ItemNoID)");
            }
            if (note != null && note.Trim().Length > 0)
            {
                SQL_main.AddCommand("INSERT INTO Notes (Name, Date, ItemNo_ID) VALUES ('" + note + "', GETDATE(), @ItemNoID)");
            }

            //SQL_main.AddCommand("UPDATE Count SET Count=Count + " + count + " WHERE ItemNo_ID=@ItemNoId AND Employer_ID IS NULL");

            SQL_main.AddCommand("END");

            SQL_main.Commit_Transaction();

        }


        public static void AddItemEmployer(string name, string surname, int stockID, string[] values, decimal count, decimal wrecked)
        {
            int[] parametersID = Stock_SQL.GetParameters(stockID).Keys.ToArray<int>();
            SQL_main.AddCommand("DECLARE @id_stock int, @id_employer int, @INo_ID int, @EID int");
            SQL_main.AddCommand("SET @EID=(SELECT TOP 1 ID FROM Employer WHERE Name='" + name + "' AND Surname='" + surname + "')");
            SQL_main.AddCommand("SET @INo_ID =(SELECT II.ItemNo_ID FROM Items I JOIN Items_ItemNo II ON II.Items_ID = I.ID WHERE ");

            for (int i = 0; i < parametersID.Length; i++)
            {
                SQL_main.AddCommand("~(I.Name= '" + values[i] + "' AND I.Parameters_ID= " + parametersID[i] + " )");
                if (i != parametersID.Length - 1) SQL_main.AddCommand("~OR");
            }
            SQL_main.AddCommand("~GROUP BY II.ItemNo_ID  having COUNT(II.ItemNo_ID)=" + parametersID.Length + ")");



            SQL_main.AddCommand("SET @id_employer=(select TOP 1 C.ID from Count C where C.Employer_ID = @EID AND C.ItemNo_ID = @INo_ID ORDER BY Date DESC)");
            SQL_main.AddCommand("SET @id_stock=(select TOP 1 C.ID from Count C where C.Employer_ID is null AND C.ItemNo_ID = @INo_ID ORDER BY Date DESC)");

            SQL_main.AddCommand("IF (@id_stock IS NULL) begin");
            SQL_main.AddCommand("Select 'ERROR: Not found'");
            SQL_main.AddCommand("END ELSE BEGIN");
            if (wrecked == 0)
            {
                SQL_main.AddCommand("INSERT INTO Count (Count, Wrecked, Date, ItemNo_ID) SELECT TOP 1 Count - " + count.ToString().Replace(',', '.') + ", Wrecked, GETDATE(), @INo_ID FROM Count WHERE ID=@id_stock ");
                SQL_main.AddCommand("WAITFOR DELAY '00:00:00.002'");
            }
            SQL_main.AddCommand("IF (@id_employer IS NULL) BEGIN");
            SQL_main.AddCommand("INSERT INTO Count (Count, Wrecked, Employer_ID, Date, ItemNo_ID ) VALUES (" + count.ToString().Replace(',', '.') + "," + wrecked + ", @EID, GETDATE(), @INo_ID)");
            SQL_main.AddCommand("END ELSE BEGIN");
            SQL_main.AddCommand("INSERT INTO COUNT (Count, Wrecked, Date, Employer_ID, ItemNo_ID) SELECT TOP 1 Count + " + count.ToString().Replace(',', '.') + ", Wrecked + " + wrecked + " , GETDATE(), @EID, @INo_ID FROM Count WHERE ID=@id_employer");
            SQL_main.AddCommand("END");
            SQL_main.AddCommand("END");
            try
            {
                SQL_main.Commit_Transaction();
            }
            catch (MS_SQL_Exception e)
            {
                throw e;
            }

        }

        public static void AddItemEmployer_Export(string name, string surname, int stockID, string[] values, decimal count, decimal wrecked)
        {
            int[] parametersID = Stock_SQL.GetParameters(stockID).Keys.ToArray<int>();
            SQL_main.AddCommand("DECLARE @id_stock int, @id_employer int, @INo_ID int, @EID int");
            SQL_main.AddCommand("SET @EID=(SELECT TOP 1 ID FROM Employer WHERE Name='" + name + "' AND Surname='" + surname + "')");
            SQL_main.AddCommand("SET @INo_ID =(SELECT II.ItemNo_ID FROM Items I JOIN Items_ItemNo II ON II.Items_ID = I.ID WHERE ");

            for (int i = 0; i < parametersID.Length; i++)
            {
                SQL_main.AddCommand("~(I.Name= '" + values[i] + "' AND I.Parameters_ID= " + parametersID[i] + " )");
                if (i != parametersID.Length - 1) SQL_main.AddCommand("~OR");
            }
            SQL_main.AddCommand("~GROUP BY II.ItemNo_ID  having COUNT(II.ItemNo_ID)=" + parametersID.Length + ")");

            SQL_main.AddCommand("SET @id_employer=(select TOP 1 C.ID from Count C where C.Employer_ID = @EID AND C.ItemNo_ID = @INo_ID ORDER BY Date DESC)");
            SQL_main.AddCommand("SET @id_stock=(select TOP 1 C.ID from Count C where C.Employer_ID is null AND C.ItemNo_ID = @INo_ID ORDER BY Date DESC)");

            SQL_main.AddCommand("IF (@id_stock IS NULL) begin");
            SQL_main.AddCommand("Select 'ERROR: Not found'");
            SQL_main.AddCommand("END ELSE BEGIN");
            SQL_main.AddCommand("IF (@id_employer IS NULL) BEGIN");
            SQL_main.AddCommand("INSERT INTO Count (Count, Wrecked, Employer_ID, Date, ItemNo_ID ) VALUES (" + count.ToString().Replace(',', '.') + "," + wrecked + ", @EID, GETDATE(), @INo_ID)");
            SQL_main.AddCommand("END ELSE BEGIN");
            SQL_main.AddCommand("INSERT INTO COUNT (Count, Wrecked, Date, Employer_ID, ItemNo_ID) SELECT TOP 1 Count + " + count.ToString().Replace(',', '.') + ", Wrecked + " + wrecked + " , GETDATE(), @EID, @INo_ID FROM Count WHERE ID=@id_employer");
            SQL_main.AddCommand("END");
            SQL_main.AddCommand("END");
            try
            {
                SQL_main.Commit_Transaction();
            }
            catch (MS_SQL_Exception e)
            {
                throw e;
            }

        }

        public static void Disappear(int ino_to_change, decimal count)
        {


            SQL_main.AddCommand("UPDATE Count SET Count = (Count - " + count.ToString().Replace(',', '.') + ") WHERE ID = (SELECT TOP 1 ID FROM Count WHERE ItemNo_id = " + ino_to_change +  " ORDER BY Date DESC)"); 


            //SQL_main.AddCommand("INSERT INTO Count (COUNT, WRECKED, Date, ItemNo_ID) SELECT TOP 1 (Count - " + count.ToString().Replace(',', '.') + ") , (Wrecked), GETDATE(), ItemNo_ID  FROM COUNT WHERE ItemNo_ID=" + ino_to_change + " AND Employer_ID IS NULL ORDER BY Date DESC");
            try
            {
                SQL_main.Commit_Transaction();
            }
            catch (MS_SQL_Exception e)
            {
                throw e;
            }
        }

        public static void MoveToAnotherStock(int ino_to_change, string[] values, int stockID)
        {
            SQL_main.AddCommand("DELETE FROM Items_ItemNo WHERE ItemNo_ID = " + ino_to_change);
            int[] parametersID = Stock_SQL.GetParameters(stockID).Keys.ToArray<int>();

            SQL_main.AddCommand("DECLARE @ItemsId int");
            for (int i = 0; i < parametersID.Length; i++)
            {

                SQL_main.AddCommand("SET @ItemsId = (SELECT TOP 1 (ID) FROM Items WHERE Name = '" + values[i] + "' AND Parameters_ID = " + parametersID[i] + ")");
                SQL_main.AddCommand("IF (@ItemsId IS NULL) BEGIN");
                SQL_main.AddCommand("INSERT INTO Items (Name, Parameters_ID) VALUES ('" + values[i] + "'," + parametersID[i] + ")");
                SQL_main.AddCommand("SELECT @ItemsId = scope_identity()");
                SQL_main.AddCommand("END");
                SQL_main.AddCommand("INSERT INTO Items_ItemNo (ItemNo_Id, Items_Id) VALUES (" + ino_to_change + ", @ItemsId)");
            }
            SQL_main.AddCommand("UPDATE ItemNo SET Stock_ID = " + stockID + " WHERE ID = " + ino_to_change);

            try
            {
                SQL_main.Commit_Transaction();
            }
            catch (MS_SQL_Exception e)
            {
                throw e;
            }

        }



        public static DataSet get_Notes(int itemNo)
        {
            return SQL_main.List("SELECT ID ID, (CONVERT(varchar(10), [Date], 20) + ': '+ Name) Note from notes where ItemNo_ID=" + itemNo.ToString());
        }
        public static void add_Note(int itemNo, string Text)
        {
            SQL_main.Odpalovac("INSERT INTO Notes(Name, Date, ItemNo_ID) VALUES ('" + Text + "', GETDATE(), " + itemNo.ToString() + ")");
        }
        public static void remove_Note(int ID)
        {
            SQL_main.Odpalovac("DELETE FROM Notes WHERE ID = " + ID);
        }

    }
}
