﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace RotekIS.Forms
{
    public partial class ExportDatesDialog : RotekIS.Stocks.Inter_Form
    {
        public string nadpis { set; get; }
        public DateTime date_from { set; get; }
        public DateTime date_to { set; get; }
        public string popis { set; get; }


        public ExportDatesDialog(string nadpis)
            : base(nadpis)
        {
            InitializeComponent();
        }

        private void my_Button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void my_Button1_Click(object sender, EventArgs e)
        {
            date_from = dateTimePicker2.Value;
            date_to = dateTimePicker3.Value;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void TextDialog_Load(object sender, EventArgs e)
        {
        }
    }
}
