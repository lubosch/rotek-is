﻿using RotekIS.Forms;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RotekIS
{
    class my_Button : Button
    {
        public my_Button()
        {
            this.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FlatAppearance.BorderSize = 0;
            this.BackColor = Color.Transparent;
            this.BackgroundImage = RotekIS.Properties.Resources.Black;
            this.BackgroundImageLayout =  ImageLayout.Stretch;
            this.ForeColor = Color.WhiteSmoke;
            this.Font = new Font("Verdana",12, FontStyle.Bold) ;
            this.Size = new Size(140, 41);
            

        }

        protected override void OnMouseEnter(EventArgs e)
        {
            //base.OnMouseEnter(e);
            this.BackgroundImage = RotekIS.Properties.Resources.Black2;
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            this.BackgroundImage = RotekIS.Properties.Resources.Black;
        }
    }
}
