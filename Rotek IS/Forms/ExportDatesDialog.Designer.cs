﻿namespace RotekIS.Forms
{
    partial class ExportDatesDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExportDatesDialog));
            this.my_Button1 = new RotekIS.my_Button();
            this.my_Button2 = new RotekIS.my_Button();
            this.napis5 = new RotekIS.Forms.Napis();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.napis2 = new RotekIS.Forms.Napis();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.napis5);
            this.panel1.Controls.Add(this.dateTimePicker3);
            this.panel1.Controls.Add(this.napis2);
            this.panel1.Controls.Add(this.dateTimePicker2);
            this.panel1.Controls.Add(this.my_Button2);
            this.panel1.Controls.Add(this.my_Button1);
            this.panel1.Margin = new System.Windows.Forms.Padding(5);
            this.panel1.Size = new System.Drawing.Size(402, 166);
            // 
            // my_Button1
            // 
            this.my_Button1.BackColor = System.Drawing.Color.Transparent;
            this.my_Button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("my_Button1.BackgroundImage")));
            this.my_Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.my_Button1.FlatAppearance.BorderSize = 0;
            this.my_Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.my_Button1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.my_Button1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.my_Button1.Location = new System.Drawing.Point(199, 104);
            this.my_Button1.Margin = new System.Windows.Forms.Padding(4);
            this.my_Button1.Name = "my_Button1";
            this.my_Button1.Size = new System.Drawing.Size(187, 50);
            this.my_Button1.TabIndex = 1;
            this.my_Button1.Text = "OK";
            this.my_Button1.UseVisualStyleBackColor = false;
            this.my_Button1.Click += new System.EventHandler(this.my_Button1_Click);
            // 
            // my_Button2
            // 
            this.my_Button2.BackColor = System.Drawing.Color.Transparent;
            this.my_Button2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("my_Button2.BackgroundImage")));
            this.my_Button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.my_Button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.my_Button2.FlatAppearance.BorderSize = 0;
            this.my_Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.my_Button2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.my_Button2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.my_Button2.Location = new System.Drawing.Point(4, 104);
            this.my_Button2.Margin = new System.Windows.Forms.Padding(4);
            this.my_Button2.Name = "my_Button2";
            this.my_Button2.Size = new System.Drawing.Size(187, 50);
            this.my_Button2.TabIndex = 2;
            this.my_Button2.Text = "Cancel";
            this.my_Button2.UseVisualStyleBackColor = false;
            this.my_Button2.Click += new System.EventHandler(this.my_Button2_Click);
            // 
            // napis5
            // 
            this.napis5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.napis5.AutoSize = true;
            this.napis5.Font = new System.Drawing.Font("Verdana", 10F);
            this.napis5.Location = new System.Drawing.Point(205, 34);
            this.napis5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.napis5.Name = "napis5";
            this.napis5.Size = new System.Drawing.Size(40, 20);
            this.napis5.TabIndex = 21;
            this.napis5.Text = "Do:";
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePicker3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker3.Location = new System.Drawing.Point(209, 59);
            this.dateTimePicker3.Margin = new System.Windows.Forms.Padding(4);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.Size = new System.Drawing.Size(149, 26);
            this.dateTimePicker3.TabIndex = 20;
            // 
            // napis2
            // 
            this.napis2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.napis2.AutoSize = true;
            this.napis2.Font = new System.Drawing.Font("Verdana", 10F);
            this.napis2.Location = new System.Drawing.Point(24, 34);
            this.napis2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.napis2.Name = "napis2";
            this.napis2.Size = new System.Drawing.Size(41, 20);
            this.napis2.TabIndex = 19;
            this.napis2.Text = "Od:";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePicker2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker2.Location = new System.Drawing.Point(28, 59);
            this.dateTimePicker2.Margin = new System.Windows.Forms.Padding(4);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(163, 26);
            this.dateTimePicker2.TabIndex = 18;
            // 
            // ExportDatesDialog
            // 
            this.AcceptButton = this.my_Button1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.CancelButton = this.my_Button2;
            this.ClientSize = new System.Drawing.Size(403, 197);
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "ExportDatesDialog";
            this.Load += new System.EventHandler(this.TextDialog_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private my_Button my_Button1;
        private my_Button my_Button2;
        private Napis napis5;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private Napis napis2;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
    }
}
