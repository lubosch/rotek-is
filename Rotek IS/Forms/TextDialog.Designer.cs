﻿namespace RotekIS.Forms
{
    partial class TextDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TextDialog));
            this.my_Button1 = new RotekIS.my_Button();
            this.my_Button2 = new RotekIS.my_Button();
            this.napis1 = new RotekIS.Forms.Napis();
            this.textac1 = new RotekIS.Forms.Textac();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textac1);
            this.panel1.Controls.Add(this.napis1);
            this.panel1.Controls.Add(this.my_Button2);
            this.panel1.Controls.Add(this.my_Button1);
            this.panel1.Size = new System.Drawing.Size(390, 178);
            // 
            // my_Button1
            // 
            this.my_Button1.BackColor = System.Drawing.Color.Transparent;
            this.my_Button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("my_Button1.BackgroundImage")));
            this.my_Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.my_Button1.FlatAppearance.BorderSize = 0;
            this.my_Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.my_Button1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.my_Button1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.my_Button1.Location = new System.Drawing.Point(232, 122);
            this.my_Button1.Name = "my_Button1";
            this.my_Button1.Size = new System.Drawing.Size(140, 41);
            this.my_Button1.TabIndex = 1;
            this.my_Button1.Text = "OK";
            this.my_Button1.UseVisualStyleBackColor = false;
            this.my_Button1.Click += new System.EventHandler(this.my_Button1_Click);
            // 
            // my_Button2
            // 
            this.my_Button2.BackColor = System.Drawing.Color.Transparent;
            this.my_Button2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("my_Button2.BackgroundImage")));
            this.my_Button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.my_Button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.my_Button2.FlatAppearance.BorderSize = 0;
            this.my_Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.my_Button2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.my_Button2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.my_Button2.Location = new System.Drawing.Point(21, 122);
            this.my_Button2.Name = "my_Button2";
            this.my_Button2.Size = new System.Drawing.Size(140, 41);
            this.my_Button2.TabIndex = 2;
            this.my_Button2.Text = "Cancel";
            this.my_Button2.UseVisualStyleBackColor = false;
            this.my_Button2.Click += new System.EventHandler(this.my_Button2_Click);
            // 
            // napis1
            // 
            this.napis1.AutoSize = true;
            this.napis1.Font = new System.Drawing.Font("Verdana", 10F);
            this.napis1.Location = new System.Drawing.Point(18, 22);
            this.napis1.Name = "napis1";
            this.napis1.Size = new System.Drawing.Size(54, 17);
            this.napis1.TabIndex = 2;
            this.napis1.Text = "napis1";
            // 
            // textac1
            // 
            this.textac1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.textac1.Location = new System.Drawing.Point(21, 57);
            this.textac1.Multiline = true;
            this.textac1.Name = "textac1";
            this.textac1.Size = new System.Drawing.Size(351, 59);
            this.textac1.TabIndex = 0;
            // 
            // TextDialog
            // 
            this.AcceptButton = this.my_Button1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.CancelButton = this.my_Button2;
            this.ClientSize = new System.Drawing.Size(390, 203);
            this.Name = "TextDialog";
            this.Load += new System.EventHandler(this.TextDialog_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private my_Button my_Button1;
        private my_Button my_Button2;
        private Textac textac1;
        private Napis napis1;
    }
}
