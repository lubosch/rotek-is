﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace RotekIS.Forms
{
    public partial class TextDialog : RotekIS.Stocks.Inter_Form
    {
        public string nadpis { set; get; }
        public string text { set; get; }
        public string popis { set; get; }


        public TextDialog(string nadpis, string popis)
            : base(nadpis)
        {
            InitializeComponent();
            napis1.Text = popis;
        }

        private void my_Button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void my_Button1_Click(object sender, EventArgs e)
        {
            this.text = textac1.Text;
            if (text.Length > 0)
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                DialogResult = DialogResult.Cancel;
            }
            Close();
        }

        private void TextDialog_Load(object sender, EventArgs e)
        {
            textac1.Select();
        }
    }
}
