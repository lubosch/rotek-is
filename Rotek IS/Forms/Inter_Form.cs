﻿using RotekIS.General;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace RotekIS.Stocks
{
    public partial class Inter_Form : Form, IDisposable
    {
        private int mode;

        public Inter_Form()
        {
            InitializeComponent();

        }
        public Inter_Form(string Nadpis)
        {
            InitializeComponent();
            nadpis.Text = Nadpis;

        }



        private void Add_Item_Paint(object sender, PaintEventArgs e)
        {
            Form f = sender as Form;
            Button b = nadpis;
            b.Location = new Point(this.Width / 2 - b.Width / 2, -8);
            cross_out.Location = new Point((b.Location.X + 360), cross_out.Location.Y);
            System.Drawing.Drawing2D.GraphicsPath shape = new System.Drawing.Drawing2D.GraphicsPath();

            shape.AddLine(0, 10, f.Width / 2 - b.Width / 2, 10);
            e.Graphics.DrawPath(new Pen(Color.Red, 30), shape);

            shape = new System.Drawing.Drawing2D.GraphicsPath();
            shape.AddLine(f.Width / 2 + b.Width / 2, 10, f.Width, 10);
            e.Graphics.DrawPath(new Pen(Color.Red, 30), shape);

        }



        private void button1_Paint(object sender, PaintEventArgs e)
        {
            Form f = this;
            Button b = sender as Button;

            System.Drawing.Drawing2D.GraphicsPath shape = new System.Drawing.Drawing2D.GraphicsPath();
            shape.AddArc(-20, 0, 35, 25, 270, 180);
            e.Graphics.DrawPath(new Pen(Color.Red, 30), shape);

            shape = new System.Drawing.Drawing2D.GraphicsPath();
            shape.AddArc(b.Width - 10, 0, 35, 25, 90, 180);
            e.Graphics.DrawPath(new Pen(Color.Red, 30), shape);
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Close();
        }

        protected void nadpis_set(string s)
        {
            nadpis.Text = s;
        }


        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private void button1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(Handle, 0xA1, 0x2, 0);
        }

        private void my_Button2_Click(object sender, EventArgs e)
        {
            base.Close();

        }
    }
}
