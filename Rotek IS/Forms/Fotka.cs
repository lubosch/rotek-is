﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RotekIS.General;

namespace RotekIS.Forms
{
    public partial class Fotka : Form
    {
        private string cesta;

        public Fotka(string cesta)
        {
            InitializeComponent();
            this.cesta = cesta;
            System.Diagnostics.Debug.WriteLine(cesta);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void Fotka_Load(object sender, EventArgs e)
        {
            try
            {

                pictureBox1.Image = System.Drawing.Image.FromFile(cesta);

                BackColor = Color.Lime;
                int sirka = pictureBox1.Image.Width;
                int vyska = pictureBox1.Image.Height;

                int obrv = pictureBox1.Size.Height - 100;
                int obrs = pictureBox1.Size.Width - 100;

                if (vyska < 200)
                {
                    decimal pom;
                    pom = (decimal)(vyska) / (decimal)(sirka);
                    vyska = 200;
                    sirka = (int)(vyska / pom);
                }
                else if (sirka < 200)
                {

                    decimal pom;
                    pom = (decimal)sirka / (decimal)vyska;
                    sirka = 200;
                    vyska = (int)(sirka / pom);
                }

                if (vyska > obrv)
                {
                    decimal pom;

                    pom = (decimal)(vyska) / (decimal)(sirka);
                    vyska = obrv;

                    sirka = (int)(vyska / pom);

                }
                else if (sirka > obrs)
                {
                    decimal pom;
                    pom = (decimal)sirka / (decimal)vyska;
                    sirka = obrs;
                    vyska = (int)(sirka / pom);
                }

                vecsi(vyska, sirka);
            }
            catch (System.Exception ex)
            {
                Close();
            }
        }

        public void vecsi(int vys, int sir)
        {
            Bitmap bm_source = new Bitmap(pictureBox1.Image);
            Bitmap bm_dest = new Bitmap(Convert.ToInt32(sir), Convert.ToInt32(vys));
            Graphics gr_dest = Graphics.FromImage(bm_dest);
            gr_dest.DrawImage(bm_source, 0, 0, sir, vys);
            pictureBox1.Image = bm_dest;
        }

    }
}
