﻿using RotekIS.General;
namespace RotekIS.Stocks
{
    partial class Inter_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(true);

        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.cross_out = new System.Windows.Forms.Label();
            this.nadpis = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(0, 25);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(390, 200);
            this.panel1.TabIndex = 502;
            // 
            // cross_out
            // 
            this.cross_out.AutoSize = true;
            this.cross_out.BackColor = System.Drawing.Color.Black;
            this.cross_out.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cross_out.ForeColor = System.Drawing.Color.White;
            this.cross_out.Location = new System.Drawing.Point(717, 0);
            this.cross_out.Name = "cross_out";
            this.cross_out.Size = new System.Drawing.Size(26, 25);
            this.cross_out.TabIndex = 92;
            this.cross_out.Text = "X";
            this.cross_out.Click += new System.EventHandler(this.label1_Click);
            // 
            // nadpis
            // 
            this.nadpis.BackColor = System.Drawing.Color.White;
            this.nadpis.BackgroundImage = global::RotekIS.Properties.Resources.Black;
            this.nadpis.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.nadpis.Font = new System.Drawing.Font("Verdana", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nadpis.ForeColor = System.Drawing.Color.White;
            this.nadpis.Location = new System.Drawing.Point(301, -8);
            this.nadpis.Name = "nadpis";
            this.nadpis.Size = new System.Drawing.Size(417, 40);
            this.nadpis.TabIndex = 501;
            this.nadpis.Text = "Pridať";
            this.nadpis.UseVisualStyleBackColor = false;
            this.nadpis.Paint += new System.Windows.Forms.PaintEventHandler(this.button1_Paint);
            this.nadpis.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button1_MouseDown);
            // 
            // Inter_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Red;
            this.ClientSize = new System.Drawing.Size(390, 225);
            this.Controls.Add(this.cross_out);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.nadpis);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Inter_Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add_Item";
            this.TransparencyKey = System.Drawing.Color.Red;
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Add_Item_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button nadpis;
        private System.Windows.Forms.Label cross_out;
        protected System.Windows.Forms.Panel panel1;

    }
}