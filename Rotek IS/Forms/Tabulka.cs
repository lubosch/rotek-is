﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RotekIS.Forms
{
    class Tabulka : DataGridView
    {
        public Tabulka()
        {

            this.RowHeadersWidth = 35;
            this.Dock = DockStyle.Bottom;

            this.DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 10);
            this.ColumnHeadersDefaultCellStyle.Font = new Font("Verdana", 10);
            this.BackgroundColor = Color.Silver;
            this.AlternatingRowsDefaultCellStyle.BackColor = Color.Gainsboro;
            this.DoubleBuffered = true;
        }

        protected override void OnRowPostPaint(DataGridViewRowPostPaintEventArgs e)
        {
            var rowIdx = (e.RowIndex + 1).ToString();

            var centerFormat = new StringFormat()
            {
                // right alignment might actually make more sense for numbers
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };

            var headerBounds = new Rectangle(e.RowBounds.Left, e.RowBounds.Top, RowHeadersWidth, e.RowBounds.Height);
            e.Graphics.DrawString(rowIdx, this.Font, SystemBrushes.ControlText, headerBounds, centerFormat);
        }

        protected override void OnLocationChanged(EventArgs e)
        {
            if (Dock == DockStyle.Bottom)
            {
                int x = 0, x1 = this.Size.Width, y = Location.Y, y1 = Size.Height;
                this.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
                this.Tag = new[] { -1, x, x1, y, y1 };
            }
            else if (Tag != null && ((int[])Tag)[0] == -1)
            {
                int x1 = ((int[])Tag)[2];
                int y1 = ((int[])Tag)[4];
                int x = ((int[])Tag)[1];
                int y = ((int[])Tag)[3];
                this.Tag = null;
                Location = new Point(x, y);
                this.Size = new Size(x1, y1);
            }
        }


    }
}
