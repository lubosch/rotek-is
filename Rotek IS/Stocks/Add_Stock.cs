﻿using RotekIS.General;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using RotekIS.SQL;

namespace RotekIS.Stocks
{
    public partial class Add_Stock : RotekIS.Stocks.Inter_Form
    {
        int mode;

        public Add_Stock(int mode)
        {
            InitializeComponent();
            if (mode == 1)
            {
                this.mode = 1;
                items2.Hide();
                nadpis_set("Pridať sklad");
            }
            else
            {
                this.mode = 2;
                my_Button1.Text = "Uložiť zmeny";
                nadpis_set("Upraviť sklad");
            }
        }

        private void Add_Stock_Load(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            if (mode == 2)
            {
                items2.DataSource = Stock_SQL.List().Tables[0];
                items2.DisplayMember = "Name";
                items2.SelectedIndexChanged += items2_SelectedIndexChanged;

            }

            name.Select();
        }

        void items2_SelectedIndexChanged(object sender, EventArgs e)
        {
            name.Text = ((DataRowView)items2.SelectedItem).Row["Name"].ToString();
            name.Select();
            name.SelectAll();
            listView1.Items.Clear();
            DataTable dd = Stock_SQL.GetParameters(name.Text).Tables[0];
            for (int i = 0; i < dd.Rows.Count; i++)
            {
                string[] row = { dd.Rows[i]["Name"].ToString(), dd.Rows[i]["Search"].ToString() };
                var listViewItem = new ListViewItem(row);
                listView1.Items.Add(listViewItem);
            }
        }




        private void my_Button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            param_to_listbox();
        }

        private void param_to_listbox()
        {
            if (parameter.Text != "")
            {

                if (radioButton1.Checked)
                {
                    string[] row = { parameter.Text, "Všade" };
                    var listViewItem = new ListViewItem(row);
                    listView1.Items.Add(listViewItem);
                }
                else if (radioButton2.Checked)
                {
                    string[] row = { parameter.Text, "Z ľava" };
                    var listViewItem = new ListViewItem(row);
                    listView1.Items.Add(listViewItem);
                }
                else if (radioButton3.Checked)
                {
                    string[] row = { parameter.Text, "Presne" };
                    var listViewItem = new ListViewItem(row);
                    listView1.Items.Add(listViewItem);
                }
                parameter.Text = "";
                parameter.Select();
                radioButton1.Checked = true;
            }
        }

        private void parameter_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                param_to_listbox();
            }
        }

        private void items1_MouseUp(object sender, MouseEventArgs e)
        {
            try
            {
                listView1.GetItemAt(e.X, e.Y).Selected = true;
                if (e.Button == System.Windows.Forms.MouseButtons.Right)
                {
                    contextMenuStrip1.Show(Cursor.Position);
                }
                else if (listView1.SelectedItems.Count > 0)
                {

                    parameter.Text = listView1.SelectedItems[0].SubItems[0].Text;
                    String search = listView1.SelectedItems[0].SubItems[1].Text;
                    if (search == "Všade")
                    {
                        radioButton1.Checked = true;
                    }
                    else if (search == "Z ľava")
                    {
                        radioButton2.Checked = true;
                    }
                    else if (search == "Presne")
                    {
                        radioButton3.Checked = true;
                    }
                }
            }
            catch (System.Exception ex)
            {

            }


        }

        private void zmazat()
        {
            if (listView1.SelectedItems.Count > 0)
            {
                string parameter = listView1.SelectedItems[0].Text;
                if (Stock_SQL.isParameter(parameter, name.Text.ToString()))
                {
                    if (Error.Question("Naozaj chcete vymazať parameter " + parameter + " so všetkými údajmi, ktoré sa v danej kolonke nachádzajú?"))
                    {
                        Stock_SQL.Remove(parameter, name.Text);
                    }
                }

                listView1.SelectedItems[0].Remove();
            }
        }

        private void my_Button1_Click(object sender, EventArgs e)
        {
            Add();
            this.Dispose();
        }

        private void Add()
        {
            if (name.Text == "" || listView1.Items.Count == 0) Error.Show("Nevyplnene meno alebo parametre");
            ListViewItem[] prameters = new ListViewItem[listView1.Items.Count];
            listView1.Items.CopyTo(prameters, 0);
            for (int i = 0; i < listView1.Items.Count; i++)
            {
                //MessageBox.Show(prameters[i].SubItems[1].Text);
            }
            Stock_SQL.Add(name.Text, prameters);

        }

        private void zmazaťToolStripMenuItem_Click(object sender, EventArgs e)
        {
            zmazat();
        }

        private void premenovaťToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                string parameter = listView1.SelectedItems[0].Text;
                if (Stock_SQL.isParameter(parameter, name.Text.ToString()))
                {
                    string premenovat = Error.ShowDialog("Premenovať " + parameter + " na:", "Premenovať");
                    if (premenovat.Length > 0)
                    {
                        Stock_SQL.Rename(premenovat, parameter, name.Text);
                        listView1.SelectedItems[0].Text = premenovat;
                    }
                }
            }

        }



        private void všadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listView1.SelectedItems[0].SubItems[1].Text = "Všade";

        }

        private void zĽavaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listView1.SelectedItems[0].SubItems[1].Text = "Z ľava";

        }

        private void presneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listView1.SelectedItems[0].SubItems[1].Text = "Presne";

        }


    }
}
