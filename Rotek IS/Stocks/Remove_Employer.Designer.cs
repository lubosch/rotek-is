﻿namespace RotekIS.Stocks
{
    partial class Remove_Employer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Remove_Employer));
            this.napis1 = new RotekIS.Forms.Napis();
            this.textac1 = new RotekIS.Forms.Textac();
            this.napis2 = new RotekIS.Forms.Napis();
            this.textac2 = new RotekIS.Forms.Textac();
            this.items1 = new RotekIS.Forms.Lister();
            this.my_Button3 = new RotekIS.my_Button();
            this.my_Button2 = new RotekIS.my_Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.items1);
            this.panel1.Controls.Add(this.textac2);
            this.panel1.Controls.Add(this.napis2);
            this.panel1.Controls.Add(this.textac1);
            this.panel1.Controls.Add(this.napis1);
            this.panel1.Controls.Add(this.my_Button3);
            this.panel1.Controls.Add(this.my_Button2);
            this.panel1.Size = new System.Drawing.Size(390, 328);
            // 
            // napis1
            // 
            this.napis1.AutoSize = true;
            this.napis1.Font = new System.Drawing.Font("Verdana", 10F);
            this.napis1.Location = new System.Drawing.Point(3, 9);
            this.napis1.Name = "napis1";
            this.napis1.Size = new System.Drawing.Size(45, 17);
            this.napis1.TabIndex = 0;
            this.napis1.Text = "Meno";
            // 
            // textac1
            // 
            this.textac1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.textac1.Location = new System.Drawing.Point(6, 29);
            this.textac1.Name = "textac1";
            this.textac1.Size = new System.Drawing.Size(163, 23);
            this.textac1.TabIndex = 1;
            this.textac1.TextChanged += new System.EventHandler(this.textac1_TextChanged);
            // 
            // napis2
            // 
            this.napis2.AutoSize = true;
            this.napis2.Font = new System.Drawing.Font("Verdana", 10F);
            this.napis2.Location = new System.Drawing.Point(175, 9);
            this.napis2.Name = "napis2";
            this.napis2.Size = new System.Drawing.Size(77, 17);
            this.napis2.TabIndex = 2;
            this.napis2.Text = "Priezvisko";
            // 
            // textac2
            // 
            this.textac2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.textac2.Location = new System.Drawing.Point(178, 29);
            this.textac2.Name = "textac2";
            this.textac2.Size = new System.Drawing.Size(163, 23);
            this.textac2.TabIndex = 3;
            // 
            // items1
            // 
            this.items1.Font = new System.Drawing.Font("Verdana", 10F);
            this.items1.FormattingEnabled = true;
            this.items1.ItemHeight = 16;
            this.items1.Location = new System.Drawing.Point(6, 58);
            this.items1.Name = "items1";
            this.items1.Size = new System.Drawing.Size(163, 212);
            this.items1.TabIndex = 4;
            this.items1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.items1_MouseClick);
            // 
            // my_Button3
            // 
            this.my_Button3.BackColor = System.Drawing.Color.Transparent;
            this.my_Button3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("my_Button3.BackgroundImage")));
            this.my_Button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.my_Button3.FlatAppearance.BorderSize = 0;
            this.my_Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.my_Button3.Font = new System.Drawing.Font("Verdana", 12F);
            this.my_Button3.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.my_Button3.Location = new System.Drawing.Point(6, 282);
            this.my_Button3.Name = "my_Button3";
            this.my_Button3.Size = new System.Drawing.Size(140, 41);
            this.my_Button3.TabIndex = 7;
            this.my_Button3.Text = "Zrušiť";
            this.my_Button3.UseVisualStyleBackColor = false;
            this.my_Button3.Click += new System.EventHandler(this.my_Button2_Click);
            // 
            // my_Button2
            // 
            this.my_Button2.BackColor = System.Drawing.Color.Transparent;
            this.my_Button2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("my_Button2.BackgroundImage")));
            this.my_Button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.my_Button2.FlatAppearance.BorderSize = 0;
            this.my_Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.my_Button2.Font = new System.Drawing.Font("Verdana", 12F);
            this.my_Button2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.my_Button2.Location = new System.Drawing.Point(201, 282);
            this.my_Button2.Name = "my_Button2";
            this.my_Button2.Size = new System.Drawing.Size(140, 41);
            this.my_Button2.TabIndex = 8;
            this.my_Button2.Text = "Zmazať";
            this.my_Button2.UseVisualStyleBackColor = false;
            this.my_Button2.Click += new System.EventHandler(this.my_Button2_Click_1);
            // 
            // Remove_Employer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(390, 353);
            this.Name = "Remove_Employer";
            this.Load += new System.EventHandler(this.Remove_Employer_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Forms.Napis napis1;
        private Forms.Lister items1;
        private Forms.Textac textac2;
        private Forms.Napis napis2;
        private Forms.Textac textac1;
        private my_Button my_Button3;
        private my_Button my_Button2;
    }
}
