﻿namespace RotekIS.Stocks
{
    partial class Stock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Stock));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.dataRepeater1 = new Microsoft.VisualBasic.PowerPacks.DataRepeater();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.DR_surname = new System.Windows.Forms.Label();
            this.DR_name = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.my_Button9 = new RotekIS.my_Button();
            this.my_Button1 = new RotekIS.my_Button();
            this.napis3 = new RotekIS.Forms.Napis();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer5 = new System.Windows.Forms.SplitContainer();
            this.my_Button2 = new RotekIS.my_Button();
            this.my_Button7 = new RotekIS.my_Button();
            this.dataRepeater2 = new Microsoft.VisualBasic.PowerPacks.DataRepeater();
            this.DR_stock = new System.Windows.Forms.Label();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.my_Button8 = new RotekIS.my_Button();
            this.my_Button3 = new RotekIS.my_Button();
            this.my_Button4 = new RotekIS.my_Button();
            this.label1 = new System.Windows.Forms.Label();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.Button4 = new System.Windows.Forms.Button();
            this.Button3 = new System.Windows.Forms.Button();
            this.tabulka = new RotekIS.Forms.Tabulka();
            this.my_Button5 = new RotekIS.my_Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.pridaťToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vrátiťToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zničiťToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.informácieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zmazaťToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.presunúťToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.daťNaPredajToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.premenovaťToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.daťNaInýSkladToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.premenovaťSkladToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vytvoriťPredajnýSkladToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zmazaťSkladToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.headline1 = new RotekIS.Forms.Headline();
            this.button2 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.contextMenuStrip3 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.exportVydanéhoNáradiaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button6 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.dataRepeater1.ItemTemplate.SuspendLayout();
            this.dataRepeater1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).BeginInit();
            this.splitContainer5.Panel1.SuspendLayout();
            this.splitContainer5.Panel2.SuspendLayout();
            this.splitContainer5.SuspendLayout();
            this.dataRepeater2.ItemTemplate.SuspendLayout();
            this.dataRepeater2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.GroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabulka)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.contextMenuStrip3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.radioButton2);
            this.splitContainer1.Panel1.Controls.Add(this.radioButton1);
            this.splitContainer1.Panel1.Controls.Add(this.dataRepeater1);
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            this.splitContainer1.Panel1.Controls.Add(this.my_Button9);
            this.splitContainer1.Panel1.Controls.Add(this.my_Button1);
            this.splitContainer1.Panel1.Controls.Add(this.napis3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1202, 525);
            this.splitContainer1.SplitterDistance = 227;
            this.splitContainer1.TabIndex = 0;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.radioButton2.Location = new System.Drawing.Point(101, 132);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(123, 21);
            this.radioButton2.TabIndex = 8;
            this.radioButton2.Text = "Nezamestnanci";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.radioButton1.Location = new System.Drawing.Point(0, 131);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(107, 21);
            this.radioButton1.TabIndex = 7;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Zamestnanci";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // dataRepeater1
            // 
            this.dataRepeater1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataRepeater1.BackColor = System.Drawing.Color.DimGray;
            this.dataRepeater1.ItemHeaderVisible = false;
            // 
            // dataRepeater1.ItemTemplate
            // 
            this.dataRepeater1.ItemTemplate.BackColor = System.Drawing.Color.White;
            this.dataRepeater1.ItemTemplate.Controls.Add(this.splitContainer3);
            this.dataRepeater1.ItemTemplate.Size = new System.Drawing.Size(212, 49);
            this.dataRepeater1.ItemTemplate.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DR1_MouseClick);
            this.dataRepeater1.Location = new System.Drawing.Point(4, 154);
            this.dataRepeater1.Name = "dataRepeater1";
            this.dataRepeater1.Size = new System.Drawing.Size(220, 368);
            this.dataRepeater1.TabIndex = 2;
            this.dataRepeater1.Text = "dataRepeater3";
            this.dataRepeater1.CurrentItemIndexChanged += new System.EventHandler(this.DR_IndexChange);
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.DR_surname);
            this.splitContainer3.Panel1.Controls.Add(this.DR_name);
            this.splitContainer3.Panel1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.DR1_MouseClick);
            this.splitContainer3.Panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DR1_MouseClick);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.shapeContainer1);
            this.splitContainer3.Panel2.Controls.Add(this.pictureBox1);
            this.splitContainer3.Size = new System.Drawing.Size(208, 44);
            this.splitContainer3.SplitterDistance = 172;
            this.splitContainer3.SplitterWidth = 1;
            this.splitContainer3.TabIndex = 4;
            // 
            // DR_surname
            // 
            this.DR_surname.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.DR_surname.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.DR_surname.Location = new System.Drawing.Point(0, 18);
            this.DR_surname.Name = "DR_surname";
            this.DR_surname.Size = new System.Drawing.Size(172, 26);
            this.DR_surname.TabIndex = 1;
            this.DR_surname.Text = "label2";
            this.DR_surname.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.DR_surname.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DR1_MouseClick);
            // 
            // DR_name
            // 
            this.DR_name.AutoSize = true;
            this.DR_name.Font = new System.Drawing.Font("Verdana", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DR_name.Location = new System.Drawing.Point(0, 0);
            this.DR_name.Name = "DR_name";
            this.DR_name.Size = new System.Drawing.Size(68, 23);
            this.DR_name.TabIndex = 0;
            this.DR_name.Text = "label1";
            this.DR_name.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DR1_MouseClick);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Size = new System.Drawing.Size(35, 44);
            this.shapeContainer1.TabIndex = 0;
            this.shapeContainer1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::RotekIS.Properties.Resources.arrow_right;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(35, 44);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.button1);
            this.panel1.Location = new System.Drawing.Point(3, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(221, 76);
            this.panel1.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.BackgroundImage = global::RotekIS.Properties.Resources.Echo_Grey_Gradient_Home_Screen;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.Location = new System.Drawing.Point(4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(211, 68);
            this.button1.TabIndex = 0;
            this.button1.Text = "Sklad";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            this.button1.Paint += new System.Windows.Forms.PaintEventHandler(this.button1_Paint);
            this.button1.MouseEnter += new System.EventHandler(this.button1_MouseEnter);
            this.button1.MouseLeave += new System.EventHandler(this.button1_MouseLeave);
            // 
            // my_Button9
            // 
            this.my_Button9.BackColor = System.Drawing.Color.Transparent;
            this.my_Button9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("my_Button9.BackgroundImage")));
            this.my_Button9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.my_Button9.FlatAppearance.BorderSize = 0;
            this.my_Button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.my_Button9.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.my_Button9.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.my_Button9.Location = new System.Drawing.Point(3, 82);
            this.my_Button9.Name = "my_Button9";
            this.my_Button9.Size = new System.Drawing.Size(98, 41);
            this.my_Button9.TabIndex = 5;
            this.my_Button9.Text = "Pridať";
            this.my_Button9.UseVisualStyleBackColor = false;
            this.my_Button9.Click += new System.EventHandler(this.my_Button1_Click);
            // 
            // my_Button1
            // 
            this.my_Button1.BackColor = System.Drawing.Color.Transparent;
            this.my_Button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("my_Button1.BackgroundImage")));
            this.my_Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.my_Button1.FlatAppearance.BorderSize = 0;
            this.my_Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.my_Button1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.my_Button1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.my_Button1.Location = new System.Drawing.Point(107, 82);
            this.my_Button1.Name = "my_Button1";
            this.my_Button1.Size = new System.Drawing.Size(117, 41);
            this.my_Button1.TabIndex = 6;
            this.my_Button1.Text = "Zmazať";
            this.my_Button1.UseVisualStyleBackColor = false;
            this.my_Button1.Click += new System.EventHandler(this.my_Button2_Click);
            // 
            // napis3
            // 
            this.napis3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.napis3.AutoSize = true;
            this.napis3.Font = new System.Drawing.Font("Verdana", 10F);
            this.napis3.Location = new System.Drawing.Point(-12, 114);
            this.napis3.Name = "napis3";
            this.napis3.Size = new System.Drawing.Size(323, 17);
            this.napis3.TabIndex = 14;
            this.napis3.Text = "___________________________________";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.splitContainer5);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer4);
            this.splitContainer2.Size = new System.Drawing.Size(971, 525);
            this.splitContainer2.SplitterDistance = 90;
            this.splitContainer2.SplitterWidth = 2;
            this.splitContainer2.TabIndex = 0;
            // 
            // splitContainer5
            // 
            this.splitContainer5.BackColor = System.Drawing.Color.White;
            this.splitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer5.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer5.IsSplitterFixed = true;
            this.splitContainer5.Location = new System.Drawing.Point(0, 0);
            this.splitContainer5.Name = "splitContainer5";
            // 
            // splitContainer5.Panel1
            // 
            this.splitContainer5.Panel1.BackColor = System.Drawing.Color.White;
            this.splitContainer5.Panel1.Controls.Add(this.my_Button2);
            this.splitContainer5.Panel1.Controls.Add(this.my_Button7);
            // 
            // splitContainer5.Panel2
            // 
            this.splitContainer5.Panel2.Controls.Add(this.dataRepeater2);
            this.splitContainer5.Size = new System.Drawing.Size(971, 90);
            this.splitContainer5.SplitterDistance = 145;
            this.splitContainer5.SplitterWidth = 1;
            this.splitContainer5.TabIndex = 6;
            // 
            // my_Button2
            // 
            this.my_Button2.BackColor = System.Drawing.Color.Transparent;
            this.my_Button2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("my_Button2.BackgroundImage")));
            this.my_Button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.my_Button2.FlatAppearance.BorderSize = 0;
            this.my_Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.my_Button2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.my_Button2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.my_Button2.Location = new System.Drawing.Point(0, 0);
            this.my_Button2.Name = "my_Button2";
            this.my_Button2.Size = new System.Drawing.Size(142, 43);
            this.my_Button2.TabIndex = 6;
            this.my_Button2.Text = "Pridať";
            this.my_Button2.UseVisualStyleBackColor = false;
            this.my_Button2.Click += new System.EventHandler(this.my_Button7_Click);
            // 
            // my_Button7
            // 
            this.my_Button7.BackColor = System.Drawing.Color.Transparent;
            this.my_Button7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("my_Button7.BackgroundImage")));
            this.my_Button7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.my_Button7.FlatAppearance.BorderSize = 0;
            this.my_Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.my_Button7.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.my_Button7.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.my_Button7.Location = new System.Drawing.Point(2, 46);
            this.my_Button7.Name = "my_Button7";
            this.my_Button7.Size = new System.Drawing.Size(142, 44);
            this.my_Button7.TabIndex = 7;
            this.my_Button7.Text = "Upraviť";
            this.my_Button7.UseVisualStyleBackColor = false;
            this.my_Button7.Click += new System.EventHandler(this.my_Button8_Click);
            // 
            // dataRepeater2
            // 
            this.dataRepeater2.BackColor = System.Drawing.Color.Gray;
            this.dataRepeater2.Dock = System.Windows.Forms.DockStyle.Fill;
            // 
            // dataRepeater2.ItemTemplate
            // 
            this.dataRepeater2.ItemTemplate.BackColor = System.Drawing.Color.White;
            this.dataRepeater2.ItemTemplate.Controls.Add(this.DR_stock);
            this.dataRepeater2.ItemTemplate.Size = new System.Drawing.Size(158, 82);
            this.dataRepeater2.LayoutStyle = Microsoft.VisualBasic.PowerPacks.DataRepeaterLayoutStyles.Horizontal;
            this.dataRepeater2.Location = new System.Drawing.Point(0, 0);
            this.dataRepeater2.Name = "dataRepeater2";
            this.dataRepeater2.Size = new System.Drawing.Size(825, 90);
            this.dataRepeater2.TabIndex = 2;
            this.dataRepeater2.Text = "dataRepeater3";
            this.dataRepeater2.CurrentItemIndexChanged += new System.EventHandler(this.DR_IndexChange);
            // 
            // DR_stock
            // 
            this.DR_stock.AutoEllipsis = true;
            this.DR_stock.BackColor = System.Drawing.Color.White;
            this.DR_stock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DR_stock.Font = new System.Drawing.Font("Verdana", 14F, System.Drawing.FontStyle.Bold);
            this.DR_stock.Location = new System.Drawing.Point(0, 0);
            this.DR_stock.Name = "DR_stock";
            this.DR_stock.Size = new System.Drawing.Size(157, 67);
            this.DR_stock.TabIndex = 1;
            this.DR_stock.Text = "Ahoj ako sa mas ja sa mam dobre";
            this.DR_stock.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.DR_stock.MouseClick += new System.Windows.Forms.MouseEventHandler(this.DR_stock_MouseClick);
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer4.IsSplitterFixed = true;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.BackColor = System.Drawing.Color.White;
            this.splitContainer4.Panel1.Controls.Add(this.progressBar1);
            this.splitContainer4.Panel1.Controls.Add(this.my_Button8);
            this.splitContainer4.Panel1.Controls.Add(this.my_Button3);
            this.splitContainer4.Panel1.Controls.Add(this.my_Button4);
            this.splitContainer4.Panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer4_Panel1_Paint);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.BackColor = System.Drawing.Color.White;
            this.splitContainer4.Panel2.Controls.Add(this.label1);
            this.splitContainer4.Panel2.Controls.Add(this.GroupBox1);
            this.splitContainer4.Panel2.Controls.Add(this.tabulka);
            this.splitContainer4.Panel2.Controls.Add(this.my_Button5);
            this.splitContainer4.Panel2.Controls.Add(this.dateTimePicker1);
            this.splitContainer4.Size = new System.Drawing.Size(971, 433);
            this.splitContainer4.SplitterDistance = 47;
            this.splitContainer4.SplitterWidth = 1;
            this.splitContainer4.TabIndex = 7;
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Location = new System.Drawing.Point(459, 10);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(249, 23);
            this.progressBar1.TabIndex = 9;
            this.progressBar1.Visible = false;
            // 
            // my_Button8
            // 
            this.my_Button8.BackColor = System.Drawing.Color.Transparent;
            this.my_Button8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("my_Button8.BackgroundImage")));
            this.my_Button8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.my_Button8.FlatAppearance.BorderSize = 0;
            this.my_Button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.my_Button8.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.my_Button8.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.my_Button8.Location = new System.Drawing.Point(0, 4);
            this.my_Button8.Name = "my_Button8";
            this.my_Button8.Size = new System.Drawing.Size(144, 41);
            this.my_Button8.TabIndex = 3;
            this.my_Button8.Text = "Požičať";
            this.my_Button8.UseVisualStyleBackColor = false;
            this.my_Button8.Click += new System.EventHandler(this.my_Button3_Click);
            // 
            // my_Button3
            // 
            this.my_Button3.BackColor = System.Drawing.Color.Transparent;
            this.my_Button3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("my_Button3.BackgroundImage")));
            this.my_Button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.my_Button3.FlatAppearance.BorderSize = 0;
            this.my_Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.my_Button3.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.my_Button3.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.my_Button3.Location = new System.Drawing.Point(150, 4);
            this.my_Button3.Name = "my_Button3";
            this.my_Button3.Size = new System.Drawing.Size(157, 41);
            this.my_Button3.TabIndex = 4;
            this.my_Button3.Text = "Vrátiť na sklad";
            this.my_Button3.UseVisualStyleBackColor = false;
            this.my_Button3.Click += new System.EventHandler(this.my_Button4_Click);
            // 
            // my_Button4
            // 
            this.my_Button4.BackColor = System.Drawing.Color.Transparent;
            this.my_Button4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("my_Button4.BackgroundImage")));
            this.my_Button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.my_Button4.FlatAppearance.BorderSize = 0;
            this.my_Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.my_Button4.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.my_Button4.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.my_Button4.Location = new System.Drawing.Point(313, 3);
            this.my_Button4.Name = "my_Button4";
            this.my_Button4.Size = new System.Drawing.Size(140, 41);
            this.my_Button4.TabIndex = 5;
            this.my_Button4.Text = "Zničiť";
            this.my_Button4.UseVisualStyleBackColor = false;
            this.my_Button4.Click += new System.EventHandler(this.my_Button5_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(657, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 17);
            this.label1.TabIndex = 11;
            this.label1.Text = "Stav ku:";
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.listBox1);
            this.GroupBox1.Controls.Add(this.Button4);
            this.GroupBox1.Controls.Add(this.Button3);
            this.GroupBox1.Location = new System.Drawing.Point(361, 93);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(333, 181);
            this.GroupBox1.TabIndex = 9;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Poznámky";
            this.GroupBox1.Visible = false;
            this.GroupBox1.VisibleChanged += new System.EventHandler(this.GroupBox1_VisibleChanged);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(6, 19);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(321, 121);
            this.listBox1.TabIndex = 3;
            // 
            // Button4
            // 
            this.Button4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Button4.Location = new System.Drawing.Point(194, 151);
            this.Button4.Name = "Button4";
            this.Button4.Size = new System.Drawing.Size(138, 30);
            this.Button4.TabIndex = 2;
            this.Button4.Text = "Pridať poznámku";
            this.Button4.UseVisualStyleBackColor = true;
            this.Button4.Click += new System.EventHandler(this.Button4_Click);
            // 
            // Button3
            // 
            this.Button3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Button3.Location = new System.Drawing.Point(0, 151);
            this.Button3.Name = "Button3";
            this.Button3.Size = new System.Drawing.Size(163, 30);
            this.Button3.TabIndex = 1;
            this.Button3.Text = "Odstrániť poznámku";
            this.Button3.UseVisualStyleBackColor = true;
            this.Button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // tabulka
            // 
            this.tabulka.AllowDrop = true;
            this.tabulka.AllowUserToAddRows = false;
            this.tabulka.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.tabulka.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.tabulka.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabulka.BackgroundColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Verdana", 10F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.tabulka.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.tabulka.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.tabulka.DefaultCellStyle = dataGridViewCellStyle3;
            this.tabulka.Location = new System.Drawing.Point(0, 64);
            this.tabulka.Name = "tabulka";
            this.tabulka.ReadOnly = true;
            this.tabulka.RowHeadersWidth = 35;
            this.tabulka.Size = new System.Drawing.Size(971, 332);
            this.tabulka.TabIndex = 3;
            this.tabulka.DataSourceChanged += new System.EventHandler(this.tabulka_DataSourceChanged);
            this.tabulka.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.tabulka_CellClick);
            this.tabulka.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.tabulka_CellFormatting);
            this.tabulka.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.tabulka_CellMouseUp);
            // 
            // my_Button5
            // 
            this.my_Button5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.my_Button5.BackColor = System.Drawing.Color.Transparent;
            this.my_Button5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("my_Button5.BackgroundImage")));
            this.my_Button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.my_Button5.FlatAppearance.BorderSize = 0;
            this.my_Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.my_Button5.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.my_Button5.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.my_Button5.Location = new System.Drawing.Point(823, 4);
            this.my_Button5.Name = "my_Button5";
            this.my_Button5.Size = new System.Drawing.Size(140, 41);
            this.my_Button5.TabIndex = 7;
            this.my_Button5.Text = "Export";
            this.my_Button5.UseVisualStyleBackColor = false;
            this.my_Button5.Click += new System.EventHandler(this.my_Button6_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePicker1.CustomFormat = "dd. M. yyyy HH:mm";
            this.dateTimePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(661, 22);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(156, 23);
            this.dateTimePicker1.TabIndex = 10;
            this.dateTimePicker1.CloseUp += new System.EventHandler(this.dateTimePicker1_CloseUp);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pridaťToolStripMenuItem,
            this.vrátiťToolStripMenuItem,
            this.zničiťToolStripMenuItem,
            this.informácieToolStripMenuItem,
            this.zmazaťToolStripMenuItem,
            this.presunúťToolStripMenuItem,
            this.daťNaPredajToolStripMenuItem,
            this.premenovaťToolStripMenuItem,
            this.daťNaInýSkladToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(159, 202);
            this.contextMenuStrip1.VisibleChanged += new System.EventHandler(this.contextMenuStrip1_VisibleChanged);
            // 
            // pridaťToolStripMenuItem
            // 
            this.pridaťToolStripMenuItem.Name = "pridaťToolStripMenuItem";
            this.pridaťToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.pridaťToolStripMenuItem.Text = "Pridať";
            this.pridaťToolStripMenuItem.Click += new System.EventHandler(this.pridaťToolStripMenuItem_Click);
            // 
            // vrátiťToolStripMenuItem
            // 
            this.vrátiťToolStripMenuItem.Name = "vrátiťToolStripMenuItem";
            this.vrátiťToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.vrátiťToolStripMenuItem.Text = "Vrátiť";
            this.vrátiťToolStripMenuItem.Click += new System.EventHandler(this.vrátiťToolStripMenuItem_Click);
            // 
            // zničiťToolStripMenuItem
            // 
            this.zničiťToolStripMenuItem.Name = "zničiťToolStripMenuItem";
            this.zničiťToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.zničiťToolStripMenuItem.Text = "Zničiť";
            this.zničiťToolStripMenuItem.Click += new System.EventHandler(this.zničiťToolStripMenuItem_Click);
            // 
            // informácieToolStripMenuItem
            // 
            this.informácieToolStripMenuItem.Name = "informácieToolStripMenuItem";
            this.informácieToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.informácieToolStripMenuItem.Text = "Informácie";
            this.informácieToolStripMenuItem.Click += new System.EventHandler(this.informácieToolStripMenuItem_Click);
            // 
            // zmazaťToolStripMenuItem
            // 
            this.zmazaťToolStripMenuItem.Name = "zmazaťToolStripMenuItem";
            this.zmazaťToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.zmazaťToolStripMenuItem.Text = "Zmazať";
            this.zmazaťToolStripMenuItem.Click += new System.EventHandler(this.zmazaťToolStripMenuItem_Click);
            // 
            // presunúťToolStripMenuItem
            // 
            this.presunúťToolStripMenuItem.Name = "presunúťToolStripMenuItem";
            this.presunúťToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.presunúťToolStripMenuItem.Text = "Presunúť";
            this.presunúťToolStripMenuItem.Click += new System.EventHandler(this.presunúťToolStripMenuItem_Click);
            // 
            // daťNaPredajToolStripMenuItem
            // 
            this.daťNaPredajToolStripMenuItem.Name = "daťNaPredajToolStripMenuItem";
            this.daťNaPredajToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.daťNaPredajToolStripMenuItem.Text = "Dať na predaj";
            this.daťNaPredajToolStripMenuItem.Click += new System.EventHandler(this.daťNaPredajToolStripMenuItem_Click);
            // 
            // premenovaťToolStripMenuItem
            // 
            this.premenovaťToolStripMenuItem.Name = "premenovaťToolStripMenuItem";
            this.premenovaťToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.premenovaťToolStripMenuItem.Text = "Premenovať";
            this.premenovaťToolStripMenuItem.Click += new System.EventHandler(this.premenovaťToolStripMenuItem_Click);
            // 
            // daťNaInýSkladToolStripMenuItem
            // 
            this.daťNaInýSkladToolStripMenuItem.Name = "daťNaInýSkladToolStripMenuItem";
            this.daťNaInýSkladToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.daťNaInýSkladToolStripMenuItem.Text = "Dať na iný sklad";
            this.daťNaInýSkladToolStripMenuItem.Click += new System.EventHandler(this.daťNaInýSkladToolStripMenuItem_Click);
            this.daťNaInýSkladToolStripMenuItem.MouseHover += new System.EventHandler(this.daťNaInýSkladToolStripMenuItem_MouseHover);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.premenovaťSkladToolStripMenuItem,
            this.vytvoriťPredajnýSkladToolStripMenuItem,
            this.zmazaťSkladToolStripMenuItem});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(219, 76);
            // 
            // premenovaťSkladToolStripMenuItem
            // 
            this.premenovaťSkladToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.premenovaťSkladToolStripMenuItem.Name = "premenovaťSkladToolStripMenuItem";
            this.premenovaťSkladToolStripMenuItem.Size = new System.Drawing.Size(218, 24);
            this.premenovaťSkladToolStripMenuItem.Text = "Premenovať sklad";
            this.premenovaťSkladToolStripMenuItem.Click += new System.EventHandler(this.premenovaťSkladToolStripMenuItem_Click);
            // 
            // vytvoriťPredajnýSkladToolStripMenuItem
            // 
            this.vytvoriťPredajnýSkladToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.vytvoriťPredajnýSkladToolStripMenuItem.Name = "vytvoriťPredajnýSkladToolStripMenuItem";
            this.vytvoriťPredajnýSkladToolStripMenuItem.Size = new System.Drawing.Size(218, 24);
            this.vytvoriťPredajnýSkladToolStripMenuItem.Text = "Vytvoriť predajný sklad";
            this.vytvoriťPredajnýSkladToolStripMenuItem.Click += new System.EventHandler(this.vytvoriťPredajnýSkladToolStripMenuItem_Click);
            // 
            // zmazaťSkladToolStripMenuItem
            // 
            this.zmazaťSkladToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.zmazaťSkladToolStripMenuItem.Name = "zmazaťSkladToolStripMenuItem";
            this.zmazaťSkladToolStripMenuItem.Size = new System.Drawing.Size(218, 24);
            this.zmazaťSkladToolStripMenuItem.Text = "Zmazať sklad";
            this.zmazaťSkladToolStripMenuItem.Click += new System.EventHandler(this.zmazaťSkladToolStripMenuItem_Click);
            // 
            // headline1
            // 
            this.headline1.AutoSize = true;
            this.headline1.Font = new System.Drawing.Font("Tahoma", 40F, System.Drawing.FontStyle.Bold);
            this.headline1.Location = new System.Drawing.Point(539, 0);
            this.headline1.Name = "headline1";
            this.headline1.Size = new System.Drawing.Size(91, 65);
            this.headline1.TabIndex = 1;
            this.headline1.Text = " / ";
            this.headline1.TextChanged += new System.EventHandler(this.headline1_TextChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(0, 0);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(103, 34);
            this.button2.TabIndex = 9;
            this.button2.Text = "Export mesačný";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(105, 0);
            this.button5.Margin = new System.Windows.Forms.Padding(2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(119, 34);
            this.button5.TabIndex = 10;
            this.button5.Text = "Resetovať zlomené";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // contextMenuStrip3
            // 
            this.contextMenuStrip3.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportVydanéhoNáradiaToolStripMenuItem});
            this.contextMenuStrip3.Name = "contextMenuStrip3";
            this.contextMenuStrip3.Size = new System.Drawing.Size(205, 26);
            // 
            // exportVydanéhoNáradiaToolStripMenuItem
            // 
            this.exportVydanéhoNáradiaToolStripMenuItem.Name = "exportVydanéhoNáradiaToolStripMenuItem";
            this.exportVydanéhoNáradiaToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.exportVydanéhoNáradiaToolStripMenuItem.Text = "Export vydaného náradia";
            this.exportVydanéhoNáradiaToolStripMenuItem.Click += new System.EventHandler(this.exportVydanehoNaradiaToolStripMenuItem_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.splitContainer1);
            this.panel2.Location = new System.Drawing.Point(0, 57);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1202, 525);
            this.panel2.TabIndex = 11;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(228, 0);
            this.button6.Margin = new System.Windows.Forms.Padding(2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(83, 34);
            this.button6.TabIndex = 12;
            this.button6.Text = "Vyhľadať";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // Stock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1205, 584);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.headline1);
            this.KeyPreview = true;
            this.Name = "Stock";
            this.Text = "Sklad";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Click += new System.EventHandler(this.Stock_Click);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.dataRepeater1.ItemTemplate.ResumeLayout(false);
            this.dataRepeater1.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel1.PerformLayout();
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer5.Panel1.ResumeLayout(false);
            this.splitContainer5.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).EndInit();
            this.splitContainer5.ResumeLayout(false);
            this.dataRepeater2.ItemTemplate.ResumeLayout(false);
            this.dataRepeater2.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            this.splitContainer4.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.GroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabulka)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.contextMenuStrip2.ResumeLayout(false);
            this.contextMenuStrip3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem pridaťToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vrátiťToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zničiťToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem informácieToolStripMenuItem;
        private my_Button my_Button9;
        private my_Button my_Button1;
        private Forms.Headline headline1;
        private System.Windows.Forms.ToolStripMenuItem zmazaťToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem presunúťToolStripMenuItem;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem vytvoriťPredajnýSkladToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem daťNaPredajToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zmazaťSkladToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem premenovaťToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem premenovaťSkladToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem daťNaInýSkladToolStripMenuItem;
        private Microsoft.VisualBasic.PowerPacks.DataRepeater dataRepeater1;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.Label DR_name;
        private System.Windows.Forms.Label DR_surname;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private Forms.Napis napis3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip3;
        private System.Windows.Forms.ToolStripMenuItem exportVydanéhoNáradiaToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.ProgressBar progressBar1;
        private my_Button my_Button8;
        private my_Button my_Button3;
        private my_Button my_Button4;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.GroupBox GroupBox1;
        private System.Windows.Forms.ListBox listBox1;
        internal System.Windows.Forms.Button Button4;
        internal System.Windows.Forms.Button Button3;
        private Forms.Tabulka tabulka;
        private my_Button my_Button5;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.SplitContainer splitContainer5;
        private my_Button my_Button2;
        private my_Button my_Button7;
        private Microsoft.VisualBasic.PowerPacks.DataRepeater dataRepeater2;
        private System.Windows.Forms.Label DR_stock;
        private System.Windows.Forms.Button button6;














    }
}

