﻿namespace RotekIS.Stocks
{
    partial class Item_Info
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.napis1 = new RotekIS.Forms.Napis();
            this.napis2 = new RotekIS.Forms.Napis();
            this.napis3 = new RotekIS.Forms.Napis();
            this.tabulka1 = new RotekIS.Forms.Tabulka();
            this.headline1 = new RotekIS.Forms.Headline();
            this.tabulka2 = new RotekIS.Forms.Tabulka();
            this.napis4 = new RotekIS.Forms.Napis();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabulka1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabulka2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.napis4);
            this.panel1.Controls.Add(this.tabulka2);
            this.panel1.Controls.Add(this.headline1);
            this.panel1.Controls.Add(this.tabulka1);
            this.panel1.Controls.Add(this.napis3);
            this.panel1.Controls.Add(this.napis2);
            this.panel1.Controls.Add(this.napis1);
            this.panel1.Size = new System.Drawing.Size(652, 700);
            // 
            // napis1
            // 
            this.napis1.AutoSize = true;
            this.napis1.Font = new System.Drawing.Font("Verdana", 10F);
            this.napis1.Location = new System.Drawing.Point(11, 99);
            this.napis1.Name = "napis1";
            this.napis1.Size = new System.Drawing.Size(81, 17);
            this.napis1.TabIndex = 0;
            this.napis1.Text = "Na sklade:";
            // 
            // napis2
            // 
            this.napis2.AutoSize = true;
            this.napis2.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold);
            this.napis2.Location = new System.Drawing.Point(89, 99);
            this.napis2.Name = "napis2";
            this.napis2.Size = new System.Drawing.Size(0, 17);
            this.napis2.TabIndex = 1;
            // 
            // napis3
            // 
            this.napis3.AutoSize = true;
            this.napis3.Font = new System.Drawing.Font("Verdana", 10F);
            this.napis3.Location = new System.Drawing.Point(11, 135);
            this.napis3.Name = "napis3";
            this.napis3.Size = new System.Drawing.Size(202, 17);
            this.napis3.TabIndex = 2;
            this.napis3.Text = "Zo zamestnancov ho majú:";
            // 
            // tabulka1
            // 
            this.tabulka1.AllowUserToAddRows = false;
            this.tabulka1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.Gainsboro;
            this.tabulka1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.tabulka1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabulka1.BackgroundColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Verdana", 10F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.tabulka1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.tabulka1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.NullValue = "Sklad";
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.tabulka1.DefaultCellStyle = dataGridViewCellStyle6;
            this.tabulka1.Location = new System.Drawing.Point(0, 155);
            this.tabulka1.Name = "tabulka1";
            this.tabulka1.ReadOnly = true;
            this.tabulka1.RowHeadersWidth = 35;
            this.tabulka1.Size = new System.Drawing.Size(650, 228);
            this.tabulka1.TabIndex = 3;
            // 
            // headline1
            // 
            this.headline1.Dock = System.Windows.Forms.DockStyle.Top;
            this.headline1.Font = new System.Drawing.Font("Tahoma", 40F, System.Drawing.FontStyle.Bold);
            this.headline1.Location = new System.Drawing.Point(0, 0);
            this.headline1.Name = "headline1";
            this.headline1.Size = new System.Drawing.Size(650, 65);
            this.headline1.TabIndex = 4;
            this.headline1.Text = "headline1";
            this.headline1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabulka2
            // 
            this.tabulka2.AllowUserToAddRows = false;
            this.tabulka2.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.tabulka2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.tabulka2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabulka2.BackgroundColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Verdana", 10F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.tabulka2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.tabulka2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.NullValue = "Sklad";
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.tabulka2.DefaultCellStyle = dataGridViewCellStyle3;
            this.tabulka2.Location = new System.Drawing.Point(0, 434);
            this.tabulka2.Name = "tabulka2";
            this.tabulka2.ReadOnly = true;
            this.tabulka2.RowHeadersWidth = 35;
            this.tabulka2.Size = new System.Drawing.Size(650, 261);
            this.tabulka2.TabIndex = 5;
            this.tabulka2.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.tabulka2_CellFormatting);
            // 
            // napis4
            // 
            this.napis4.AutoSize = true;
            this.napis4.Font = new System.Drawing.Font("Verdana", 10F);
            this.napis4.Location = new System.Drawing.Point(3, 414);
            this.napis4.Name = "napis4";
            this.napis4.Size = new System.Drawing.Size(97, 17);
            this.napis4.TabIndex = 6;
            this.napis4.Text = "Manipulácie :";
            // 
            // Item_Info
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(652, 725);
            this.Name = "Item_Info";
            this.Load += new System.EventHandler(this.Item_Info_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabulka1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabulka2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Forms.Napis napis1;
        private Forms.Tabulka tabulka1;
        private Forms.Napis napis3;
        private Forms.Napis napis2;
        private Forms.Headline headline1;
        private Forms.Napis napis4;
        private Forms.Tabulka tabulka2;
    }
}
