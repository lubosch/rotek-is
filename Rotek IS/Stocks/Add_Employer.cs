﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using RotekIS.General;

namespace RotekIS.Stocks
{
    public partial class Add_Employer : RotekIS.Stocks.Inter_Form
    {
        int typ;

        public Add_Employer(int typ)
            : base("Pridať osobu")
        {
            InitializeComponent();
            this.typ = typ;
            
        }

        private void my_Button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void my_Button2_Click_1(object sender, EventArgs e)
        {
            Pridaj();
        }

        private void textac2_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Pridaj();
            }
        }
        
        private void Pridaj()
        {
            if (textac1.Text == "" || textac2.Text == "")
            {
                Error.Show("Nevyplnene meno alebo priezvisko");
            }
            else
            {
                Employer.Add(textac1.Text, textac2.Text, typ);
            }
            this.Dispose();
        }

        private void Add_Employer_Load(object sender, EventArgs e)
        {
            textac1.Select();
        }

    }
}
