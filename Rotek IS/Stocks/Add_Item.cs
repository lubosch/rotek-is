﻿using RotekIS.General;
using RotekIS.SQL;
using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using RotekIS.Forms;


namespace RotekIS.Stocks
{
    public partial class Add_Item : RotekIS.Stocks.Inter_Form, IDisposable
    {
        private int mode;
        private string name;
        private string surname;
        private int stockID;
        private Dictionary<int, string> parameterers;
        private List<Textac> textace;
        private List<Lister> listere;
        private List<Napis> popisy;
        private List<BindingSource> bss;
        private string[] temp_values;
        private int ino_to_change;

        public Add_Item(int mod, string name, string surname, int stockID, string stock)
        {
            InitializeComponent();
            this.name = name;
            this.surname = surname;
            this.stockID = stockID;
            textace = new List<Textac>();
            listere = new List<Lister>();
            popisy = new List<Napis>();

            mode = mod;
            switch (mod)
            {
                case 1:
                    this.nadpis_set("Pridať");
                    if (name != null) napis6.Text = "Z " + stock + " pridať pre " + name + " " + surname; else napis6.Text = "Pridať do " + stock;

                    my_Button1.Text = "Pridať";
                    break;
                case 2:
                    napis6.Text = name + " " + surname + " vracia do " + stock;
                    this.nadpis_set("Vrátiť");
                    napis2.Hide();
                    textac2.Hide();
                    button2.Hide();
                    napis3.Hide();
                    textac3.Hide();
                    napis4.Hide();
                    textac4.Hide();
                    textac5.Hide();
                    napis5.Hide();
                    my_Button1.Text = "Vrátiť";
                    break;
                case 3:
                    this.nadpis_set("Zničiť");
                    if (name != null) napis6.Text = name + " " + surname + " zničil z " + stock; else napis6.Text = "Zničiť v " + stock;
                    napis2.Hide();
                    textac2.Hide();
                    button2.Hide();
                    napis3.Hide();
                    textac3.Hide();
                    napis4.Hide();
                    textac4.Hide();
                    textac5.Hide();
                    napis5.Hide();
                    my_Button1.Text = "Zničiť";
                    break;
                case 4:
                    this.nadpis_set("Zlúčiť");
                    break;
                case 5:
                    this.nadpis_set("Presunúť na predaj");
                    break;
                case 6:
                    napis1.Hide();
                    textac1.Hide();
                    textac1.Text = "1";

                    napis6.Text = "Presunúť do " + stock;
                    this.nadpis_set("Presunúť na iný sklad");
                    break;

                default:
                    Error.Show("Neexistujuca volba");
                    break;
            }

            if (mode == 1 && name != null || mode == 4 || mode == 6)
            {
                napis2.Hide();
                textac2.Hide();
                button2.Hide();
                napis3.Hide();
                textac3.Hide();
                napis4.Hide();
                textac4.Hide();
                textac5.Hide();
                napis5.Hide();
            }

            parameterers = Stock_SQL.GetParameters(stockID);

        }
        public Add_Item(int mod, string name, string surname, int stockID, string stock, string[] values)
            : this(mod, name, surname, stockID, stock)
        {
            ;
            temp_values = values;
        }
        public Add_Item(int mod, string name, string surname, int stockID, string stock, int itemNo)
            : this(mod, name, surname, stockID, stock)
        {

            ino_to_change = itemNo;

        }
        public Add_Item(int mod, string name, string surname, int stockID, string stock, int itemNo, string[] values)
            : this(mod, name, surname, stockID, stock)
        {
            temp_values = values;
            ino_to_change = itemNo;

        }



        private void Add_Item_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'rotekDataSet.Stock' table. You can move, or remove it, as needed.
            this.stockTableAdapter.Fill(this.rotekDataSet.Stock);

            try
            {
                bss = new List<BindingSource>();
                BindingSource bs = new BindingSource();
                DataSet ds;
                string[] toParams = new string[parameterers.Keys.Count];
                int i = 0;
                for (i = 0; i < parameterers.Keys.Count; i++)
                {
                    toParams[i] = "";
                }

                if (mode != 1 && name != null)
                {
                    ds = Stock_SQL.ListParameterItems(parameterers.Keys.ToArray<int>(), toParams, name, surname);
                }
                else
                {
                    ds = Stock_SQL.ListParameterItems(parameterers.Keys.ToArray<int>(), toParams);
                }
                int offset = 190;
                i = 0;
                foreach (int ID in parameterers.Keys.ToArray())
                {
                    textace.Add(new Textac());
                    panel1.Controls.Add(textace[i]);
                    textace[i].Location = new Point(i * offset + 11, 43);
                    textace[i].TabIndex = i;
                    textace[i].Tag = i;
                    textace[i].KeyUp += Add_Item_KeyUp;
                    //textace[i].Leave += Add_Item_Leave;

                    popisy.Add(new Napis());
                    panel1.Controls.Add(popisy[i]);
                    popisy[i].Location = new Point(i * offset + 11, 20);
                    popisy[i].Text = parameterers[ID];
                    popisy[i].Tag = ID;

                    listere.Add(new Lister());
                    panel1.Controls.Add(listere[i]);
                    listere[i].Location = new Point(i * offset + 11, 73);
                    listere[i].TabIndex = 102 + i;
                    bss.Add(new BindingSource());
                    bss[i].DataSource = ds.Copy().Tables[0];
                    listere[i].DataSource = bss[i];
                    bss[i].Filter = "Parameters_ID= " + ID;
                    //Error.Show(ds.Clone().Tables[0].Rows.Count);
                    listere[i].DisplayMember = "Name";
                    //listere[i].HorizontalScrollbar = true;
                    listere[i].MouseClick += Add_Item_SelectedIndexChanged;

                    //listere[i].SelectedIndexChanged += Add_Item_SelectedIndexChanged;
                    listere[i].Tag = i;
                    listere[i].MouseClick += listac_SelectedIndexChanged;
                    i++;
                }
                this.Width = this.Width + (i - 1) * offset;
                textace[0].Select();
                if (temp_values != null)
                {
                    for (i = 0; i < textace.Count; i++)
                    {
                        textace[i].Text = temp_values[i];
                    }
                    textac1.Select();
                }
            }
            catch (System.Exception ex)
            {
                Error.Show(ex);
            }
            //Error.Show("OK");
        }

        void Add_Item_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Lister l = (Lister)sender;
                if (l.SelectedIndex == null || l.SelectedIndex == -1) return;
                string s = ((DataRowView)listere[(int)l.Tag].SelectedItem).Row[0].ToString();
                //Error.Show(s + (int)l.Tag);
                textace[(int)l.Tag].Text = s;

                filtrik(textace[(int)(l.Tag)]);
                textace[(int)l.Tag].Select();
                textace[(int)l.Tag].SelectAll();

            }
            catch (System.Exception ex)
            {
                Error.Show(ex);
            }
        }


        void Add_Item_KeyUp(object sender, KeyEventArgs e)
        {
            //Error.Show("bb");
            if (e.KeyCode == Keys.Up)
            {
                Posun(1, (Textac)sender, listere[(int)((Textac)sender).Tag]);
            }
            else if (e.KeyCode == Keys.Down)
            {
                Posun(0, (Textac)sender, listere[(int)((Textac)sender).Tag]);
            }
            else if (e.KeyCode == Keys.Enter)
            {
                Confirm();
            }
            else if (Keys.Back == e.KeyCode || e.KeyValue == 96 || char.IsLetterOrDigit((char)e.KeyValue))
            {
                //foreach ( ListBox l in listere)
                //{
                //    l.SelectedIndex = -1;
                //}

                filtrik(null);

            }
        }



        private void filtrik(Textac sender)
        {
            Textac tx = (sender);
            //int poradie = (int)tx.Tag;
            //int ID = (int)popisy[poradie].Tag;

            string[] values = new string[textace.Count];
            int i = 0;
            for (i = 0; i < textace.Count; i++)
            {
                values[i] = textace[i].Text;
            }

            DataSet ds;
            if (mode != 1 && name != null)
            {
                ds = Stock_SQL.ListParameterItems(parameterers.Keys.ToArray<int>(), values, name, surname);
            }
            else
            {
                ds = Stock_SQL.ListParameterItems(parameterers.Keys.ToArray<int>(), values);
            }

            for (i = 0; i < textace.Count; i++)
            {
                if (textace[i] != tx)
                {
                    bss[i].DataSource = ds.Copy().Tables[0];

                }

            }
        }

        private void Posun(int smer, Textac textac, Lister lister)
        {
            if (smer == 1 && lister.SelectedIndex <= 0)
            {
                lister.SelectedIndex = lister.Items.Count - 1;
                //textac.Text = ((DataRowView)lister.Items[lister.Items.Count - 1]).Row[0].ToString();
            }
            else if (smer == 1)
            {
                lister.SelectedIndex--;
                //textac.Text = ((DataRowView)lister.Items[lister.SelectedIndex - 1]).Row[0].ToString();
            }
            else if (smer == 0 && lister.SelectedIndex >= lister.Items.Count - 1)
            {
                lister.SelectedIndex = 0;
                //textac.Text = ((DataRowView)lister.Items[0]).Row[0].ToString();
            }
            else if (smer == 0)
            {
                lister.SelectedIndex++;
                //textac.Text = ((DataRowView)lister.Items[lister.SelectedIndex + 1]).Row[0].ToString();
            }
            Add_Item_SelectedIndexChanged(lister, null);
            //listac_SelectedIndexChanged(lister, null);
        }


        private void listac_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        private void my_Button1_Click(object sender, EventArgs e)
        {
            Confirm();
        }


        private void Confirm()
        {
            string[] values = new string[textace.Count];
            int i = 0;
            foreach (Textac t in textace)
            {
                if (t.Text == "")
                {
                    Error.Show("Nevyplnene vsetky parametre");
                    return;
                }
                values[i] = t.Text.Trim();
                i++;
            }

            string photo = null;
            if (textac2.Text != "")
            {
                photo = Save_disk.Stock_Image(textac2.Text, stockID);
            }

            string note = null;
            if (textac4.Text != "")
            {
                note = textac4.Text.Trim();
            }
            string shelf = null;
            if (textac5.Text != "")
            {
                shelf = textac5.Text.Trim();
            }


            decimal count = 0, wrecked = 0;
            try
            {
                count = decimal.Parse(textac1.Text.Replace('.', ','));
                if (mode == 2) count = 0 - count;
                if (mode == 3)
                {
                    wrecked = count;
                    count = 0 - count;
                }
            }
            catch (Exception ex)
            {
                Error.Show("Nesprávne zadaný počet");
                return;
            }
            decimal cost = 0;
            if (textac3.Text != "")
            {
                textac3.Text = textac3.Text.Replace('.', ',');
                try
                {
                    cost = decimal.Parse(textac3.Text);
                }
                catch (Exception ex)
                {
                    Error.Show("Nespravne zadany format ceny");
                    return;
                }
            }

            try
            {

                if (name != null)
                {
                    Item_SQL item = new Item_SQL(stockID, values);
                    if (item.count >= count || Error.Question("Na sklade je menej (" + item.count + ") ako chcete zobrať. Chcete aj tak zobrať?"))
                    {
                        Employer_Stock.AddItemEmployer(name, surname, stockID, values, count, wrecked);
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    if (mode == 6)
                    {
                        Employer_Stock.MoveToAnotherStock(ino_to_change, values, stockID);
                    }
                    else if (mode != 5)
                    {
                        Employer_Stock.AddItem(stockID, values, note, photo, shelf, count, wrecked, cost, mode);
                    }
                    else
                    {
                        int sold_stockID = Stock_SQL.stock_sold_id(stockID);
                        Employer_Stock.AddItem(sold_stockID, values, note, photo, shelf, count, wrecked, cost, mode);
                    }

                    if (mode == 4 || mode == 5)
                    {
                        Employer_Stock.Disappear(ino_to_change, count);
                    }
                }
            }
            catch (MS_SQL_Exception ex)
            {
                Error.Show(ex.Message);
                return;
            }
            if (checkBox1.Checked)
            {
                Dispose();
            }
        }

        private void textac_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Confirm();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                Dispose();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textac2.Text = openFileDialog1.FileName;
            }
        }

        private void my_Button3_Click(object sender, EventArgs e)
        {
            Close();
        }

    }
}
