﻿namespace RotekIS.Stocks
{
    partial class Add_Stock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Add_Stock));
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("");
            this.napis1 = new RotekIS.Forms.Napis();
            this.name = new RotekIS.Forms.Textac();
            this.napis2 = new RotekIS.Forms.Napis();
            this.parameter = new RotekIS.Forms.Textac();
            this.add_parameter = new System.Windows.Forms.Button();
            this.items2 = new RotekIS.Forms.Lister();
            this.my_Button3 = new RotekIS.my_Button();
            this.my_Button1 = new RotekIS.my_Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.premenovaťToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zmazaťToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zmeniťVyhľadávanieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.všadeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zĽavaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.presneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.napis3 = new RotekIS.Forms.Napis();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.panel1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radioButton3);
            this.panel1.Controls.Add(this.listView1);
            this.panel1.Controls.Add(this.radioButton2);
            this.panel1.Controls.Add(this.radioButton1);
            this.panel1.Controls.Add(this.napis3);
            this.panel1.Controls.Add(this.items2);
            this.panel1.Controls.Add(this.add_parameter);
            this.panel1.Controls.Add(this.parameter);
            this.panel1.Controls.Add(this.napis2);
            this.panel1.Controls.Add(this.name);
            this.panel1.Controls.Add(this.napis1);
            this.panel1.Controls.Add(this.my_Button3);
            this.panel1.Controls.Add(this.my_Button1);
            this.panel1.Size = new System.Drawing.Size(646, 331);
            // 
            // napis1
            // 
            this.napis1.AutoSize = true;
            this.napis1.Font = new System.Drawing.Font("Verdana", 10F);
            this.napis1.Location = new System.Drawing.Point(11, 9);
            this.napis1.Name = "napis1";
            this.napis1.Size = new System.Drawing.Size(51, 17);
            this.napis1.TabIndex = 0;
            this.napis1.Text = "Názov";
            // 
            // name
            // 
            this.name.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.name.Location = new System.Drawing.Point(14, 29);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(163, 23);
            this.name.TabIndex = 0;
            // 
            // napis2
            // 
            this.napis2.AutoSize = true;
            this.napis2.Font = new System.Drawing.Font("Verdana", 10F);
            this.napis2.Location = new System.Drawing.Point(187, 9);
            this.napis2.Name = "napis2";
            this.napis2.Size = new System.Drawing.Size(79, 17);
            this.napis2.TabIndex = 2;
            this.napis2.Text = "Parameter";
            // 
            // parameter
            // 
            this.parameter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.parameter.Location = new System.Drawing.Point(190, 29);
            this.parameter.Name = "parameter";
            this.parameter.Size = new System.Drawing.Size(124, 23);
            this.parameter.TabIndex = 1;
            this.parameter.KeyUp += new System.Windows.Forms.KeyEventHandler(this.parameter_KeyUp);
            // 
            // add_parameter
            // 
            this.add_parameter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.add_parameter.Location = new System.Drawing.Point(508, 24);
            this.add_parameter.Name = "add_parameter";
            this.add_parameter.Size = new System.Drawing.Size(125, 31);
            this.add_parameter.TabIndex = 2;
            this.add_parameter.Text = "Pridať parameter";
            this.add_parameter.UseVisualStyleBackColor = true;
            this.add_parameter.Click += new System.EventHandler(this.button1_Click);
            // 
            // items2
            // 
            this.items2.Font = new System.Drawing.Font("Verdana", 10F);
            this.items2.FormattingEnabled = true;
            this.items2.ItemHeight = 16;
            this.items2.Location = new System.Drawing.Point(14, 58);
            this.items2.Name = "items2";
            this.items2.Size = new System.Drawing.Size(163, 212);
            this.items2.TabIndex = 5;
            // 
            // my_Button3
            // 
            this.my_Button3.BackColor = System.Drawing.Color.Transparent;
            this.my_Button3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("my_Button3.BackgroundImage")));
            this.my_Button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.my_Button3.FlatAppearance.BorderSize = 0;
            this.my_Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.my_Button3.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.my_Button3.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.my_Button3.Location = new System.Drawing.Point(14, 276);
            this.my_Button3.Name = "my_Button3";
            this.my_Button3.Size = new System.Drawing.Size(140, 41);
            this.my_Button3.TabIndex = 7;
            this.my_Button3.Text = "Zrušiť";
            this.my_Button3.UseVisualStyleBackColor = false;
            this.my_Button3.Click += new System.EventHandler(this.my_Button2_Click);
            // 
            // my_Button1
            // 
            this.my_Button1.BackColor = System.Drawing.Color.Transparent;
            this.my_Button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("my_Button1.BackgroundImage")));
            this.my_Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.my_Button1.FlatAppearance.BorderSize = 0;
            this.my_Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.my_Button1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.my_Button1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.my_Button1.Location = new System.Drawing.Point(205, 276);
            this.my_Button1.Name = "my_Button1";
            this.my_Button1.Size = new System.Drawing.Size(148, 41);
            this.my_Button1.TabIndex = 8;
            this.my_Button1.Text = "Vytvoriť sklad";
            this.my_Button1.UseVisualStyleBackColor = false;
            this.my_Button1.Click += new System.EventHandler(this.my_Button1_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.premenovaťToolStripMenuItem,
            this.zmazaťToolStripMenuItem,
            this.zmeniťVyhľadávanieToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(187, 92);
            // 
            // premenovaťToolStripMenuItem
            // 
            this.premenovaťToolStripMenuItem.Name = "premenovaťToolStripMenuItem";
            this.premenovaťToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.premenovaťToolStripMenuItem.Text = "Premenovať";
            this.premenovaťToolStripMenuItem.Click += new System.EventHandler(this.premenovaťToolStripMenuItem_Click);
            // 
            // zmazaťToolStripMenuItem
            // 
            this.zmazaťToolStripMenuItem.Name = "zmazaťToolStripMenuItem";
            this.zmazaťToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.zmazaťToolStripMenuItem.Text = "Zmazať";
            this.zmazaťToolStripMenuItem.Click += new System.EventHandler(this.zmazaťToolStripMenuItem_Click);
            // 
            // zmeniťVyhľadávanieToolStripMenuItem
            // 
            this.zmeniťVyhľadávanieToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.všadeToolStripMenuItem,
            this.zĽavaToolStripMenuItem,
            this.presneToolStripMenuItem});
            this.zmeniťVyhľadávanieToolStripMenuItem.Name = "zmeniťVyhľadávanieToolStripMenuItem";
            this.zmeniťVyhľadávanieToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.zmeniťVyhľadávanieToolStripMenuItem.Text = "Zmeniť vyhľadávanie";
            // 
            // všadeToolStripMenuItem
            // 
            this.všadeToolStripMenuItem.Name = "všadeToolStripMenuItem";
            this.všadeToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.všadeToolStripMenuItem.Text = "Všade";
            this.všadeToolStripMenuItem.Click += new System.EventHandler(this.všadeToolStripMenuItem_Click);
            // 
            // zĽavaToolStripMenuItem
            // 
            this.zĽavaToolStripMenuItem.Name = "zĽavaToolStripMenuItem";
            this.zĽavaToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.zĽavaToolStripMenuItem.Text = "Z ľava";
            this.zĽavaToolStripMenuItem.Click += new System.EventHandler(this.zĽavaToolStripMenuItem_Click);
            // 
            // presneToolStripMenuItem
            // 
            this.presneToolStripMenuItem.Name = "presneToolStripMenuItem";
            this.presneToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.presneToolStripMenuItem.Text = "Presne";
            this.presneToolStripMenuItem.Click += new System.EventHandler(this.presneToolStripMenuItem_Click);
            // 
            // napis3
            // 
            this.napis3.AutoSize = true;
            this.napis3.Font = new System.Drawing.Font("Verdana", 10F);
            this.napis3.Location = new System.Drawing.Point(363, 9);
            this.napis3.Name = "napis3";
            this.napis3.Size = new System.Drawing.Size(100, 17);
            this.napis3.TabIndex = 9;
            this.napis3.Text = "Vyhľadávanie";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Font = new System.Drawing.Font("Verdana", 10F);
            this.radioButton1.Location = new System.Drawing.Point(420, 30);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(69, 21);
            this.radioButton1.TabIndex = 10;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Všade";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Font = new System.Drawing.Font("Verdana", 10F);
            this.radioButton2.Location = new System.Drawing.Point(420, 58);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(69, 21);
            this.radioButton2.TabIndex = 11;
            this.radioButton2.Text = "Z ľava";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listView1.Font = new System.Drawing.Font("Verdana", 10F);
            this.listView1.FullRowSelect = true;
            this.listView1.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1});
            this.listView1.Location = new System.Drawing.Point(190, 58);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(224, 212);
            this.listView1.TabIndex = 12;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.items1_MouseUp);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Názov";
            this.columnHeader1.Width = 120;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Vyhľadávanie";
            this.columnHeader2.Width = 100;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Font = new System.Drawing.Font("Verdana", 10F);
            this.radioButton3.Location = new System.Drawing.Point(420, 83);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(73, 21);
            this.radioButton3.TabIndex = 13;
            this.radioButton3.Text = "Presne";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // Add_Stock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(646, 356);
            this.Name = "Add_Stock";
            this.Load += new System.EventHandler(this.Add_Stock_Load);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Forms.Napis napis1;
        private Forms.Textac name;
        private System.Windows.Forms.Button add_parameter;
        private Forms.Textac parameter;
        private Forms.Napis napis2;
        private Forms.Lister items2;
        private my_Button my_Button3;
        private my_Button my_Button1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem premenovaťToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zmazaťToolStripMenuItem;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private Forms.Napis napis3;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ToolStripMenuItem zmeniťVyhľadávanieToolStripMenuItem;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.ToolStripMenuItem všadeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zĽavaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem presneToolStripMenuItem;
    }
}
