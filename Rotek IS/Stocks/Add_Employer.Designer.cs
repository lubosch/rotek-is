﻿namespace RotekIS.Stocks
{
    partial class Add_Employer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Add_Employer));
            this.napis1 = new RotekIS.Forms.Napis();
            this.textac1 = new RotekIS.Forms.Textac();
            this.napis2 = new RotekIS.Forms.Napis();
            this.textac2 = new RotekIS.Forms.Textac();
            this.my_Button3 = new RotekIS.my_Button();
            this.my_Button2 = new RotekIS.my_Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textac2);
            this.panel1.Controls.Add(this.napis2);
            this.panel1.Controls.Add(this.textac1);
            this.panel1.Controls.Add(this.napis1);
            this.panel1.Controls.Add(this.my_Button3);
            this.panel1.Controls.Add(this.my_Button2);
            this.panel1.Size = new System.Drawing.Size(390, 150);
            // 
            // napis1
            // 
            this.napis1.AutoSize = true;
            this.napis1.Font = new System.Drawing.Font("Verdana", 10F);
            this.napis1.Location = new System.Drawing.Point(11, 9);
            this.napis1.Name = "napis1";
            this.napis1.Size = new System.Drawing.Size(45, 17);
            this.napis1.TabIndex = 14;
            this.napis1.Text = "Meno";
            // 
            // textac1
            // 
            this.textac1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.textac1.Location = new System.Drawing.Point(14, 29);
            this.textac1.Name = "textac1";
            this.textac1.Size = new System.Drawing.Size(163, 23);
            this.textac1.TabIndex = 0;
            // 
            // napis2
            // 
            this.napis2.AutoSize = true;
            this.napis2.Font = new System.Drawing.Font("Verdana", 10F);
            this.napis2.Location = new System.Drawing.Point(11, 55);
            this.napis2.Name = "napis2";
            this.napis2.Size = new System.Drawing.Size(77, 17);
            this.napis2.TabIndex = 16;
            this.napis2.Text = "Priezvisko";
            // 
            // textac2
            // 
            this.textac2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.textac2.Location = new System.Drawing.Point(14, 75);
            this.textac2.Name = "textac2";
            this.textac2.Size = new System.Drawing.Size(163, 23);
            this.textac2.TabIndex = 1;
            this.textac2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textac2_KeyUp);
            // 
            // my_Button3
            // 
            this.my_Button3.BackColor = System.Drawing.Color.Transparent;
            this.my_Button3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("my_Button3.BackgroundImage")));
            this.my_Button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.my_Button3.FlatAppearance.BorderSize = 0;
            this.my_Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.my_Button3.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.my_Button3.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.my_Button3.Location = new System.Drawing.Point(11, 104);
            this.my_Button3.Name = "my_Button3";
            this.my_Button3.Size = new System.Drawing.Size(140, 41);
            this.my_Button3.TabIndex = 3;
            this.my_Button3.Text = "Zrušiť";
            this.my_Button3.UseVisualStyleBackColor = false;
            this.my_Button3.Click += new System.EventHandler(this.my_Button2_Click);
            // 
            // my_Button2
            // 
            this.my_Button2.BackColor = System.Drawing.Color.Transparent;
            this.my_Button2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("my_Button2.BackgroundImage")));
            this.my_Button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.my_Button2.FlatAppearance.BorderSize = 0;
            this.my_Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.my_Button2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.my_Button2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.my_Button2.Location = new System.Drawing.Point(237, 104);
            this.my_Button2.Name = "my_Button2";
            this.my_Button2.Size = new System.Drawing.Size(140, 41);
            this.my_Button2.TabIndex = 2;
            this.my_Button2.Text = "Pridať";
            this.my_Button2.UseVisualStyleBackColor = false;
            this.my_Button2.Click += new System.EventHandler(this.my_Button2_Click_1);
            // 
            // Add_Employer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(390, 175);
            this.Name = "Add_Employer";
            this.Load += new System.EventHandler(this.Add_Employer_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Forms.Textac textac2;
        private Forms.Napis napis2;
        private Forms.Textac textac1;
        private Forms.Napis napis1;
        private my_Button my_Button3;
        private my_Button my_Button2;

    }
}
