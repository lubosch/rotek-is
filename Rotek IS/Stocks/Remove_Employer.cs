﻿using RotekIS.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Controls;
using System.Windows.Forms;
using RotekIS.General;

namespace RotekIS.Stocks
{
    public partial class Remove_Employer : RotekIS.Stocks.Inter_Form
    {
        public Remove_Employer()
            : base("Vymazať zamestnanca")
        {
            InitializeComponent();
        }

        private void Remove_Employer_Load(object sender, EventArgs e)
        {
            napl_list();
            textac1.Select();
        }

        private void my_Button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void napl_list()
        {
            items1.DataSource = Employer.Name_Surname("").Tables[0];
            items1.DisplayMember = "NameC";
        }

        private void textac1_TextChanged(object sender, EventArgs e)
        {
            items1.DataSource = Employer.Name_Surname(textac1.Text).Tables[0];
            items1.DisplayMember = "NameC";

        }

        private void items1_MouseClick(object sender, MouseEventArgs e)
        {
            if (items1.SelectedIndex == -1) return;
            string text = ((DataRowView)items1.SelectedValue)[0].ToString();
            textac2.Text = text.Split(new[] { ' ' }, 2)[0];
            textac1.Text = text.Split(new[] { ' ' }, 2)[1];
            textac1.SelectAll();

        }

        private void my_Button2_Click_1(object sender, EventArgs e)
        {
            zmaz();
        }

        private void zmaz()
        {
            if (textac1.Text == "" || textac2.Text == "")
            {
                Error.Show("Nevyplnene meno a priezvisko");
            }
            Employer.Remove(textac1.Text, textac2.Text);
            Dispose();
        }


    }
}
