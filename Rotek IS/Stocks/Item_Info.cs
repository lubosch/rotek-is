﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using RotekIS.SQL;

namespace RotekIS.Stocks
{
    public partial class Item_Info : RotekIS.Stocks.Inter_Form
    {
        private string[] values;
        private int stockID;
        private Dictionary<int, string> parameterers;


        public Item_Info(string[] values, int stockID)
        {
            InitializeComponent();
            this.stockID = stockID;
            this.values = values;
            parameterers = Stock_SQL.GetParameters(stockID);
            headline1.Text = "";
            foreach (string s in values)
            {
                headline1.Text += s + " ";
            }
        }

        private void Item_Info_Load(object sender, EventArgs e)
        {
            DataSet ds = Stock_SQL.ItemInfo(parameterers.Keys.ToArray(), values);
            tabulka1.DataSource = ds.Tables[0];

            DataSet ds2 = Stock_SQL.ItemHistory(parameterers.Keys.ToArray(), values);
            DataTable dt = ds2.Tables[0];

            int i = 0;
            while (i < dt.Rows.Count)
            {
                //if (dt.Rows[i]["Meno"] == DBNull.Value && Double.Parse(dt.Rows[i]["Zmena poctu"].ToString()) < 0)
                //{
                //dt.Rows.RemoveAt(i);
                //}
                //else 
                if (dt.Rows[i]["Meno"] == DBNull.Value && (
                (i > 0 && Math.Abs((DateTime.Parse(dt.Rows[i]["Dátum"].ToString()) - DateTime.Parse(dt.Rows[i - 1]["Dátum"].ToString())).TotalSeconds) < 2) ||
                (i + 1 < dt.Rows.Count && Math.Abs((DateTime.Parse(dt.Rows[i]["Dátum"].ToString()) - DateTime.Parse(dt.Rows[i + 1]["Dátum"].ToString())).TotalSeconds) < 2)
                ))
                {
                    dt.Rows.RemoveAt(i);

                }
                else
                {
                    i += 1;
                }
            }



            tabulka2.DataSource = ds2.Tables[0];

        }

        private void tabulka2_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            // value.
            if (e.ColumnIndex == 0)
            {
                if (this.tabulka2.Rows[e.RowIndex].Cells["Meno"].Value == DBNull.Value)
                {

                    tabulka2.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightGreen;
                }
                else if (Decimal.Parse(tabulka2.Rows[e.RowIndex].Cells["Zmena poctu"].Value.ToString()) >= 0)
                {
                    tabulka2.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightPink;

                }
                else
                {
                    tabulka2.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightYellow;
                }
            }
        }
    }
}
