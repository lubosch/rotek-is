﻿using RotekIS.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RotekIS.Stocks;
using RotekIS.General;
using RotekIS.SQL;
using System.Diagnostics;

namespace RotekIS.Stocks
{
    public partial class Stock : Form
    {
        private DataSet employers;
        private DataSet stocks;
        private List<Textac> textace;
        private BindingSource BS;
        const int stlpcovy_pridavok = 8;
        const int pravy_pridavok = 3;
        private int page = 0;

        public Stock()
        {
            InitializeComponent();
        }

        private void poverenia()
        {
            switch (Main.Menu.uzivatel.typ)
            {
                case "admin":
                    my_Button1.Show();
                    my_Button2.Show();
                    my_Button3.Show();
                    my_Button4.Show();
                    my_Button5.Show();
                    button2.Show();
                    my_Button7.Show();
                    my_Button8.Show();
                    my_Button9.Show();
                    contextMenuStrip1.Items[0].Visible = true;
                    contextMenuStrip1.Items[1].Visible = true;
                    contextMenuStrip1.Items[2].Visible = true;
                    contextMenuStrip1.Items[4].Visible = true;
                    contextMenuStrip1.Items[5].Visible = true;
                    contextMenuStrip1.Items[6].Visible = true;
                    contextMenuStrip1.Items[7].Visible = true;
                    contextMenuStrip1.Items[8].Visible = true;

                    break;
                case "sklad":
                    my_Button1.Hide();
                    my_Button3.Show();
                    my_Button4.Show();
                    my_Button5.Show();
                    button2.Show();
                    my_Button8.Show();
                    my_Button7.Show();
                    my_Button2.Show();
                    my_Button9.Show();

                    contextMenuStrip1.Items[0].Visible = true;
                    contextMenuStrip1.Items[1].Visible = true;
                    contextMenuStrip1.Items[2].Visible = true;
                    contextMenuStrip1.Items[4].Visible = false;
                    contextMenuStrip1.Items[5].Visible = true;
                    contextMenuStrip1.Items[6].Visible = true;
                    contextMenuStrip1.Items[7].Visible = true;
                    contextMenuStrip1.Items[8].Visible = true;

                    break;
                default:
                    my_Button1.Hide();
                    my_Button2.Hide();
                    my_Button3.Hide();
                    my_Button4.Hide();
                    my_Button5.Hide();
                    button2.Hide();
                    my_Button7.Hide();
                    my_Button8.Hide();
                    my_Button9.Hide();
                    button5.Hide();
                    splitContainer5.SplitterDistance = 25;
                    splitContainer4.SplitterDistance = 25;
                    contextMenuStrip1.Items[0].Visible = false;
                    contextMenuStrip1.Items[1].Visible = false;
                    contextMenuStrip1.Items[2].Visible = false;
                    contextMenuStrip1.Items[4].Visible = false;
                    contextMenuStrip1.Items[5].Visible = false;
                    contextMenuStrip1.Items[6].Visible = false;
                    contextMenuStrip1.Items[7].Visible = false;
                    contextMenuStrip1.Items[8].Visible = false;

                    break;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (!Stock_SQL.should_reset() || (Main.Menu.uzivatel.typ != "sklad" && Main.Menu.uzivatel.typ != "admin"))
            {
                button5.Hide();
                int y_up = 45;
                radioButton1.Location = new Point(radioButton1.Location.X, radioButton1.Location.Y - y_up);
                radioButton2.Location = new Point(radioButton2.Location.X, radioButton2.Location.Y - y_up);
                dataRepeater1.Location = new Point(dataRepeater1.Location.X, dataRepeater1.Location.Y - y_up);
            }
            dateTimePicker1.Value = DateTime.Now.AddDays(1);
            //MessageBox.Show(ClientSize + "\n" + Size + "\n" + panel2.Size + "\n" + splitContainer1.Size.ToString() + splitContainer2.Size.ToString() + splitContainer3.Size.ToString() + splitContainer4.Size.ToString() + splitContainer5.Size.ToString());

            poverenia();
            BS = new BindingSource();
            textace = new List<Textac>();
            ripiter();
            stocky();
            headline1.Text = "Sklady";
            stock_change();
            //tabulka.Size = new Size(tabulka.Width, splitContainer4.Height - tabulka.Location.Y - 50);

            //employersBS= employers.Tables[0];

        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            button1.BackColor = Color.Silver;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            button1.BackColor = Color.White;
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Control | Keys.F))
            {
                TextDialog txtdialog = new TextDialog("Najdi", "Nájsť vo všetkých skladoch. Zadať len 1 názov:");
                if (txtdialog.ShowDialog() == DialogResult.OK)
                {
                    stock_change(txtdialog.text);

                }
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void button1_Paint(object sender, PaintEventArgs e)
        {
            //Button b = sender as Button;
            //System.Drawing.Drawing2D.GraphicsPath shape = new System.Drawing.Drawing2D.GraphicsPath();

            //shape.AddArc(-20, -20, 30, 30, 0, 90);
            //e.Graphics.DrawPath(new Pen(Color.FromArgb(64, 64, 64), 15), shape);

            //shape = new System.Drawing.Drawing2D.GraphicsPath();
            //shape.AddArc(b.Width - 10, -20, 30, 30, 90, 90);
            //e.Graphics.DrawPath(new Pen(Color.FromArgb(64, 64, 64), 15), shape);

            //shape = new System.Drawing.Drawing2D.GraphicsPath();
            //shape.AddArc(-20, b.Height - 10, 30, 30, 270, 90);
            //e.Graphics.DrawPath(new Pen(Color.FromArgb(64, 64, 64), 15), shape);

            //shape = new System.Drawing.Drawing2D.GraphicsPath();
            //shape.AddArc(b.Width - 10, b.Height - 10, 30, 30, 180, 90);
            ////shape.AddArc(-20, -20, 30, 30, 0, 90);
            //e.Graphics.DrawPath(new Pen(Color.FromArgb(64, 64, 64), 15), shape);

        }

        private void my_Button3_Click(object sender, EventArgs e)
        {
            if (my_Button8.Text == "Zobraziť sklad")
            {
                if (tabulka.SelectedCells.Count == 1)
                {
                    int stock_id = int.Parse(tabulka["stock_id", tabulka.SelectedCells[0].RowIndex].Value.ToString());
                    int new_index = stocks.Tables[0].AsEnumerable().Select(r => r.Field<int>("ID")).ToList().FindIndex(col => col == stock_id);
                    if (dataRepeater2.CurrentItemIndex == new_index)
                    {
                        stock_change();
                    }
                    else
                    {
                        dataRepeater2.CurrentItemIndex = new_index;
                    }

                    //((Label)(dataRepeater2.CurrentItem.Controls.Find("DR_stock", true)[0])).Text;
                    //int stock_ID = int.Parse(stocks.Tables[0].Rows[dataRepeater2.CurrentItemIndex][0].ToString());
                }
            }
            else
            {
                Add_Item_Dialog(1);
            }
        }

        private void my_Button4_Click(object sender, EventArgs e)
        {
            Add_Item_Dialog(2);
        }

        private void Move_Item_Dialog(int row, int stock_id, string stockName)
        {
            int itemno = (int)tabulka["ItemNo_ID", row].Value;

            int cols = Stock_SQL.GetParameters(stock_id).Count;
            string[] values = new string[cols];

            for (int i = 0; i < cols; i++)
            {
                values[i] = tabulka[i, row].Value.ToString();
            }


            Add_Item f = new Add_Item(6, null, null, stock_id, stockName, itemno, values);
            f.ShowDialog();
            stock_change();

        }

        private void Add_Item_Dialog(int mode)
        {
            Add_Item_Dialog(mode, -2);
        }
        private void Add_Item_Dialog(int mode, int row)
        {
            Label l1 = (Label)(dataRepeater2.CurrentItem.Controls.Find("DR_stock", true)[0]);
            Label l3 = (Label)(dataRepeater1.CurrentItem.Controls.Find("DR_Name", true)[0]);
            Label l4 = (Label)(dataRepeater1.CurrentItem.Controls.Find("DR_Surname", true)[0]);
            Add_Item f;
            if (row == -2)
            {
                if (button1.Tag == "Off")
                {
                    f = new Add_Item(mode, l3.Text, l4.Text, int.Parse(stocks.Tables[0].Rows[dataRepeater2.CurrentItemIndex][0].ToString()), stocks.Tables[0].Rows[dataRepeater2.CurrentItemIndex][1].ToString());
                    f.ShowDialog();
                }
                else
                {
                    f = new Add_Item(mode, null, null, int.Parse(stocks.Tables[0].Rows[dataRepeater2.CurrentItemIndex][0].ToString()), stocks.Tables[0].Rows[dataRepeater2.CurrentItemIndex][1].ToString());
                    f.ShowDialog();
                }
            }
            else
            {
                int itemno = (int)tabulka["ItemNo_ID", row].Value;
                string stockName = stocks.Tables[0].Rows[dataRepeater2.CurrentItemIndex][1].ToString();
                int stockID = int.Parse(stocks.Tables[0].Rows[dataRepeater2.CurrentItemIndex][0].ToString());


                string[] values = new string[tabulka.Columns.Count - stlpcovy_pridavok];
                int i = 0;
                for (i = 0; i < tabulka.Columns.Count - stlpcovy_pridavok; i++)
                {
                    values[i] = tabulka[i, row].Value.ToString();
                }

                if (button1.Tag == "Off")
                {
                    f = new Add_Item(mode, l3.Text, l4.Text, stockID, stockName, values);
                }
                else if (mode == 4 || mode == 6)
                {
                    f = new Add_Item(mode, null, null, stockID, stockName, itemno);
                }
                else if (mode == 5)
                {
                    if (Stock_SQL.stock_sold_id(stockID) == -1)
                    {
                        if (Error.Question("Neexistuje sklad pre preadaj. Vytvoriť nový?"))
                        {
                            Stock_SQL.AddSold(stockID);
                            Nacitaj_sklady();
                        }
                        else
                        {
                            return;
                        }
                    }

                    f = new Add_Item(mode, null, null, stockID, stockName, itemno, values);
                }
                else
                {
                    f = new Add_Item(mode, null, null, stockID, stockName, values);
                }

                f.ShowDialog();


            }
            stock_change();

        }

        private void my_Button5_Click(object sender, EventArgs e)
        {
            Add_Item_Dialog(3);

        }

        private void my_Button1_Click(object sender, EventArgs e)
        {
            int typ = 0;
            if (radioButton2.Checked) typ = 1;
            Add_Employer sk = new Add_Employer(typ);
            sk.ShowDialog();
            Nacitaj_zamestnancov();
        }

        private void my_Button7_Click(object sender, EventArgs e)
        {
            Add_Stock f = new Add_Stock(1);
            f.ShowDialog();
            Nacitaj_sklady();
        }

        private void my_Button8_Click(object sender, EventArgs e)
        {
            Add_Stock f = new Add_Stock(2);
            f.ShowDialog();
            Nacitaj_sklady();
        }

        private void my_Button2_Click(object sender, EventArgs e)
        {
            Remove_Employer f = new Remove_Employer();
            f.ShowDialog();
            Nacitaj_zamestnancov();
        }

        private void my_Button6_Click(object sender, EventArgs e)
        {
            //Error.Show("Coming sooon :)");
            // .Tables[0].Merge() Employer.List();
            Export.export(tabulka);
        }

        private void Nacitaj_zamestnancov()
        {

            if (radioButton2.Checked)
            {
                employers = Employer.List_nezamestnanci();

            }
            else if (radioButton1.Checked)
            {
                employers = Employer.List_zamestnanci();
            }
            dataRepeater1.DataSource = employers.Tables[0].DefaultView;


        }

        private void Nacitaj_sklady()
        {
            stocks = Stock_SQL.List();
            dataRepeater2.DataSource = stocks.Tables[0].DefaultView;

        }

        private void ripiter()
        {

            Nacitaj_zamestnancov();

            DR_name.DataBindings.Add(new Binding("Text", employers.Tables[0], "Name", true));
            DR_surname.DataBindings.Add(new Binding("Text", employers.Tables[0], "Surname", true));
            Nacitaj_zamestnancov();

        }
        private void stocky()
        {

            stocks = Stock_SQL.List();

            DR_stock.DataBindings.Add(new Binding("Text", stocks.Tables[0], "Name", true));

            dataRepeater2.DataSource = stocks.Tables[0].DefaultView;
            //dataRepeater2.DataMember = "Name";
        }


        private void DR_IndexChange(object sender, EventArgs e)
        {
            stock_change();
        }


        private void headline1_TextChanged(object sender, EventArgs e)
        {
            System.Drawing.Graphics g = this.CreateGraphics();
            SizeF strSz = g.MeasureString(headline1.Text, headline1.Font);
            int stred;
            stred = (int)(strSz.Width / 2);
            int rw = this.Width / 2 - stred;
            headline1.Location = new Point(rw, 0);
        }

        private int current_stock_ID()
        {
            return Stock_SQL.stock_ID(current_stock());
        }


        private String current_stock()
        {
            return ((Label)(dataRepeater2.CurrentItem.Controls.Find("DR_stock", true)[0])).Text;
        }
        private void stock_change()
        {
            stock_change("");
        }
        private void stock_change(String query)
        {

            if (dataRepeater2.ItemCount == 0) return;

            tabulka.Columns.Clear();

            try
            {

                String sklad_nazov = current_stock();
                bool change_everithing = false;
                bool isSold = false;
                if (sklad_nazov.LastIndexOf(" - predaj") > -1)
                {
                    isSold = true;
                }
                else
                {
                    isSold = false;
                }

                string newHeadline;
                int stock_ID = int.Parse(stocks.Tables[0].Rows[dataRepeater2.CurrentItemIndex][0].ToString());
                DataSet ds;
                if (query != "")
                {
                    ds = Employer_Stock.List(query, 200, page);
                    foreach (var column in ds.Tables[0].Columns.Cast<DataColumn>().ToArray())
                    {
                        if (ds.Tables[0].AsEnumerable().All(dr => dr.IsNull(column)))
                            ds.Tables[0].Columns.Remove(column);
                    }
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        ds = null;
                    }
                    newHeadline = "Nájsť v skladoch";
                    my_Button4.Hide();
                    my_Button3.Hide();
                    contextMenuStrip1.Items[1].Visible = false;
                    my_Button8.Text = "Zobraziť sklad";
                }
                else if (button1.Tag == "Off")
                {
                    String meno_zamestnanec = ((Label)(dataRepeater1.CurrentItem.Controls.Find("DR_Name", true)[0])).Text;
                    String priezvisko_zamestnanec = ((Label)(dataRepeater1.CurrentItem.Controls.Find("DR_Surname", true)[0])).Text;
                    newHeadline = meno_zamestnanec + " " + priezvisko_zamestnanec + " / " + sklad_nazov;

                    my_Button4.Text = "Zničiť";
                    my_Button8.Text = "Požičať";
                    if (Main.Menu.uzivatel.typ == "admin" || Main.Menu.uzivatel.typ == "sklad")
                    {
                        my_Button3.Show();
                        contextMenuStrip1.Items[1].Visible = true;
                    }
                    ds = Employer_Stock.List(meno_zamestnanec, priezvisko_zamestnanec, stock_ID, dateTimePicker1.Value, 200, page);
                }
                else if (isSold)
                {
                    ds = Employer_Stock.List(null, null, stock_ID, dateTimePicker1.Value, 200, page);
                    newHeadline = "Predaj / " + sklad_nazov;
                    my_Button3.Hide();
                    my_Button4.Text = "Predať";
                    my_Button8.Text = "Pridať";
                }
                else
                {
                    ds = Employer_Stock.List(null, null, stock_ID, dateTimePicker1.Value, 200, page);
                    newHeadline = "SKLAD / " + sklad_nazov;
                    my_Button3.Hide();
                    contextMenuStrip1.Items[1].Visible = false;
                    my_Button8.Text = "Pridať";
                    my_Button4.Text = "Zničiť";

                }

                if (headline1.Text != newHeadline)
                {
                    change_everithing = true;
                    headline1.Text = newHeadline;
                }


                if (ds == null || ds.Tables.Count == 0)
                {
                    BS.DataSource = null;
                    tabulka.DataSource = null;
                }
                else
                {

                    BS.Filter = "";
                    BS.DataSource = ds.Tables[0];
                    tabulka.DataSource = BS;
                    DataGridViewCheckBoxColumn dchck = new DataGridViewCheckBoxColumn();
                    dchck.HeaderText = "Photo";
                    dchck.Name = "PhotoCHCK";
                    tabulka.Columns.Add(dchck);

                    //DataGridViewComboBoxColumn dcombo = new DataGridViewComboBoxColumn();
                    //dcombo.HeaderText = "Poznamka";
                    //dcombo.Name = "Poznamka";

                    //tabulka.Columns.Add(dcombo);

                    tabulka.Columns[tabulka.Columns.Count - pravy_pridavok].Visible = false;
                    tabulka.Columns[tabulka.Columns.Count - pravy_pridavok - 1].Visible = false; //Itmeno_id schovat
                    tabulka.Columns[tabulka.Columns.Count - pravy_pridavok - 3].Visible = false; //Stock_id schovat


                    System.Windows.Forms.DataGridViewCellStyle dcs = new System.Windows.Forms.DataGridViewCellStyle();
                    dcs.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
                    System.Windows.Forms.DataGridViewCellStyle dcs2 = new System.Windows.Forms.DataGridViewCellStyle();
                    dcs2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
                    dcs2.NullValue = "-";
                    if (tabulka.Columns["Cena"] != null)
                    {
                        tabulka.Columns["Cena"].DefaultCellStyle = dcs2;

                    }

                    dcs.NullValue = "0";
                    dcs.Format = "N0";

                    if (tabulka.Columns["Znicene"] != null)
                    {
                        tabulka.Columns["Znicene"].DefaultCellStyle = dcs;
                    }
                    if (tabulka.Columns["Pocet"] != null)
                    {
                        tabulka.Columns["Pocet"].DefaultCellStyle = dcs;
                        Debug.WriteLine("Pocet");
                    }
                    if (tabulka.Columns["Na predaj"] != null)
                    {
                        tabulka.Columns["Na predaj"].DefaultCellStyle = dcs;
                        Debug.WriteLine("Na predaj");
                    }
                    if (tabulka.Columns["Predane"] != null)
                    {
                        tabulka.Columns["Predane"].DefaultCellStyle = dcs;
                    }



                    if (change_everithing)
                    {
                        int i = 0;
                        for (i = splitContainer4.Panel2.Controls.Count - 1; i > 0; i--)
                        {
                            //System.Console.WriteLine(c.GetType());
                            if (splitContainer4.Panel2.Controls[i].GetType() == typeof(Textac) || splitContainer4.Panel2.Controls[i].GetType() == typeof(Napis))
                            {
                                splitContainer4.Panel2.Controls[i].Dispose();
                            }
                        }
                        textace.Clear();

                        for (i = 0; i < tabulka.ColumnCount - stlpcovy_pridavok; i++)
                        {


                            tabulka.Columns[i].Width = 150;
                            textace.Add(new Textac());
                            splitContainer4.Panel2.Controls.Add(textace[i]);
                            textace[i].Location = new Point(35 + i * 150, 23);
                            textace[i].Size = new Size(136, 23);
                            textace[i].TextChanged += Stock_TextChanged;
                            textace[i].MouseDoubleClick += Stock_MouseClick;

                            textace[i].Tag = i;
                            Napis p = new Napis();
                            if (i == tabulka.ColumnCount - stlpcovy_pridavok - 1)
                            {
                                p.Text = "Menej ako:";
                                tabulka.Columns[i].Width = 80;
                                textace[i].Size = new Size(70, 23);
                            }
                            else
                            {
                                p.Text = tabulka.Columns[i].HeaderText;
                            }
                            splitContainer4.Panel2.Controls.Add(p);
                            p.Location = new Point(i * 150 + 35, 0);
                            //Error.Show("");
                        }
                    }
                    else filter();

                }
                page = 0;
            }
            catch (System.Exception ex)
            {
                Error.Show(ex);
            }

        }

        void Stock_MouseClick(object sender, MouseEventArgs e)
        {
            ((Textac)sender).Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.BackgroundImage = RotekIS.Properties.Resources.Echo_Grey_Gradient_Home_Screen;
            button1.Tag = "On";
            stock_change();
        }

        private void DR1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                button1.BackgroundImage = RotekIS.Properties.Resources.Echo_Grey_Gradient_Home_Screen_Off;
                button1.Tag = "Off";
                stock_change();
            }
            else
            {
                contextMenuStrip3.Show(Cursor.Position.X, Cursor.Position.Y);
                contextMenuStrip3.Tag = int.Parse(employers.Tables[0].Rows[dataRepeater1.CurrentItemIndex][0].ToString());
            }
        }

        private void tabulka_DataSourceChanged(object sender, EventArgs e)
        {


        }

        void Stock_TextChanged(object sender, EventArgs e)
        {
            int id = (int)((Textac)sender).Tag;
            filter();
        }

        public void filter()
        {
            int i = 0;
            string filter = "";
            Dictionary<String, String> searches = Stock_SQL.getSearch(current_stock());

            for (i = 0; i < textace.Count - 1; i++)
            {
                String actual_column = tabulka.Columns[i].HeaderText;
                String actual_word = textace[i].Text;

                if (searches.ContainsKey(actual_column))
                {
                    if (searches[actual_column] == "Presne")
                    {
                        filter += "[" + actual_column + "] = '" + actual_word + "' AND ";
                    }
                    else if (searches[actual_column] == "Z ľava")
                    {
                        filter += "[" + actual_column + "] LIKE '" + actual_word + "%' AND ";
                    }
                    else
                    {
                        string[] values = actual_word.Split(' ');
                        foreach (string s in values)
                        {
                            filter += "[" + actual_column + "] LIKE '%" + s + "%' AND ";
                        }
                    }

                }
                //if (i != textace.Count - 2) filter += " AND ";
            }

            if (filter.LastIndexOf("AND") > -1) filter = filter.Substring(0, filter.LastIndexOf("AND"));
            if (textace[textace.Count - 1].Text != "")
            {
                try
                {
                    int pocet = int.Parse(textace[textace.Count - 1].Text);
                    filter += " AND [Pocet] <= " + pocet;
                }
                catch (System.Exception ex)
                {

                }
            }
            Debug.WriteLine(filter);
            BS.Filter = filter;
        }

        private void tabulka_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    tabulka.CurrentCell = tabulka.Rows[e.RowIndex].Cells[e.ColumnIndex];
                }
                contextMenuStrip1.Show(MousePosition.X, MousePosition.Y);
                contextMenuStrip1.Tag = e.RowIndex + ";" + e.ColumnIndex;
                contextMenuStrip1.Visible = true;
            }
            else
            {

            }
        }

        private void pridaťToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Add_Item_Dialog(1, int.Parse(contextMenuStrip1.Tag.ToString().Substring(0, contextMenuStrip1.Tag.ToString().IndexOf(";"))));
        }

        private void vrátiťToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Add_Item_Dialog(2, int.Parse(contextMenuStrip1.Tag.ToString().Substring(0, contextMenuStrip1.Tag.ToString().IndexOf(";"))));

        }

        private void zničiťToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Add_Item_Dialog(3, int.Parse(contextMenuStrip1.Tag.ToString().Substring(0, contextMenuStrip1.Tag.ToString().IndexOf(";"))));

        }

        private void informácieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string[] values = new string[tabulka.Columns.Count - stlpcovy_pridavok];
            int i = 0;
            for (i = 0; i < tabulka.Columns.Count - stlpcovy_pridavok; i++)
            {
                values[i] = tabulka[i, int.Parse(contextMenuStrip1.Tag.ToString().Substring(0, contextMenuStrip1.Tag.ToString().IndexOf(";")))].Value.ToString();
            }

            Item_Info ii = new Item_Info(values, int.Parse(stocks.Tables[0].Rows[dataRepeater2.CurrentItemIndex][0].ToString()));
            ii.ShowDialog();


        }

        private void tabulka_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (tabulka.Columns.Count > 2)
            {
                if (tabulka.Columns["PhotoCHCK"] != null && e.ColumnIndex == tabulka.Columns["PhotoCHCK"].Index && tabulka.Columns.Contains("Photo") && tabulka["Photo", e.RowIndex].Value.ToString() != "")
                {
                    tabulka["PhotoCHCK", e.RowIndex].Value = true;
                }
            }
        }

        private void tabulka_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }
            if (tabulka.Columns["PhotoCHCK"] != null && e.ColumnIndex == tabulka.Columns["PhotoCHCK"].Index && tabulka["PhotoCHCK", e.RowIndex].Value != null)
            {
                Fotka f = new Fotka(tabulka["Photo", e.RowIndex].Value.ToString());

                f.ShowDialog();
            }

            if (tabulka.Columns["Poznámky"] != null && e.ColumnIndex == tabulka.Columns["Poznámky"].Index)
            {
                int itemno = (int)tabulka["ItemNo_ID", e.RowIndex].Value;

                GroupBox1.Location = new System.Drawing.Point(Cursor.Position.X - 227 - GroupBox1.Size.Width / 2, 110 + tabulka.Rows[0].Height * (e.RowIndex + 1));
                GroupBox1.Show();
                GroupBox1.BringToFront();
                GroupBox1.Tag = itemno;
                note_show(itemno);

            }
            else
            {
                zmizni();
            }


        }

        private void note_show(int itemNo)
        {
            //listBox1.Items;
            DataTable ds = Employer_Stock.get_Notes(itemNo).Tables[0];
            //for (int i = 0; i < ds.Rows.Count; i++)
            {
                listBox1.DataSource = ds;
                listBox1.DisplayMember = "Note";
                listBox1.ValueMember = "ID";
            }
        }

        private void zmizni()
        {
            if ((GroupBox1.Visible) && ((Cursor.Position.X < GroupBox1.Location.X) || (Cursor.Position.X > (GroupBox1.Location.X + GroupBox1.Size.Width)) || (Cursor.Position.Y < GroupBox1.Location.Y) || (Cursor.Position.Y > (GroupBox1.Location.Y + GroupBox1.Size.Height))))
            {
                GroupBox1.Hide();
            }
        }


        private void contextMenuStrip1_VisibleChanged(object sender, EventArgs e)
        {
        }

        private void splitContainer4_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void zmazaťToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int row = int.Parse(contextMenuStrip1.Tag.ToString().Substring(0, contextMenuStrip1.Tag.ToString().IndexOf(";")));
            if (Error.Question("Naozaj to chcete zmazať?"))
            {
                int itemno = (int)tabulka["ItemNo_ID", row].Value;
                Stock_SQL.Delete(itemno);
                stock_change();
            }
        }

        private void presunúťToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Add_Item_Dialog(4, int.Parse(contextMenuStrip1.Tag.ToString().Substring(0, contextMenuStrip1.Tag.ToString().IndexOf(";"))));

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            Nacitaj_zamestnancov();
        }

        private void DR_stock_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip2.Show(Cursor.Position.X, Cursor.Position.Y);
                contextMenuStrip2.Tag = int.Parse(stocks.Tables[0].Rows[dataRepeater2.CurrentItemIndex][0].ToString());
            }
        }

        private void vytvoriťPredajnýSkladToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int stock_id = int.Parse(contextMenuStrip2.Tag.ToString());
            Stock_SQL.AddSold(stock_id);
            Nacitaj_sklady();

        }

        private void daťNaPredajToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Add_Item_Dialog(5, int.Parse(contextMenuStrip1.Tag.ToString().Substring(0, contextMenuStrip1.Tag.ToString().IndexOf(";"))));
        }

        private void zmazaťSkladToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int stock_id = int.Parse(contextMenuStrip2.Tag.ToString());
            if (Error.Question("Naozaj chcete vymazať sklad " + Stock_SQL.stock_name(stock_id) + " ?"))
            {
                Stock_SQL.RemoveStock(stock_id);
                Nacitaj_sklady();
            }
        }

        private void premenovaťToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int row = int.Parse(contextMenuStrip1.Tag.ToString().Substring(0, contextMenuStrip1.Tag.ToString().IndexOf(";")));
            int column = int.Parse(contextMenuStrip1.Tag.ToString().Substring(contextMenuStrip1.Tag.ToString().IndexOf(";") + 1));
            String columnName = tabulka.Columns[column].HeaderText;
            String newName = Error.ShowDialog("Zadajte nový názov pre " + columnName, "Premenovať " + columnName);
            if (newName != null && newName.Length > 0)
            {
                int itemno = (int)tabulka["ItemNo_ID", row].Value;
                int stock_id = current_stock_ID();
                Stock_SQL.RenameItem(itemno, newName, columnName, stock_id);
                stock_change();

            }
        }

        private void premenovaťSkladToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int stock_id = int.Parse(contextMenuStrip2.Tag.ToString());
            if (Stock_SQL.isSold(stock_id))
            {
                Error.Show("Nedá sa premenovať sklad pre predaj. Premenujte pôvodný sklad.");
                return;
            }

            String newName = Error.ShowDialog("Premenovať sklad na:", "Premenovať sklad");
            if (newName.Length > 0)
            {
                if (Stock_SQL.stock_ID(newName) == -1)
                {
                    int stock_soldID = Stock_SQL.stock_sold_id(stock_id);
                    if (stock_soldID != -1)
                    {
                        Stock_SQL.rename_stock(stock_soldID, newName + " - predaj");
                    }

                    Stock_SQL.rename_stock(stock_id, newName);
                    Nacitaj_sklady();
                    stock_change();
                }
                else
                {
                    Error.Show("Sklad s takým názvom už existuje");
                }

            }

        }

        private void GroupBox1_VisibleChanged(object sender, EventArgs e)
        {
            if (GroupBox1.Location.Y + 270 + GroupBox1.Size.Height > this.Height)
            {
                GroupBox1.Location = new System.Drawing.Point(GroupBox1.Location.X, MousePosition.Y - 300 - GroupBox1.Size.Height);
                if (GroupBox1.Location.Y + GroupBox1.Size.Height > this.Height | GroupBox1.Location.Y < this.Location.Y)
                {
                    GroupBox1.Location = new System.Drawing.Point(GroupBox1.Location.X, this.Height - 50 - GroupBox1.Size.Height);
                }
            }

            int diffwidth = GroupBox1.Location.X + 227 + GroupBox1.Size.Width - this.Width;
            if (diffwidth > 0)
            {
                GroupBox1.Location = new System.Drawing.Point(GroupBox1.Location.X - diffwidth, GroupBox1.Location.Y);
                if (GroupBox1.Location.X + GroupBox1.Size.Width > this.Width | GroupBox1.Location.X < this.Location.X)
                {
                    GroupBox1.Location = new System.Drawing.Point(this.Width - GroupBox1.Size.Width, GroupBox1.Location.Y);
                }
            }
        }

        private void Stock_Click(object sender, EventArgs e)
        {
            zmizni();
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            string text = Error.ShowDialog("Text poznámky", "Pridať poznámku");
            int itemNo = int.Parse(GroupBox1.Tag.ToString());
            Employer_Stock.add_Note(itemNo, text);

            note_show(itemNo);
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            int itemNo = int.Parse(GroupBox1.Tag.ToString());
            int ID = int.Parse(listBox1.SelectedValue.ToString());
            Employer_Stock.remove_Note(ID);
            note_show(itemNo);

        }

        private void daťNaInýSkladToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void daťNaInýSkladToolStripMenuItem_MouseHover(object sender, EventArgs e)
        {
            daťNaInýSkladToolStripMenuItem.DropDownItems.Clear();

            for (int i = 0; i < stocks.Tables[0].Rows.Count; i++)
            {
                string stock_name = stocks.Tables[0].Rows[i]["Name"].ToString();
                int stock_id = int.Parse(stocks.Tables[0].Rows[i]["ID"].ToString());


                ToolStripMenuItem stockNameToolMenuItem = new System.Windows.Forms.ToolStripMenuItem();
                stockNameToolMenuItem.Name = stock_name;
                stockNameToolMenuItem.Text = stock_name;
                stockNameToolMenuItem.Tag = stock_id;
                stockNameToolMenuItem.Click += stockNameToolMenuItem_Click;
                this.daťNaInýSkladToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { stockNameToolMenuItem });
            }



        }

        void stockNameToolMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem stockNameToolMenuItem = (ToolStripMenuItem)sender;
            Move_Item_Dialog(int.Parse(contextMenuStrip1.Tag.ToString().Substring(0, contextMenuStrip1.Tag.ToString().IndexOf(";"))), int.Parse(stockNameToolMenuItem.Tag.ToString()), stockNameToolMenuItem.Name);
        }


        private void dateTimePicker1_CloseUp(object sender, EventArgs e)
        {
            stock_change();

        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExportDatesDialog datesdialog = new ExportDatesDialog("Výber časového obdobia");
            if (datesdialog.ShowDialog() == DialogResult.OK)
            {
                progressBar1.Visible = true;
                Stock_SQL.export_monthly(-1, datesdialog.date_from, datesdialog.date_to, progressBar1);
                progressBar1.Visible = false;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            ExportDatesDialog datesdialog = new ExportDatesDialog("Výber časového obdobia");
            if (datesdialog.ShowDialog() == DialogResult.OK)
            {
                progressBar1.Visible = true;
                
                Stock_SQL.export_monthly(-1, datesdialog.date_from, datesdialog.date_to.AddDays(1) , progressBar1);
                progressBar1.Visible = false;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            progressBar1.Visible = true;
            Stock_SQL.reset_wrecked(progressBar1);
            progressBar1.Visible = false;
        }

        private void exportVydanehoNaradiaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExportDatesDialog datesdialog = new ExportDatesDialog("Výber časového obdobia");
            if (datesdialog.ShowDialog() == DialogResult.OK)
            {
                progressBar1.Visible = true;
                Stock_SQL.export_monthly((int)contextMenuStrip3.Tag, datesdialog.date_from, datesdialog.date_to, progressBar1);
                progressBar1.Visible = false;
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            TextDialog txtdialog = new TextDialog("Najdi", "Nájsť vo všetkých skladoch. Zadať len 1 názov:");
            if (txtdialog.ShowDialog() == DialogResult.OK)
            {
                stock_change(txtdialog.text);

            }
        }

    }
}
