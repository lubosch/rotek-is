﻿namespace RotekIS.Stocks
{
    partial class Add_Item
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Add_Item));
            this.textac4 = new RotekIS.Forms.Textac();
            this.napis4 = new RotekIS.Forms.Napis();
            this.textac3 = new RotekIS.Forms.Textac();
            this.button2 = new System.Windows.Forms.Button();
            this.napis3 = new RotekIS.Forms.Napis();
            this.textac2 = new RotekIS.Forms.Textac();
            this.napis2 = new RotekIS.Forms.Napis();
            this.textac1 = new RotekIS.Forms.Textac();
            this.napis1 = new RotekIS.Forms.Napis();
            this.my_Button3 = new RotekIS.my_Button();
            this.my_Button1 = new RotekIS.my_Button();
            this.textac5 = new RotekIS.Forms.Textac();
            this.napis5 = new RotekIS.Forms.Napis();
            this.napis6 = new RotekIS.Forms.Napis();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.rotekDataSet = new RotekIS.RotekDataSet();
            this.stockBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.stockTableAdapter = new RotekIS.RotekDataSetTableAdapters.StockTableAdapter();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rotekDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stockBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.checkBox1);
            this.panel1.Controls.Add(this.napis6);
            this.panel1.Controls.Add(this.napis5);
            this.panel1.Controls.Add(this.textac5);
            this.panel1.Controls.Add(this.napis1);
            this.panel1.Controls.Add(this.textac1);
            this.panel1.Controls.Add(this.textac4);
            this.panel1.Controls.Add(this.napis4);
            this.panel1.Controls.Add(this.textac3);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.napis3);
            this.panel1.Controls.Add(this.textac2);
            this.panel1.Controls.Add(this.napis2);
            this.panel1.Controls.Add(this.my_Button3);
            this.panel1.Controls.Add(this.my_Button1);
            this.panel1.Size = new System.Drawing.Size(390, 428);
            // 
            // textac4
            // 
            this.textac4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textac4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.textac4.Location = new System.Drawing.Point(207, 289);
            this.textac4.Multiline = true;
            this.textac4.Name = "textac4";
            this.textac4.Size = new System.Drawing.Size(163, 66);
            this.textac4.TabIndex = 85;
            this.textac4.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textac_KeyUp);
            // 
            // napis4
            // 
            this.napis4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.napis4.AutoSize = true;
            this.napis4.Font = new System.Drawing.Font("Verdana", 10F);
            this.napis4.Location = new System.Drawing.Point(204, 269);
            this.napis4.Name = "napis4";
            this.napis4.Size = new System.Drawing.Size(79, 17);
            this.napis4.TabIndex = 87;
            this.napis4.Text = "Poznámka";
            // 
            // textac3
            // 
            this.textac3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textac3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.textac3.Location = new System.Drawing.Point(210, 192);
            this.textac3.Name = "textac3";
            this.textac3.Size = new System.Drawing.Size(163, 23);
            this.textac3.TabIndex = 83;
            this.textac3.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textac_KeyUp);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button2.Location = new System.Drawing.Point(229, 131);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(127, 30);
            this.button2.TabIndex = 81;
            this.button2.Text = "Vložiť obrázok";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // napis3
            // 
            this.napis3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.napis3.AutoSize = true;
            this.napis3.Font = new System.Drawing.Font("Verdana", 10F);
            this.napis3.Location = new System.Drawing.Point(207, 171);
            this.napis3.Name = "napis3";
            this.napis3.Size = new System.Drawing.Size(57, 17);
            this.napis3.TabIndex = 88;
            this.napis3.Text = "Cena €";
            // 
            // textac2
            // 
            this.textac2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textac2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.textac2.Location = new System.Drawing.Point(210, 102);
            this.textac2.Name = "textac2";
            this.textac2.Size = new System.Drawing.Size(163, 23);
            this.textac2.TabIndex = 82;
            // 
            // napis2
            // 
            this.napis2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.napis2.AutoSize = true;
            this.napis2.Font = new System.Drawing.Font("Verdana", 10F);
            this.napis2.Location = new System.Drawing.Point(207, 82);
            this.napis2.Name = "napis2";
            this.napis2.Size = new System.Drawing.Size(47, 17);
            this.napis2.TabIndex = 89;
            this.napis2.Text = "Fotka";
            // 
            // textac1
            // 
            this.textac1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textac1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.textac1.Location = new System.Drawing.Point(210, 43);
            this.textac1.Name = "textac1";
            this.textac1.Size = new System.Drawing.Size(163, 23);
            this.textac1.TabIndex = 80;
            this.textac1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textac_KeyUp);
            // 
            // napis1
            // 
            this.napis1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.napis1.AutoSize = true;
            this.napis1.Font = new System.Drawing.Font("Verdana", 10F);
            this.napis1.Location = new System.Drawing.Point(207, 23);
            this.napis1.Name = "napis1";
            this.napis1.Size = new System.Drawing.Size(46, 17);
            this.napis1.TabIndex = 90;
            this.napis1.Text = "Počet";
            // 
            // my_Button3
            // 
            this.my_Button3.BackColor = System.Drawing.Color.Transparent;
            this.my_Button3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("my_Button3.BackgroundImage")));
            this.my_Button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.my_Button3.FlatAppearance.BorderSize = 0;
            this.my_Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.my_Button3.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.my_Button3.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.my_Button3.Location = new System.Drawing.Point(9, 361);
            this.my_Button3.Name = "my_Button3";
            this.my_Button3.Size = new System.Drawing.Size(140, 41);
            this.my_Button3.TabIndex = 87;
            this.my_Button3.Text = "Zrušiť";
            this.my_Button3.UseVisualStyleBackColor = false;
            this.my_Button3.Click += new System.EventHandler(this.my_Button3_Click);
            // 
            // my_Button1
            // 
            this.my_Button1.BackColor = System.Drawing.Color.Transparent;
            this.my_Button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("my_Button1.BackgroundImage")));
            this.my_Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.my_Button1.FlatAppearance.BorderSize = 0;
            this.my_Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.my_Button1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.my_Button1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.my_Button1.Location = new System.Drawing.Point(216, 361);
            this.my_Button1.Name = "my_Button1";
            this.my_Button1.Size = new System.Drawing.Size(140, 41);
            this.my_Button1.TabIndex = 86;
            this.my_Button1.Text = "Pridať";
            this.my_Button1.UseVisualStyleBackColor = false;
            this.my_Button1.Click += new System.EventHandler(this.my_Button1_Click);
            // 
            // textac5
            // 
            this.textac5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textac5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.textac5.Location = new System.Drawing.Point(210, 243);
            this.textac5.Name = "textac5";
            this.textac5.Size = new System.Drawing.Size(163, 23);
            this.textac5.TabIndex = 84;
            this.textac5.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textac_KeyUp);
            // 
            // napis5
            // 
            this.napis5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.napis5.AutoSize = true;
            this.napis5.Font = new System.Drawing.Font("Verdana", 10F);
            this.napis5.Location = new System.Drawing.Point(207, 223);
            this.napis5.Name = "napis5";
            this.napis5.Size = new System.Drawing.Size(46, 17);
            this.napis5.TabIndex = 92;
            this.napis5.Text = "Regal";
            // 
            // napis6
            // 
            this.napis6.Dock = System.Windows.Forms.DockStyle.Top;
            this.napis6.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold);
            this.napis6.Location = new System.Drawing.Point(0, 0);
            this.napis6.Name = "napis6";
            this.napis6.Size = new System.Drawing.Size(388, 17);
            this.napis6.TabIndex = 93;
            this.napis6.Text = "-";
            this.napis6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif;" +
    " *.png";
            // 
            // rotekDataSet
            // 
            this.rotekDataSet.DataSetName = "RotekDataSet";
            this.rotekDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // stockBindingSource
            // 
            this.stockBindingSource.DataMember = "Stock";
            this.stockBindingSource.DataSource = this.rotekDataSet;
            // 
            // stockTableAdapter
            // 
            this.stockTableAdapter.ClearBeforeFill = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.checkBox1.Location = new System.Drawing.Point(218, 402);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(138, 21);
            this.checkBox1.TabIndex = 94;
            this.checkBox1.Text = "Po pridaní zavrieť";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // Add_Item
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(390, 453);
            this.Name = "Add_Item";
            this.Load += new System.EventHandler(this.Add_Item_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rotekDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stockBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Forms.Textac textac4;
        private Forms.Napis napis4;
        private Forms.Textac textac3;
        private System.Windows.Forms.Button button2;
        private Forms.Napis napis3;
        private Forms.Textac textac2;
        private Forms.Napis napis2;
        private Forms.Textac textac1;
        private Forms.Napis napis1;
        private my_Button my_Button3;
        private my_Button my_Button1;
        private Forms.Napis napis5;
        private Forms.Textac textac5;
        private Forms.Napis napis6;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private RotekDataSet rotekDataSet;
        private System.Windows.Forms.BindingSource stockBindingSource;
        private RotekDataSetTableAdapters.StockTableAdapter stockTableAdapter;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}
