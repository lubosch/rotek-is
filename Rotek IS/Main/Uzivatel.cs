﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RotekIS.Main
{

    public class Uzivatel
    {
        public string nick { set; get; }
        public string typ { set; get; }
        public string mail { set; get; }
        public string mail_heslo { set; get; }
        public string real_name { set; get; }
        public Uzivatel()
        {
            nick = "neznámy užívateľ";
            typ = "nikto";
            mail = null;
            mail_heslo = null;
            real_name = "Votrelec";
        }
    }
}
