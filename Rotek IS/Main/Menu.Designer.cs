﻿namespace RotekIS.Main
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.my_Button1 = new RotekIS.my_Button();
            this.my_Button3 = new RotekIS.my_Button();
            this.my_Button2 = new RotekIS.my_Button();
            this.my_Button5 = new RotekIS.my_Button();
            this.napis1 = new RotekIS.Forms.Napis();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.my_Button4 = new RotekIS.my_Button();
            this.headline1 = new RotekIS.Forms.Headline();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.ErrorImage = global::RotekIS.Properties.Resources.x;
            this.pictureBox1.Image = global::RotekIS.Properties.Resources.x;
            this.pictureBox1.Location = new System.Drawing.Point(551, 1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(48, 43);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = global::RotekIS.Properties.Resources.logo;
            this.pictureBox2.Location = new System.Drawing.Point(171, 1);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(277, 94);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 10;
            this.pictureBox2.TabStop = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackgroundImage = global::RotekIS.Properties.Resources.bg;
            this.splitContainer1.Panel1.Controls.Add(this.pictureBox2);
            this.splitContainer1.Panel1.Controls.Add(this.my_Button1);
            this.splitContainer1.Panel1.Controls.Add(this.my_Button3);
            this.splitContainer1.Panel1.Controls.Add(this.pictureBox1);
            this.splitContainer1.Panel1.Controls.Add(this.my_Button2);
            this.splitContainer1.Panel1.Controls.Add(this.my_Button5);
            this.splitContainer1.Panel1.Controls.Add(this.napis1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Panel2.BackgroundImage = global::RotekIS.Properties.Resources.bg;
            this.splitContainer1.Panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.splitContainer1.Panel2.Controls.Add(this.richTextBox1);
            this.splitContainer1.Panel2.Controls.Add(this.my_Button4);
            this.splitContainer1.Panel2.Controls.Add(this.headline1);
            this.splitContainer1.Size = new System.Drawing.Size(814, 345);
            this.splitContainer1.SplitterDistance = 598;
            this.splitContainer1.TabIndex = 11;
            // 
            // my_Button1
            // 
            this.my_Button1.BackColor = System.Drawing.Color.Transparent;
            this.my_Button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("my_Button1.BackgroundImage")));
            this.my_Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.my_Button1.FlatAppearance.BorderSize = 0;
            this.my_Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.my_Button1.Font = new System.Drawing.Font("Verdana", 15F, System.Drawing.FontStyle.Bold);
            this.my_Button1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.my_Button1.Location = new System.Drawing.Point(252, 232);
            this.my_Button1.Name = "my_Button1";
            this.my_Button1.Size = new System.Drawing.Size(140, 41);
            this.my_Button1.TabIndex = 5;
            this.my_Button1.Text = "SKLAD";
            this.my_Button1.UseVisualStyleBackColor = false;
            this.my_Button1.Click += new System.EventHandler(this.my_Button1_Click);
            // 
            // my_Button3
            // 
            this.my_Button3.BackColor = System.Drawing.Color.Transparent;
            this.my_Button3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("my_Button3.BackgroundImage")));
            this.my_Button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.my_Button3.FlatAppearance.BorderSize = 0;
            this.my_Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.my_Button3.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.my_Button3.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.my_Button3.Location = new System.Drawing.Point(322, 286);
            this.my_Button3.Name = "my_Button3";
            this.my_Button3.Size = new System.Drawing.Size(136, 40);
            this.my_Button3.TabIndex = 8;
            this.my_Button3.Text = "Vytvoriť účet";
            this.my_Button3.UseVisualStyleBackColor = false;
            this.my_Button3.Click += new System.EventHandler(this.my_Button3_Click);
            // 
            // my_Button2
            // 
            this.my_Button2.BackColor = System.Drawing.Color.Transparent;
            this.my_Button2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("my_Button2.BackgroundImage")));
            this.my_Button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.my_Button2.FlatAppearance.BorderSize = 0;
            this.my_Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.my_Button2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.my_Button2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.my_Button2.Location = new System.Drawing.Point(133, 288);
            this.my_Button2.Name = "my_Button2";
            this.my_Button2.Size = new System.Drawing.Size(136, 38);
            this.my_Button2.TabIndex = 7;
            this.my_Button2.Text = "Zmeniť účet";
            this.my_Button2.UseVisualStyleBackColor = false;
            this.my_Button2.Click += new System.EventHandler(this.my_Button2_Click_1);
            // 
            // my_Button5
            // 
            this.my_Button5.BackColor = System.Drawing.Color.Transparent;
            this.my_Button5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("my_Button5.BackgroundImage")));
            this.my_Button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.my_Button5.FlatAppearance.BorderSize = 0;
            this.my_Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.my_Button5.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.my_Button5.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.my_Button5.Location = new System.Drawing.Point(71, 232);
            this.my_Button5.Name = "my_Button5";
            this.my_Button5.Size = new System.Drawing.Size(129, 35);
            this.my_Button5.TabIndex = 6;
            this.my_Button5.Text = "Zadať heslo";
            this.my_Button5.UseVisualStyleBackColor = false;
            this.my_Button5.Click += new System.EventHandler(this.my_Button5_Click);
            // 
            // napis1
            // 
            this.napis1.BackColor = System.Drawing.Color.Transparent;
            this.napis1.Dock = System.Windows.Forms.DockStyle.Top;
            this.napis1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.napis1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.napis1.Location = new System.Drawing.Point(0, 0);
            this.napis1.Name = "napis1";
            this.napis1.Size = new System.Drawing.Size(598, 134);
            this.napis1.TabIndex = 9;
            this.napis1.Text = "napis1";
            this.napis1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.richTextBox1.Location = new System.Drawing.Point(2, 51);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(207, 244);
            this.richTextBox1.TabIndex = 3;
            this.richTextBox1.Text = resources.GetString("richTextBox1.Text");
            // 
            // my_Button4
            // 
            this.my_Button4.BackColor = System.Drawing.Color.Transparent;
            this.my_Button4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("my_Button4.BackgroundImage")));
            this.my_Button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.my_Button4.FlatAppearance.BorderSize = 0;
            this.my_Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.my_Button4.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.my_Button4.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.my_Button4.Location = new System.Drawing.Point(18, 301);
            this.my_Button4.Name = "my_Button4";
            this.my_Button4.Size = new System.Drawing.Size(176, 41);
            this.my_Button4.TabIndex = 2;
            this.my_Button4.Text = "Vyžiadat novinku";
            this.my_Button4.UseVisualStyleBackColor = false;
            this.my_Button4.Click += new System.EventHandler(this.my_Button4_Click);
            // 
            // headline1
            // 
            this.headline1.AutoSize = true;
            this.headline1.BackColor = System.Drawing.Color.Transparent;
            this.headline1.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold);
            this.headline1.Location = new System.Drawing.Point(16, 0);
            this.headline1.Name = "headline1";
            this.headline1.Size = new System.Drawing.Size(184, 48);
            this.headline1.TabIndex = 0;
            this.headline1.Text = "Novinky";
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Blue;
            this.BackgroundImage = global::RotekIS.Properties.Resources.bg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(814, 345);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Menu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu";
            this.TransparencyKey = System.Drawing.Color.Blue;
            this.Load += new System.EventHandler(this.Menu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private my_Button my_Button1;
        private my_Button my_Button5;
        private my_Button my_Button2;
        private my_Button my_Button3;
        private Forms.Napis napis1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private Forms.Headline headline1;
        private my_Button my_Button4;
        private System.Windows.Forms.RichTextBox richTextBox1;



    }
}