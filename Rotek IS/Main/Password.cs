﻿using RotekIS.SQL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace RotekIS.Main
{
    public partial class Password : RotekIS.Stocks.Inter_Form
    {
        public Password()
        {
            InitializeComponent();
            nadpis_set("Heslo");
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                textac1.PasswordChar = '\0';
            }
            else
            {
                textac1.PasswordChar = '•';
            }
        }

        private void Password_Load(object sender, EventArgs e)
        {
            textac1.Select();
        }

        private void my_Button2_Click(object sender, EventArgs e)
        {
            Main.Menu.uzivatel = new Uzivatel();
            Dispose();
        }

        private void my_Button1_Click(object sender, EventArgs e)
        {
            commit();            
        }
        private void commit()
        {
            if (textac1.Text == "")
            {
                Main.Menu.uzivatel = new Uzivatel();
            }
            else
            {
                Main.Menu.uzivatel = Account_SQL.Type_User(textac1.Text);
            }

            Dispose();

        }

        private void textac1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) commit();
            if (e.KeyCode == Keys.Escape) Dispose();

        }
    }
}
