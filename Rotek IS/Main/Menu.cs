﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RotekIS.Stocks;
using System.Net;
using RotekIS.General;
using System.IO;
using Microsoft.VisualBasic;
using Microsoft.Win32;
using System.Diagnostics;
using RotekIS.Forms;
using RotekIS.SQL;

namespace RotekIS.Main
{
    public partial class Menu : Form
    {
        public static Uzivatel uzivatel;

        public Menu()
        {
            InitializeComponent();
        }

        private void Menu_Load(object sender, EventArgs e)
        {
            aktualizacie();
            uzivatel = new Uzivatel();
            pass();
        }

        private void my_Button2_Click(object sender, EventArgs e)
        {

        }

        private void my_Button1_Click(object sender, EventArgs e)
        {


            Stock f = new Stock();
            this.Hide();
            f.ShowDialog();
            this.Show();

        }

        private void my_Button3_Click(object sender, EventArgs e)
        {
            Account f = new Account(1);
            f.ShowDialog();
        }

        private void my_Button2_Click_1(object sender, EventArgs e)
        {
            Account f = new Account(2);
            f.ShowDialog();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        //switch (uzivatel.typ)
        //  {
        //      case "admin":
        //          break;
        //      case "zakaz":
        //          break;
        //      case "sklad":
        //          break;
        //      case else:
        //          break;
        //  }


        private void poverenie()
        {
            switch (uzivatel.typ)
            {
                case "admin":
                    my_Button3.Show();
                    break;
                case "zakaz":
                    my_Button3.Hide();
                    break;
                case "sklad":
                    my_Button3.Hide();
                    break;
                default:
                    my_Button3.Hide();
                    break;
            }
        }

        private void pass()
        {
            Password p = new Password();
            p.ShowDialog();
            napis1.Text = "Vítajte " + uzivatel.nick + " . Systém k Vaším službám";
            poverenie();
        }

        private void my_Button5_Click(object sender, EventArgs e)
        {
            pass();
        }

        private bool strom(ref string[] servers, ref List<string> zalohy)
        {
            try
            {
                //System.Net.FtpWebRequest ftpWebReq = (System.Net.FtpWebRequest)System.Net.WebRequest.Create("ftp://vnenk.orava.sk/Rotek IS");
                //ftpWebReq.Method = System.Net.WebRequestMethods.Ftp.ListDirectory;

                //ftpWebReq.Credentials = new System.Net.NetworkCredential("vnenk", "ptkw452d");

                //System.Net.FtpWebResponse ftpWebResp = (System.Net.FtpWebResponse)ftpWebReq.GetResponse();

                //System.IO.Stream streamer = ftpWebResp.GetResponseStream();

                //System.IO.StreamReader reader = new System.IO.StreamReader(streamer);

                ////Chyby.Show(reader.ReadToEnd(), "FTP")
                //int i = 0;
                //while ((reader.EndOfStream != true))
                //{
                //    servers[i] = (reader.ReadLine().ToString());
                //    i = i + 1;
                //}

                //ftpWebReq = (System.Net.FtpWebRequest)System.Net.WebRequest.Create("ftp://vnenk.orava.sk/Databaza");

                //ftpWebReq.Method = System.Net.WebRequestMethods.Ftp.ListDirectory;

                //ftpWebReq.Credentials = new System.Net.NetworkCredential("vnenk", "ptkw452d");

                //ftpWebResp = (System.Net.FtpWebResponse)ftpWebReq.GetResponse();

                //streamer = ftpWebResp.GetResponseStream();

                //reader = new System.IO.StreamReader(streamer);

                ////Chyby.Show(reader.ReadToEnd(), "FTP")
                //i = 0;
                //while ((reader.EndOfStream != true))
                //{
                //    zalohy.Add(reader.ReadLine().ToString());
                //    i = i + 1;
                //}
                if (Directory.Exists(@"\\192.168.1.150\Sklad\Updates IS\"))
                {
                    int i = 0;
                    foreach (String filePath in System.IO.Directory.EnumerateFiles(@"\\192.168.1.150\Sklad\Updates IS\"))
                    {
                        servers[i] = filePath.Substring(filePath.LastIndexOf("\\") + 1);
                    }
                }

            }
            catch (Exception ex)
            {
                Error.Show("Nedá sa pripojiť k internetu");
                return false;
            }
            return true;
        }

        private void Upload(string source, string target, NetworkCredential credential)
        {

            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(target);
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = credential;
                FileStream reader = new FileStream(source, FileMode.Open);
                byte[] buffer = new byte[Convert.ToInt32(reader.Length - 1) + 1];
                reader.Read(buffer, 0, buffer.Length);
                reader.Close();
                request.ContentLength = buffer.Length;
                Stream stream = request.GetRequestStream();
                stream.Write(buffer, 0, buffer.Length);
                stream.Close();
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                //Chyby.Show(response.StatusDescription, "File Uploaded")
                response.Close();

            }
            catch (Exception ex)
            {
                Error.Show(ex);
            }
        }


        private void aktualizacie()
        {
            try
            {
                //MessageBox.Show(Application.LocalUserAppDataPath & "\Update\")
                string[] servers = new string[1001];
                List<string> zalohy = new List<string>();
                if ((strom(ref servers, ref zalohy)))
                {
                }
                else
                {
                    return;
                }
                //Chyby.Show("a")
                DateTime[] daty = new DateTime[zalohy.Count + 1];
                int i = 0;
                DateTime d1 = default(DateTime);
                DateTime d2 = default(DateTime);
                d1 = new System.DateTime(2099, 12, 24);
                d2 = new System.DateTime(2000, 1, 1);
                //MessageBox.Show("a")
                ////////////////////////////////////////////////////////////////////////// Zaloha databazy bejvala
                /*try
                {
                    for (i = 0; i <= zalohy.Count - 1; i++)
                    {
                        try
                        {
                            daty[i] = DateTime.Parse(zalohy[i]);

                            if (daty[i].CompareTo(d2) > 0)
                            {
                                d2 = daty[i];
                            }
                            if (daty[i].CompareTo(d1) < 0)
                            {
                                d1 = daty[i];
                            }
                        }
                        catch (Exception ex)
                        {
                            daty[i] = new System.DateTime(1990, 1, 1);
                        }


                    }
                    //MessageBox.Show("a")
                    if (zalohy.Count > 20)
                    {
                        string cesta2 = "ftp://vnenk.orava.sk/Databaza/" + d1;
                        WebRequest FTPDelReq = WebRequest.Create(cesta2);
                        FTPDelReq.Credentials = new System.Net.NetworkCredential("vnenk", "ptkw452d");
                        FTPDelReq.Method = WebRequestMethods.Ftp.DeleteFile;
                        WebResponse FTPDelResp = FTPDelReq.GetResponse();
                    }
                    //MessageBox.Show("a")

                    d2 = d2.AddDays(3);

                    if (d2.CompareTo(DateAndTime.Now) < 1)
                    {
                        NetworkCredential credential = new NetworkCredential("vnenk", "ptkw452d");
                        string cesta = SQL.Save_disk.root + "Rotek.mdb";
                        string cesta2 = "ftp://vnenk.orava.sk/Databaza/" + System.DateTime.Now.ToShortDateString();
                        Upload(cesta, cesta2, credential);
                    }
                    //MessageBox.Show("a")


                }
                catch (Exception ex)
                {
                    //Chyby.Show("Problem so zalohovanim databazy. Tuto spravu kludne ignorujte. " & ex.ToString)
                }
                 * */


                string[] localFiles = new string[1001];
                localFiles[0] = Application.LocalUserAppDataPath + "\\Update\\";

                // me.Dispose
                //            localFiles(0) = Replace(localFiles(0), "Rotek sklad.EXE", "")
                //           localFiles(0) = localFiles(0) & "Update\"

                if (Directory.Exists(localFiles[0]))
                {
                }
                else
                {
                    Directory.CreateDirectory(localFiles[0]);
                }
                // make a reference to a directory
                System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(localFiles[0]);
                System.IO.FileInfo[] diar1 = di.GetFiles();
                //Error.Show(di.ToString());
                //list the names of all files in the specified directory
                i = 0;
                //Chyby.Show("c")

                foreach (System.IO.FileInfo dra in diar1)
                {
                    i = i + 1;
                    //If dra.ToString = "Rotek.mdb" Then
                    //    My.Computer.FileSystem.CopyFile(Application.StartupPath & "\Update\Rotek.mdb", Application.StartupPath & "\Rotek.mdb", True)
                    //    My.Computer.FileSystem.DeleteFile(Application.StartupPath & "\Update\Rotek.mdb")
                    //End If
                    //If dra.ToString = "Rotek sklad.exe.config" Then
                    //    My.Computer.FileSystem.CopyFile(Application.StartupPath & "\Update\Rotek sklad.exe.config", Application.StartupPath & "\Rotek sklad.exe.config", True)
                    //    My.Computer.FileSystem.DeleteFile(Application.StartupPath & "\Update\Rotek sklad.exe.config")
                    //End If

                    localFiles[i] = dra.ToString();
                }


                IEnumerable<string> DownloadingFiles = servers.Except(localFiles.AsEnumerable());

                foreach (string FileN in DownloadingFiles)
                {
                    //Chyby.Show("d")

                    // ObjShell.exec("Rotek sklad.exe")
                    //ObjShell = Nothing

                    Error.Show("Začína " + FileN + " Netreba nič robiť len po chvíli spustiť z plochy  ");
                    System.IO.File.Copy(@"\\192.168.1.150\Sklad\Updates IS\" + FileN, localFiles[0] + FileN);

                    //enew WebClient().DownloadFile("http://vnenk.orava.sk/rotek IS/" + FileN, localFiles[0] + FileN);


                    //Dim strcomputer As String
                    //strcomputer = "."
                    //Dim objwmiservice, colsoftware
                    //objwmiservice = GetObject("winmgmts:" _
                    //    & "{impersonationLevel=impersonate}!\\" & strcomputer & "\root\cimv2")

                    //colsoftware = objwmiservice.ExecQuery _
                    //    ("Select * from Win32_Product Where Name = 'Rotek sklad'")
                    //Try
                    //    If colsoftware.count < 1 Then
                    //        unins()

                    //    End If
                    //    For Each objSoftware In colsoftware
                    //        objSoftware.Uninstall()
                    //    Next

                    //Catch ex As Exception
                    //uninst();
                    //End Try
                    //Process.Start("c:\windows\system32\msiexec.exe", "/qn /i " & """" & Application.StartupPath & "\Update\" & FileN & """")

                    string cmd = "/a /s /v\"/qb TARGETDIR=\\\"" + localFiles[0] + "temp_install\\\"\"";
                    if (Directory.Exists(localFiles[0] + "temp_install")) Directory.Delete(localFiles[0] + "temp_install", true);
                    Directory.CreateDirectory(localFiles[0] + "temp_install");
                    Process proc = new Process();

                    proc.StartInfo.FileName = localFiles[0] + FileN;
                    proc.StartInfo.Arguments = cmd;
                    proc.Start();
                    proc.WaitForInputIdle();
                    proc.WaitForExit();
                    proc.Close();
                    //"{F4231357-9D7D-4B88-BA9D-A290D9892A2E}"

                    //proc = new Process();
                    //proc.StartInfo.FileName = "msiexec.exe";
                    //cmd="/x \"{F4231357-9D7D-4B88-BA9D-A290D9892A2E}\" /passive" ;

                    //proc.StartInfo.Arguments =  cmd;
                    //proc.Start();
                    //proc.WaitForExit();
                    //proc.Close();

                    //proc = new Process();
                    //proc.StartInfo.FileName = "msiexec.exe";
                    //cmd="/fa \"" + localFiles[0] + "temp_install\\RotekIS.msi\" /passive" ;
                    //proc.StartInfo.Arguments =  cmd;


                    System.IO.File.Delete(Path.GetTempPath() + "Uninstall.bat");
                    System.IO.File.WriteAllLines(Path.GetTempPath() + "Uninstall.bat", new string[] { "msiexec.exe /x \"{F4231357-9D7D-4B88-BA9D-A290D9892A2E}\" /passive \n", "msiexec.exe /i \"" + localFiles[0] + "temp_install\\RotekIS.msi\" /passive", "start \"\" \"" + Application.ExecutablePath + "\"" });

                    proc = new Process();
                    proc.StartInfo.FileName = Path.GetTempPath() + "Uninstall.bat";
                    proc.Start();
                    Environment.Exit(0);

                    //System.Diagnostics.Process.Start(Application.LocalUserAppDataPath + "\\Update\\" + FileN);
                }

            }
            catch (Exception ex)
            {
                Error.Show(ex);
            }
        }


        private void my_Button4_Click(object sender, EventArgs e)
        {
            TextDialog txtdialog = new TextDialog("Poslať žiadosť", "V tomto programe mi ešte chýba táto možnosť:");
            if (txtdialog.ShowDialog() == DialogResult.OK)
            {
                Mail mail = new Mail("Rotek sklad dorobit", Main.Menu.uzivatel.nick + "   \n" + txtdialog.text);
                mail.mne();
                mail.posli();
            }
        }

        private void my_Button6_Click(object sender, EventArgs e)
        {


            //Stock_SQL.reset_wrecked();

            //Properties.Settings.Default.LastAlert = DateTime.Now.ToShortDateString();
            //Properties.Settings.Default.Save();
            //my_Button6.Hide();
        }
    }
}
