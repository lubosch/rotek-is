﻿namespace RotekIS.Main
{
    partial class Account
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Account));
            this.napis1 = new RotekIS.Forms.Napis();
            this.name = new RotekIS.Forms.Textac();
            this.napis2 = new RotekIS.Forms.Napis();
            this.pass1 = new RotekIS.Forms.Textac();
            this.napis3 = new RotekIS.Forms.Napis();
            this.pass2 = new RotekIS.Forms.Textac();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.napis4 = new RotekIS.Forms.Napis();
            this.textac4 = new RotekIS.Forms.Textac();
            this.napis5 = new RotekIS.Forms.Napis();
            this.pass_mail = new RotekIS.Forms.Textac();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.my_Button3 = new RotekIS.my_Button();
            this.my_Button1 = new RotekIS.my_Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.napis6 = new RotekIS.Forms.Napis();
            this.textac1 = new RotekIS.Forms.Textac();
            this.napis7 = new RotekIS.Forms.Napis();
            this.textac2 = new RotekIS.Forms.Textac();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textac2);
            this.panel1.Controls.Add(this.napis7);
            this.panel1.Controls.Add(this.textac1);
            this.panel1.Controls.Add(this.napis6);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.my_Button1);
            this.panel1.Controls.Add(this.my_Button3);
            this.panel1.Controls.Add(this.checkBox2);
            this.panel1.Controls.Add(this.pass_mail);
            this.panel1.Controls.Add(this.napis5);
            this.panel1.Controls.Add(this.textac4);
            this.panel1.Controls.Add(this.napis4);
            this.panel1.Controls.Add(this.checkBox1);
            this.panel1.Controls.Add(this.pass2);
            this.panel1.Controls.Add(this.napis3);
            this.panel1.Controls.Add(this.pass1);
            this.panel1.Controls.Add(this.napis2);
            this.panel1.Controls.Add(this.name);
            this.panel1.Controls.Add(this.napis1);
            this.panel1.Size = new System.Drawing.Size(390, 478);
            // 
            // napis1
            // 
            this.napis1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.napis1.AutoSize = true;
            this.napis1.Font = new System.Drawing.Font("Verdana", 10F);
            this.napis1.Location = new System.Drawing.Point(10, 108);
            this.napis1.Name = "napis1";
            this.napis1.Size = new System.Drawing.Size(140, 17);
            this.napis1.TabIndex = 0;
            this.napis1.Text = "Prihlasovacie meno";
            // 
            // name
            // 
            this.name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.name.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.name.Location = new System.Drawing.Point(13, 128);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(163, 23);
            this.name.TabIndex = 2;
            this.name.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textac1_KeyUp);
            // 
            // napis2
            // 
            this.napis2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.napis2.AutoSize = true;
            this.napis2.Font = new System.Drawing.Font("Verdana", 10F);
            this.napis2.Location = new System.Drawing.Point(10, 162);
            this.napis2.Name = "napis2";
            this.napis2.Size = new System.Drawing.Size(138, 17);
            this.napis2.TabIndex = 2;
            this.napis2.Text = "Prihlasovacie heslo";
            // 
            // pass1
            // 
            this.pass1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pass1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.pass1.Location = new System.Drawing.Point(13, 182);
            this.pass1.Name = "pass1";
            this.pass1.PasswordChar = '•';
            this.pass1.Size = new System.Drawing.Size(163, 23);
            this.pass1.TabIndex = 3;
            this.pass1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textac1_KeyUp);
            // 
            // napis3
            // 
            this.napis3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.napis3.AutoSize = true;
            this.napis3.Font = new System.Drawing.Font("Verdana", 10F);
            this.napis3.Location = new System.Drawing.Point(10, 208);
            this.napis3.Name = "napis3";
            this.napis3.Size = new System.Drawing.Size(186, 17);
            this.napis3.TabIndex = 4;
            this.napis3.Text = "Prihlasovacie heslo znovu";
            // 
            // pass2
            // 
            this.pass2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pass2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.pass2.Location = new System.Drawing.Point(13, 229);
            this.pass2.Name = "pass2";
            this.pass2.PasswordChar = '•';
            this.pass2.Size = new System.Drawing.Size(163, 23);
            this.pass2.TabIndex = 5;
            this.pass2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textac1_KeyUp);
            // 
            // checkBox1
            // 
            this.checkBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(13, 258);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(93, 17);
            this.checkBox1.TabIndex = 25;
            this.checkBox1.Text = "Zobraziť heslo";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // napis4
            // 
            this.napis4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.napis4.AutoSize = true;
            this.napis4.Font = new System.Drawing.Font("Verdana", 10F);
            this.napis4.Location = new System.Drawing.Point(10, 290);
            this.napis4.Name = "napis4";
            this.napis4.Size = new System.Drawing.Size(50, 17);
            this.napis4.TabIndex = 7;
            this.napis4.Text = "e-mail";
            // 
            // textac4
            // 
            this.textac4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textac4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.textac4.Location = new System.Drawing.Point(13, 310);
            this.textac4.Name = "textac4";
            this.textac4.Size = new System.Drawing.Size(163, 23);
            this.textac4.TabIndex = 8;
            this.textac4.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textac1_KeyUp);
            // 
            // napis5
            // 
            this.napis5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.napis5.AutoSize = true;
            this.napis5.Font = new System.Drawing.Font("Verdana", 10F);
            this.napis5.Location = new System.Drawing.Point(10, 336);
            this.napis5.Name = "napis5";
            this.napis5.Size = new System.Drawing.Size(92, 17);
            this.napis5.TabIndex = 9;
            this.napis5.Text = "e-mail heslo";
            // 
            // pass_mail
            // 
            this.pass_mail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pass_mail.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.pass_mail.Location = new System.Drawing.Point(13, 356);
            this.pass_mail.Name = "pass_mail";
            this.pass_mail.PasswordChar = '•';
            this.pass_mail.Size = new System.Drawing.Size(163, 23);
            this.pass_mail.TabIndex = 10;
            this.pass_mail.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textac1_KeyUp);
            // 
            // checkBox2
            // 
            this.checkBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(13, 385);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(93, 17);
            this.checkBox2.TabIndex = 26;
            this.checkBox2.Text = "Zobraziť heslo";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // my_Button3
            // 
            this.my_Button3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.my_Button3.BackColor = System.Drawing.Color.Transparent;
            this.my_Button3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("my_Button3.BackgroundImage")));
            this.my_Button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.my_Button3.FlatAppearance.BorderSize = 0;
            this.my_Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.my_Button3.Font = new System.Drawing.Font("Verdana", 12F);
            this.my_Button3.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.my_Button3.Location = new System.Drawing.Point(180, 424);
            this.my_Button3.Name = "my_Button3";
            this.my_Button3.Size = new System.Drawing.Size(140, 41);
            this.my_Button3.TabIndex = 14;
            this.my_Button3.Text = "Pridať";
            this.my_Button3.UseVisualStyleBackColor = false;
            this.my_Button3.Click += new System.EventHandler(this.my_Button3_Click);
            // 
            // my_Button1
            // 
            this.my_Button1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.my_Button1.BackColor = System.Drawing.Color.Transparent;
            this.my_Button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("my_Button1.BackgroundImage")));
            this.my_Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.my_Button1.FlatAppearance.BorderSize = 0;
            this.my_Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.my_Button1.Font = new System.Drawing.Font("Verdana", 12F);
            this.my_Button1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.my_Button1.Location = new System.Drawing.Point(13, 424);
            this.my_Button1.Name = "my_Button1";
            this.my_Button1.Size = new System.Drawing.Size(140, 41);
            this.my_Button1.TabIndex = 15;
            this.my_Button1.Text = "Zrušiť";
            this.my_Button1.UseVisualStyleBackColor = false;
            this.my_Button1.Click += new System.EventHandler(this.my_Button2_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox1.Location = new System.Drawing.Point(206, 62);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(148, 89);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Práva";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Checked = true;
            this.radioButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.radioButton2.Location = new System.Drawing.Point(6, 50);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(79, 21);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Skladník";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.radioButton1.Location = new System.Drawing.Point(6, 23);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(109, 21);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.Text = "Administrátor";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // napis6
            // 
            this.napis6.AutoSize = true;
            this.napis6.Font = new System.Drawing.Font("Verdana", 10F);
            this.napis6.Location = new System.Drawing.Point(18, 5);
            this.napis6.Name = "napis6";
            this.napis6.Size = new System.Drawing.Size(88, 17);
            this.napis6.TabIndex = 17;
            this.napis6.Text = "Staré heslo";
            // 
            // textac1
            // 
            this.textac1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.textac1.Location = new System.Drawing.Point(14, 25);
            this.textac1.Name = "textac1";
            this.textac1.PasswordChar = '•';
            this.textac1.Size = new System.Drawing.Size(163, 23);
            this.textac1.TabIndex = 0;
            this.textac1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textac1_KeyUp);
            // 
            // napis7
            // 
            this.napis7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.napis7.AutoSize = true;
            this.napis7.Font = new System.Drawing.Font("Verdana", 10F);
            this.napis7.Location = new System.Drawing.Point(18, 60);
            this.napis7.Name = "napis7";
            this.napis7.Size = new System.Drawing.Size(161, 17);
            this.napis7.TabIndex = 19;
            this.napis7.Text = "Administratívne meno";
            // 
            // textac2
            // 
            this.textac2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textac2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.textac2.Location = new System.Drawing.Point(14, 80);
            this.textac2.Name = "textac2";
            this.textac2.Size = new System.Drawing.Size(163, 23);
            this.textac2.TabIndex = 1;
            this.textac2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textac1_KeyUp);
            // 
            // Account
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(390, 503);
            this.Name = "Account";
            this.Load += new System.EventHandler(this.Account_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Forms.Napis napis1;
        private System.Windows.Forms.CheckBox checkBox2;
        private Forms.Textac pass_mail;
        private Forms.Napis napis5;
        private Forms.Textac textac4;
        private Forms.Napis napis4;
        private System.Windows.Forms.CheckBox checkBox1;
        private Forms.Textac pass2;
        private Forms.Napis napis3;
        private Forms.Textac pass1;
        private Forms.Napis napis2;
        private Forms.Textac name;
        private my_Button my_Button1;
        private my_Button my_Button3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private Forms.Textac textac1;
        private Forms.Napis napis6;
        private Forms.Textac textac2;
        private Forms.Napis napis7;
    }
}
