﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using RotekIS.SQL;
using RotekIS.General;

namespace RotekIS.Main
{
    public partial class Account : RotekIS.Stocks.Inter_Form
    {
        private int mode;

        public Account(int mode)
        {
            InitializeComponent();
            this.mode = mode;
            if (mode == 1)
            {
                nadpis_set("Pridať užívateľa");
                textac1.Hide();
                napis6.Hide();
                this.Size = new System.Drawing.Size(Width, 450);
            }
            else
            {
                nadpis_set("Zmeniť užívateľa");
                groupBox1.Hide();
            }
        }

        private void Account_Load(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                pass1.PasswordChar = '\0';
                pass2.PasswordChar = '\0';
                textac1.PasswordChar = '\0';

            }
            else
            {
                textac1.PasswordChar = '•';
                pass1.PasswordChar = '•';
                pass2.PasswordChar = '•';
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                pass_mail.PasswordChar = '\0';
            }
            else
            {
                pass_mail.PasswordChar = '•';
            }

        }

        private void my_Button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void my_Button3_Click(object sender, EventArgs e)
        {
            confirm();  

        }

        private void confirm()
        {
            string oldpass = null;
            if (textac1.Text != "")
            {
                oldpass = textac1.Text;
            }

            string nick = name.Text;
            string pass = pass1.Text;
            string mail = textac4.Text;
            string mailPass = pass_mail.Text;
            string real = textac2.Text;

            int typ = radioButton1.Checked ? 1 : 3;


            if (oldpass == null && ((nick.Length == 0 || pass.Length == 0 || real.Length == 0) && mode == 1 || mode == 2))
            {
                Error.Show("Nevyplnené všetky povinné polia");
                return;
            }


            Account_SQL.Add(nick, real, pass, oldpass, mail, mailPass, typ);
            Dispose();
        }

        private void textac1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape) Dispose();
            if (e.KeyCode == Keys.Enter) confirm();

        }


    }
}
