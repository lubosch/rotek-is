﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Windows.Forms;
using RotekIS.Main;
using System.Drawing;
using System.Windows.Interop;
using RotekIS.Properties;
using RotekIS.Forms;

namespace RotekIS.General
{
    class Error
    {

        public static void Show(object i)
        {
            try
            {
                if (i.ToString().IndexOf("line") < 0 || Main.Menu.uzivatel.typ == "admin"  )
                {
                    MessageBox.Show(i.ToString());
                    return;
                }
           
                string text ;
                string subject ;


                Bitmap b = new Bitmap(SystemInformation.VirtualScreen.Width, SystemInformation.VirtualScreen.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                Graphics gfx = Graphics.FromImage(b);
                gfx.CopyFromScreen(SystemInformation.VirtualScreen.X, SystemInformation.VirtualScreen.Y, 0, 0, Screen.PrimaryScreen.Bounds.Size, CopyPixelOperation.SourceCopy);
                string cesta = Properties.Resources.Rotek3;
                cesta = cesta + "chyby.jpg";
                b.Save(cesta, System.Drawing.Imaging.ImageFormat.Jpeg);


                subject = "CHYBA Rotek sklad";
                text = Main.Menu.uzivatel.nick + "   \n" + i.ToString();
                Mail mail = new Mail(subject, text);
                mail.mne();
                mail.pridaj_prilohu(cesta);
                mail.posli();


                MessageBox.Show("Vyskytla sa chyba. Správa už bola odoslaná a na oprave sa pracuje. Čokoľvek ste chceli spraviť sa nevykonalo. Možno.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Vyskytla sa chyba. Správa NEBOLA odoslaná a na oprave sa NEpracuje. Čokoľvek ste chceli spraviť sa nevykonalo. Možno.");
            }

        }

        public static bool Question(String text)
        {
            DialogResult dialogResult = MessageBox.Show(text, "Some Title", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                return true;
            }
            else 
            {
                return false;
            }
        }




        public static string ShowDialog(string text, string title)
        {
            TextDialog t = new TextDialog(title, text);
            if (t.ShowDialog() == DialogResult.OK)
            {
                return t.text;
            }
            else
            {
                return null;
            }

        }
    }
}
