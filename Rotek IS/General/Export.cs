﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OfficeOpenXml;
using System.Collections;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using RotekIS.SQL;

namespace RotekIS.General
{
    class Export
    {
        public static void export(DataGridView DataGridView1)
        {


            if ((DataGridView1.Columns.Count == 0) || (DataGridView1.Rows.Count == 0))
            {
                return;
            }

            //String cesta = create_excel();
            //if (cesta == "") return;

            //Creating dataset to export
            DataSet dset = new DataSet();
            //add table to dataset
            dset.Tables.Add();
            //add column to that table
            for (int i = 0; i <= DataGridView1.ColumnCount - 1; i++)
            {
                if (DataGridView1.Columns[i].Visible == true)
                {
                    dset.Tables[0].Columns.Add(DataGridView1.Columns[i].HeaderText);
                }

            }
            //add rows to the table
            DataRow dr1 = null;
            int k = 0;

            for (int i = 0; i <= DataGridView1.RowCount - 1; i++)
            {
                dr1 = dset.Tables[0].NewRow();
                for (int j = 0; j < DataGridView1.Columns.Count; j++)
                {
                    if (DataGridView1.Columns[j].Visible == true)
                    {
                        dr1[k] = DataGridView1.Rows[i].Cells[j].Value;
                        k = k + 1;
                    }
                }
                k = 0;
                dset.Tables[0].Rows.Add(dr1);
            }


            System.Data.DataTable dt = dset.Tables[0];
            export(dt);

            //FileInfo fufukeh = new FileInfo(cesta);
            //ExcelPackage excelApp = new ExcelPackage(fufukeh);
            //ExcelWorksheet excelSheet = default(ExcelWorksheet);

            //excelSheet = excelApp.Workbook.Worksheets.Add("Zoznam");

            //System.Data.DataTable dt = dset.Tables[0];
            //System.Data.DataColumn dc = null;
            //System.Data.DataRow dr = null;
            //int colIndex = 0;
            //int rowIndex = 0;

            //int pocet_stlpcov = dt.Columns.Count;
            //foreach (DataColumn dc_loopVariable in dt.Columns)
            //{
            //    dc = dc_loopVariable;
            //    colIndex = colIndex + 1;
            //    excelSheet.Cells[1, colIndex].Value = dc.ColumnName;
            //}

            //foreach (DataRow dr_loopVariable in dt.Rows)
            //{
            //    dr = dr_loopVariable;
            //    rowIndex = rowIndex + 1;
            //    colIndex = 0;
            //    foreach (DataColumn dc_loopVariable in dt.Columns)
            //    {
            //        dc = dc_loopVariable;
            //        colIndex = colIndex + 1;
            //        excelSheet.Cells[rowIndex + 1, colIndex].Value = dr[dc.ColumnName];
            //    }
            //}
            //int b = 1;

            //foreach (DataColumn dc_loopVariable in dt.Columns)
            //{
            //    dc = dc_loopVariable;
            //    colIndex = colIndex + 1;
            //    excelSheet.Cells[1, b].Style.Font.Bold = true;
            //    b = b + 1;
            //}

            //for (int i = 1; i <= pocet_stlpcov; i++)
            //{
            //    excelSheet.Column(i).AutoFit();
            //}

            //string strFileName = cesta;
            //bool blnFileOpen = false;
            //try
            //{
            //    System.IO.FileStream fileTemp = System.IO.File.OpenWrite(strFileName);
            //    fileTemp.Close();
            //}
            //catch (Exception ex)
            //{
            //    blnFileOpen = false;
            //}

            //if (System.IO.File.Exists(strFileName))
            //{
            //    System.IO.File.Delete(strFileName);
            //}
            //excelApp.SaveAs(fufukeh);
            //Process.Start(cesta);
        }

        public static String create_excel()
        {
            SaveFileDialog openFileDialog1 = new SaveFileDialog();

            openFileDialog1.InitialDirectory = System.Environment.CurrentDirectory;
            openFileDialog1.Title = "Open Text File";
            openFileDialog1.Filter = "Excel(*.xlsx)|*.xlsx";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
            string cesta = "";

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                cesta = openFileDialog1.FileName;
            }
            else
            {
                return "";
            }
            if (File.Exists(cesta))
            {
                File.Delete(cesta);
            }
            return cesta;
        }


        public static void export(DataTable dt)
        {
            string cesta = create_excel();
            if (cesta == "") return;

            FileInfo fufukeh = new FileInfo(cesta);
            ExcelPackage excelApp = new ExcelPackage(fufukeh);
            ExcelWorksheet excelSheet = default(ExcelWorksheet);

            excelSheet = excelApp.Workbook.Worksheets.Add("Zoznam");

            write_table_to_excel(dt, excelSheet);

            string strFileName = cesta;
            try
            {
                System.IO.FileStream fileTemp = System.IO.File.OpenWrite(strFileName);
                fileTemp.Close();
            }
            catch (Exception ex)
            {
            }

            if (System.IO.File.Exists(strFileName))
            {
                System.IO.File.Delete(strFileName);
            }
            excelApp.SaveAs(fufukeh);
            Process.Start(cesta);


        }


        public static void export(DataSet ds)
        {
            string cesta = create_excel();
            if (cesta == "") return;

            FileInfo fufukeh = new FileInfo(cesta);
            ExcelPackage excelApp = new ExcelPackage(fufukeh);
            ExcelWorksheet excelSheet = default(ExcelWorksheet);

            excelSheet = excelApp.Workbook.Worksheets.Add(ds.DataSetName);
            int row_count = 1;
            foreach (DataTable dt in ds.Tables)
            {
                if (dt.Rows.Count > 0)
                {
                    write_table_to_excel(dt, excelSheet, row_count);
                    row_count += dt.Rows.Count + 3;
                }
            }


            string strFileName = cesta;
            try
            {
                System.IO.FileStream fileTemp = System.IO.File.OpenWrite(strFileName);
                fileTemp.Close();
            }
            catch (Exception ex)
            {
            }

            if (System.IO.File.Exists(strFileName))
            {
                System.IO.File.Delete(strFileName);
            }
            excelApp.SaveAs(fufukeh);
            Process.Start(cesta);


        }

        public static void export(List<DataSet> list_ds)
        {
            string cesta = create_excel();
            if (cesta == "") return;

            FileInfo fufukeh = new FileInfo(cesta);
            ExcelPackage excelApp = new ExcelPackage(fufukeh);
            ExcelWorksheet excelSheet = default(ExcelWorksheet);

            foreach (DataSet ds in list_ds)
            {
                if (!is_dataset_empty(ds))
                {
                    excelSheet = excelApp.Workbook.Worksheets.Add(ds.DataSetName);
                    int row_count = 1;
                    foreach (DataTable dt in ds.Tables)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            write_table_to_excel(dt, excelSheet, row_count);
                            row_count += dt.Rows.Count + 5;
                        }
                    }
                }
            }


            string strFileName = cesta;
            try
            {
                System.IO.FileStream fileTemp = System.IO.File.OpenWrite(strFileName);
                fileTemp.Close();
            }
            catch (Exception ex)
            {
            }

            if (System.IO.File.Exists(strFileName))
            {
                System.IO.File.Delete(strFileName);
            }
            excelApp.SaveAs(fufukeh);
            Process.Start(cesta);


        }

        public static Boolean is_dataset_empty(DataSet ds)
        {
            foreach (DataTable dt in ds.Tables)
            {
                if (dt.Rows.Count > 0)
                {
                    return false;
                }
            }
            return true;
        }

        public static void write_table_to_excel(DataTable dt, ExcelWorksheet excelSheet)
        {
            write_table_to_excel(dt, excelSheet, 1);
        }

        public static void write_table_to_excel(DataTable dt, ExcelWorksheet excelSheet, int rowIndex)
        {
            System.Data.DataColumn dc = null;
            System.Data.DataRow dr = null;
            int colIndex = 0;

            excelSheet.Cells[rowIndex, 1].Value = dt.TableName;
            rowIndex++;

            int pocet_stlpcov = dt.Columns.Count;
            foreach (DataColumn dc_loopVariable in dt.Columns)
            {
                dc = dc_loopVariable;
                colIndex = colIndex + 1;
                excelSheet.Cells[rowIndex, colIndex].Value = dc.ColumnName;
                excelSheet.Cells[rowIndex, colIndex].Style.Font.Bold = true;
            }



            foreach (DataRow dr_loopVariable in dt.Rows)
            {
                dr = dr_loopVariable;
                rowIndex = rowIndex + 1;
                colIndex = 0;
                foreach (DataColumn dc_loopVariable in dt.Columns)
                {
                    dc = dc_loopVariable;
                    colIndex = colIndex + 1;
                    excelSheet.Cells[rowIndex, colIndex].Value = dr[dc.ColumnName];
                }
            }

            for (int i = 1; i <= pocet_stlpcov; i++)
            {
                excelSheet.Column(i).AutoFit();
            }

        }

    }
}
